package mod2;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL10;
import com.badlogic.gdx.graphics.Mesh;
import com.badlogic.gdx.graphics.VertexAttribute;
import com.badlogic.gdx.graphics.VertexAttributes.Usage;
import com.badlogic.gdx.graphics.g3d.model.SubMesh;

//import com.badlogic.gdx.graphics.Texture;

import com.badlogic.gdx.math.Vector3;



public class Waves {
	Mesh mesh;
	long lastframetime;
	//float currenttime;
	List<Wave> waves;
	List<Ripple> ripples;
	//we should always have that numberOfVertices=xGridPoints*yGridPoints
	int numberOfVertices;
	int xGridNumPoints;
	int zGridNumPoints;
	float GridWidth;
	Vector3 position;
	float GridHeight;
	int numberOfIndices;
	
	float currentTime;
	
	float[] vertices;
	float[] xGridPoints;
	float[] zGridPoints;
	short[] indices;
	
	//FloatBuffer vertexBuffer;
	
	public Waves()
	{
		lastframetime=System.nanoTime();
		currentTime=0.0f;
		position=new Vector3(0,0,0);
	}
	//add ripple to list of ripples
	public void addRipple(float x, float z,float duration)
	{
		ripples.add(new Ripple(x,z,duration));

	}
	
	public void create(	int xNumPoints,	int zNumPoints, float Width,	float Height) {
		numberOfVertices=xNumPoints*zNumPoints;
		xGridNumPoints=xNumPoints;
		zGridNumPoints=zNumPoints;
		GridWidth=Width;
		GridHeight=Height;
		numberOfIndices=(2*xGridNumPoints-1)*(zGridNumPoints-1)+1;

		mesh = new Mesh( false, numberOfVertices, numberOfIndices,
							new VertexAttribute(Usage.Position, 3, "wave_position"),
							new VertexAttribute(Usage.Normal, 3, "wave_normal"),
							new VertexAttribute(Usage.ColorPacked,4,"wave_color")
						);

		//create a new arrray to build the vertices.  Each vertex takes up 36bytes, or 36/4=9 floats
		vertices=new float[mesh.getMaxVertices()*mesh.getVertexSize()/4];
		xGridPoints=new float[xGridNumPoints];
		zGridPoints= new float[zGridNumPoints];
		indices= new short[mesh.getMaxIndices()];

		waves= new LinkedList<Wave>();
		ripples=new LinkedList<Ripple>();
		initializeMesh();
		mesh.setAutoBind(true);

	}

	//possibly get rid of xGridStart and just translate
	//create the default positions for the vertices on the mesh
	public void initializeMesh()
	{
		//mesh.getverticesBuffer  use to access vertices maybe
		buildVertices();
		buildIndices();
	}
	
	//place vertices along grid of width GridWidth, and height GridHeight
	public void buildVertices()
	{
		

		
		float red,green,blue,alpha,color;
		//set the colors for each vertex here.  values for each color range from 0-1
		//color red
		red=0f;

		//color green
		green=0f;

		//color blue
		blue=1f;
		//color alpha
		alpha=1f;
		
		//take the color components, convert it to a color object, then pack it into a single float
		color= Color.toFloatBits(red,green,blue,alpha);
		
		//load all the grid points
		for(int j=0;j<xGridNumPoints;j++)
		{
				xGridPoints[j]=j*(float)GridHeight/(float)zGridNumPoints;;
			
		}
		
		for(int i=0;i<zGridNumPoints;i++)
		{

			//x
			zGridPoints[i]=i*(float)GridWidth/(float)xGridNumPoints;

		}
		
		for(int arrayIndex=0;arrayIndex<numberOfVertices*(mesh.getVertexSize()/4);)
		{
			for(int j=0;j<xGridNumPoints;j++)
			{
					
				
				for(int i=0;i<zGridNumPoints;i++)
				{

					//x
					vertices[arrayIndex]=position.x+xGridPoints[i];
					//y
					vertices[arrayIndex+1]=position.y;
					//z
					vertices[arrayIndex+2]=position.z+zGridPoints[j];

					//xnormal
					vertices[arrayIndex+3]=0;
					//ynormal
					vertices[arrayIndex+4]=1;
					//znormal
					vertices[arrayIndex+5]=0;
					
					
					vertices[arrayIndex+6]=color;
					
			
					
					arrayIndex+=mesh.getVertexSize()/4;
					
				}
			}
		}

		
		mesh.setVertices(vertices);
	}
	
	
	//might be some trouble here
	//Connect the vertices to make a flat rectangular mesh.  This took kind of a while to do and might be confusing.  
	//there is a graphic in the comments describing what order the indices need to be in
	public void buildIndices()
	{
		//row and column of matrix made by the grid points
		short row=0, column=0,vertexNumber=0;
		//to build the mesh we need to go current, then above, then to the 
		
		//If . are the grid points
		//	.	.
		//	.	.	
		//	.	.	
		//	.	.		
		//the sequence of indices goes through the points
		//	.	.
		//	.	.	
		//	.	.	
		//	x	.		
		
		//	.	.
		//	.	.	
		//	x	.	
		//	x	.		
		
		//	.	.
		//	.	.	
		//	x	.	
		//	x	x		
		
		//	.	.
		//	.	.	
		//	x	x	
		//	x	x		
		
		//	.	.	
		//	x	x	
		//	x	x		
		
		//	.	x	
		//	x	x	
		//	x	x
		
		//	x	x	
		//	x	x	
		//	x	x	
		boolean goingRight=true;
		boolean up=false;
		short workingRow=0;
		short xindex=0;
		for( short currentIndex=0;currentIndex<numberOfIndices;currentIndex++)
		{
				//working row should never get to yGridNumPoints-1
				if(xindex>=xGridNumPoints)
				{
					xindex--;
					workingRow++;
					goingRight=!goingRight;
					up=true;
				}
				else if(xindex<0)
				{
					
					xindex++;
					workingRow++;
					goingRight=!goingRight;
					up=true;
				}
				column=xindex;

				if(up)
				{
					row=(short) (workingRow+1);
					if(goingRight) xindex++;
					else xindex--;
				}
				else
					row=workingRow;
				
				up=!up;
				
				//lexographic ordering for vertex positions
				vertexNumber=(short) (row*xGridNumPoints+column);
				
				indices[currentIndex]=vertexNumber;

			}

		mesh.setIndices(indices);		

	}
	
	
	public void printIndices()
	{
		for(int i=0;i<indices.length;i++)
			System.out.println(indices[i]);
		
	}
	
	public void printVertices()
	{
		
		for(int i=0;i<vertices.length;i+=mesh.getVertexSize()/4)
		{
		
			//position --- normal / color
			System.out.println("position "+vertices[i]+","+vertices[i+1]+","+vertices[i+2]+" --normal-- "+vertices[i+3]+","+vertices[i+4]+","+vertices[i+5]+"/ Color / "+vertices[i+6]);
			
		}
	}
	
	
	//update the mesh that contains the information about the waves and such.
	public void updateMesh()
	{		

		float red,green,blue,alpha,color;
		float delta=(System.nanoTime()-lastframetime)/(float)1000000000;
		lastframetime=System.nanoTime();

		//time in seconds since the set of waves started
		currentTime+=delta;

		//update the frames to the current time;
		updateWaves(delta);
		//get the current vertices in the mesh
		
		
		Vector3 position, updatedPosition, normal;
		//set the colors for each vertex here.  values for each color range from 0-1
		//color red
		red=.1f;
		
		//color green
		green=.05f;
		
		//color blue
		blue=.99f;
		//color alpha
		alpha=.01f;
		SubMesh mesh2;
		
		//take the color components, convert it to a color object, then pack it into a single float
		color= Color.toFloatBits(red,green,blue,alpha);
		
	//	System.out.println(color);
		//go through the buffer and update vertices to represent their new values
		for(int arrayIndex=0;arrayIndex<vertices.length;)
		{
			
			for(int j=0;j<xGridNumPoints;j++)
			{
					
				
				for(int i=0;i<zGridNumPoints;i++)
				{
					//the current position of the vertex on wave
					//i/(mesh.getVertexSize()/4)
					position=new Vector3(this.position.x+xGridPoints[i],this.position.y,this.position.z+zGridPoints[j]);
	
					//the new position for the vertex
					updatedPosition=getPositionOnWave(position.x,position.z).add(0,position.y,0);

					//the normal for the current position
					normal=getNormalOnWave(updatedPosition.x,updatedPosition.z);
			
		
					//x
					vertices[arrayIndex]=updatedPosition.x;
			
					//y
					vertices[arrayIndex+1]=updatedPosition.y;
			
					//z
					vertices[arrayIndex+2]=updatedPosition.z;
			
			
					//x normal
					vertices[arrayIndex+3]=normal.x;
					//y normal
					vertices[arrayIndex+4]=normal.y;
					//z normal
					vertices[arrayIndex+5]=normal.z;
					

					vertices[arrayIndex+6]=color;
					arrayIndex+=mesh.getVertexSize()/4;
				}
			}
		
		}
		mesh.setVertices(vertices);

	}
	
	//gets the normal of the waves only, no ripples
	//http://developer.nvidia.com/node/110 
	//equation 12
	public Vector3 getNormalOnWave(float x, float z)
	{
		
//		Vector3 TangentSum=new Vector3(0,0,0);
//		Vector3 BitangentSum=new Vector3(0,0,0);
//		
		Vector3 sum=new Vector3(0,0,0);
		
		Wave currentWave;
//		Vector3 Tangent=new Vector3(0,0,0);
//		Vector3 Bitangent=new Vector3(0,0,0);
		Vector3 Normal;
		Vector3 D;
		float WA=0;
		float S;
		float C;
		float Q;
		
		for(Iterator<Wave> it=waves.iterator();it.hasNext();)
		{
			currentWave=it.next();
			WA=currentWave.amplitude*currentWave.frequency;
			D=currentWave.direction;
			S=(float) (Math.sin(currentWave.frequency*D.dot(getPositionOnWave(x,z))+currentWave.wavespeed*currentTime));
     		C=(float) (Math.cos(currentWave.frequency*D.dot(getPositionOnWave(x,z))+currentWave.wavespeed*currentTime));
			Q=	currentWave.steepness/(currentWave.frequency*currentWave.amplitude*waves.size());
			sum.add(D.x*WA*C,Q*WA*S,D.z*WA*C);

			
		}
		
		Normal= new Vector3(sum.x,-1+sum.y,sum.z);
//		
//		for(Iterator<Wave> it=waves.iterator();it.hasNext();)
//		{
//			currentWave=it.next();
//			WA=currentWave.amplitude*currentWave.frequency;
//			D=currentWave.direction;
//			S=(float) (Math.sin(currentWave.frequency*D.dot(getPositionOnWave(x,z))+currentWave.wavespeed*currentTime));
//     		C=(float) (Math.cos(currentWave.frequency*D.dot(getPositionOnWave(x,z))+currentWave.wavespeed*currentTime));
//			Q=	currentWave.steepness/(currentWave.frequency*currentWave.amplitude*waves.size());
//			BitangentSum.add(Q*D.x*D.x*WA*S,D.x*WA*C,Q*D.x*D.z*WA*S);
//			TangentSum.add(Q*D.x*D.z*WA*S,D.z*WA*C,Q*D.z*D.z*WA*S);
//
//			
//		}
//
//
//		Bitangent=new Vector3(1-BitangentSum.x,BitangentSum.y,-BitangentSum.z);
//		Tangent=new Vector3(-TangentSum.x,TangentSum.y,1-TangentSum.z);
//		 
//		return Bitangent.crs(Tangent);
		//not really sure which one to go with -1, or 1
		return Normal.mul(-1);
	}
	
	//update the time for each wave part
	public void updateWaves(float delta)
	{
		List removelist = new LinkedList<Ripple>();
		Ripple ripple;
		//System.out.println("num waves is: "+waves.size());
		for(Iterator<Wave> it=waves.iterator();it.hasNext();((Wave)it.next()).updateWave(delta));
//		for(Iterator<Ripple> it=ripples.iterator();it.hasNext();)
//		{
//			ripple=it.next();
//			if(ripple.Dead)removelist.add(ripple);
//		}	
//		for(Iterator<Ripple> it=removelist.iterator();it.hasNext();)
//		{
//			ripple=it.next();
//			ripples.remove(ripple);
//		}
//		for(Iterator<Ripple> it=ripples.iterator();it.hasNext();((Ripple)it.next()).updateRipple(delta));
	}
	
	public void render()
	{
		updateMesh(); 

		mesh.render( GL10.GL_TRIANGLE_STRIP );
		
	}
	//add a wave to the list of waves
	public void addWave(float steepness, Vector3 direction,float amplitude,float frequency, float wavespeed)
	{
		Wave w=new Wave(steepness,direction,amplitude,frequency,wavespeed);
		waves.add(w);
	}
	
	//returns the position that a vertex gets moved to ontop of a wave.
	//see http://developer.nvidia.com/node/110 for reference
	public Vector3 getPositionOnWave(float x, float z)
	{
		Vector3 sum=new Vector3(0,0,0);
		//add all the wave effects together;
		for(Iterator<Wave> it=waves.listIterator();it.hasNext();sum.add((Vector3) ((Wave)it.next()).getPositionOnWave(x, z,waves.size())));
		//add the ripples onto the waves
//		for(Iterator<Ripple> it=ripples.listIterator();it.hasNext();sum.add(new Vector3(0, ((Ripple)it.next()).getValueOnRipple(x,z),0)));
		sum.add(x,0,z);
		//System.out.println(sum);
		return sum;
		
	}
	
	//gets the combined normal
	public Vector3 getNormal(float x, float y,float z)
	{
		return new Vector3(0.0f,0.0f,0.0f);
		
	}
	
	public void Translate(Vector3 pos)
	{
		this.position=pos;
	
	}
}
