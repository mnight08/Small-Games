package mod2;

import com.badlogic.gdx.graphics.Camera;

public class CameraOperations 
{
	public static void shakeCameraLeft( Camera camera )
	{
		for (int i = 0; i < 10; i++)
			camera.position.add(.5f,0,0);
	}
	
	public static void shakeCameraRight( Camera camera )
	{
		for (int i = 0; i < 10; i++)
			camera.position.add(-.5f,0,0);
	}
}
