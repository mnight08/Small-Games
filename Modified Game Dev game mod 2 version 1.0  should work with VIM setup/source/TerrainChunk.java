package mod2;

import mod2.HeightMap;

import com.badlogic.gdx.math.Vector3;

public class TerrainChunk {
	
	private HeightMap m_heightMap;

	public int m_zct, m_xct;
	public float m_height, m_width;
	public float m_meshStep;
	
	public float m_vertices[];
	public short m_indices[];

	// sample step is number of pixels per sample, in both x and z, from the heightmap "filename"
	// mesh_step is the distance in world space between mesh points in both x and z
	// yscale is the number to scale the hightmap values (0-255) by
	// texscale is how big (in world space) the texture should be on the mesh
	public TerrainChunk ( HeightMap hm, float mesh_step, float yscale, float texscale ) {

		m_heightMap = hm;

		m_zct = m_heightMap.m_height;
		m_xct = m_heightMap.m_width;
		m_meshStep = mesh_step;

		m_width = m_xct * m_meshStep;
		m_height = m_zct * m_meshStep;			
		
		// create strip mesh for this terrain
		m_vertices = new float[m_xct * m_zct * 8];
		m_indices = new short[(m_zct-1) * (m_xct+1) * 2];

		buildVertices( yscale, texscale );
		buildIndices();
	}

	public void buildVertices ( float yscale, float texscale ) {

		int idx = 0;
		Vector3 norm;
		
		// position pass
		for (int zidx = 0; zidx < m_zct; zidx++) {
			for (int xidx = 0; xidx < m_xct; xidx++) {
				m_vertices[idx++] = xidx * m_meshStep;
				m_vertices[idx++] = m_heightMap.sample( xidx, zidx ) / yscale;
				m_vertices[idx++] = zidx * m_meshStep;
				
				// skip normal slots
				idx += 3;
				
				// text coords (tiling)
				m_vertices[idx++] = xidx / texscale;
				m_vertices[idx++] = zidx / texscale;
			}
		}
		
		// normal pass
		idx = 0;
		for (int zidx = 0; zidx < m_zct; zidx++) {
			for (int xidx = 0; xidx < m_xct; xidx++) {
				norm = calc_normal( idx );
				idx += 3;
				m_vertices[idx++] = norm.x;
				m_vertices[idx++] = norm.y;
				m_vertices[idx++] = norm.z;
				idx += 2;
			}
		}
	}

	private Vector3 calc_normal( int idx )
	{
		int pitch = m_xct * 8;
		// top row
		if( idx < pitch ) {
			return new Vector3( 0.0f, 1.0f, 0.0f );
		}
		// bottom roww
		else if( (m_vertices.length - idx) <= pitch ) {
			return new Vector3( 0.0f, 1.0f, 0.0f );
		}
		// left col
		else if( idx % pitch == 0 ) {
			return new Vector3( 0.0f, 1.0f, 0.0f );
		}
		// right col
		else if( (idx+8) % pitch == 0 ) {
			return new Vector3( 0.0f, 1.0f, 0.0f );
		}
		
		// general case, 8 neighbors
		Vector3 self = new Vector3( m_vertices[idx], m_vertices[idx+1], m_vertices[idx+2] );
		Vector3 neighbors[] = new Vector3[8];

		// above
		int nidx = idx - pitch - 8;
		int nct = 0;
		neighbors[nct++] = new Vector3( m_vertices[nidx], m_vertices[nidx+1], m_vertices[nidx+2] );
		nidx += 8;
		neighbors[nct++] = new Vector3( m_vertices[nidx], m_vertices[nidx+1], m_vertices[nidx+2] );
		nidx += 8;
		neighbors[nct++] = new Vector3( m_vertices[nidx], m_vertices[nidx+1], m_vertices[nidx+2] );

		// beside
		nidx = idx - 8;
		neighbors[nct++] = new Vector3( m_vertices[nidx], m_vertices[nidx+1], m_vertices[nidx+2] );
		nidx = idx + 8;
		neighbors[nct++] = new Vector3( m_vertices[nidx], m_vertices[nidx+1], m_vertices[nidx+2] );

		// below
		neighbors[nct++] = new Vector3( m_vertices[nidx], m_vertices[nidx+1], m_vertices[nidx+2] );
		nidx += 8;
		neighbors[nct++] = new Vector3( m_vertices[nidx], m_vertices[nidx+1], m_vertices[nidx+2] );
		nidx += 8;
		neighbors[nct++] = new Vector3( m_vertices[nidx], m_vertices[nidx+1], m_vertices[nidx+2] );
		
		Vector3 norm = new Vector3();
		
		for( int i=0; i<7; i++ ) {
		norm.add( neighbors[i].cpy().sub( self ).crs( neighbors[i+1].cpy().sub( self ) ) );
		}
		norm.add( neighbors[7].cpy().sub( self ).crs( neighbors[0].cpy().sub( self ) ) );
		norm.nor();
		
		return norm;
		//return new Vector3( 0, 1, 0 );
	}
	
	private void buildIndices () {
		
		int idx = 0;
		short i = 0;

		// for each row except the last one
		for (int z = 0; z < m_zct - 1; z++) {

			// draw the strip for a row
			for (int x = 0; x < m_xct; x++) {
				m_indices[idx++] = i;
				m_indices[idx++] = (short)(i + m_xct);
				i++;
			}

			// add indices for degenerate tris to bridge to next row
			m_indices[idx++] = (short)(i - 1 + m_xct);
			m_indices[idx++] = i;
		}
	}

}
