package mod2;
import com.badlogic.gdx.math.Vector3;
public class Ripple {
	float currenttime;
	float length;
	boolean Dead;
	float x;
	float z;
	public Ripple(float x, float z,float duration)
	{
		this.x=x;
		this.z=z;
		currenttime=0;
		length=duration;
		Dead=false;
	}
	public float getValueOnRipple(float x, float z)
	{
		if(currenttime==0)return 0;
		return (float) ((Math.sin(Math.PI*2*currenttime)/(Math.PI*2*currenttime)-1)*Math.exp(-currenttime)/(Math.sqrt((this.x-x)*(this.x-x)/(100)+(this.z-z)*(this.z-z)/(100)+.1)));
		
	}
	
	
	public void updateRipple(float delta)
	{
		
		currenttime+=delta;
		if(currenttime>length)
		{
			Dead=true;
		}
	}
}
