package mod2;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Pixmap;

public class HeightMap {
	
	public short m_width, m_height;
	private short m_samples[];

	// sample step indicates how many pixels per sample in both x and z
	public HeightMap( String filename, int sample_step ) {
		
		// load the image at whatever resolution it uses
		Pixmap heightMap = new Pixmap( Gdx.files.internal( filename ) );

		// calculate height and width of the sample set
		// (being sloppy about rounding here)
		m_width = (short)(heightMap.getWidth()/sample_step);
		m_height = (short)(heightMap.getHeight()/sample_step);

		// store all the samples in nice, easy-to-use short ints (0-255)
		m_samples = new short[m_height*m_width];

		int sidx = 0;
		for (int zidx = 0; zidx < m_height; zidx++) {
			for (int xidx = 0; xidx < m_width; xidx++) {
				// no interpolation, just taking nearest (grabbing B component from 32-bit grayscale img)
				m_samples[sidx++] = (short)((heightMap.getPixel( xidx * sample_step, zidx * sample_step ) & 0xff00) / 0x100);
			}
		}
		
	}

	public short sample( int x, int z ) {
		if( x < m_width && z < m_height )
			return m_samples[ z*m_width + x ];
		return 0;
	}

}
