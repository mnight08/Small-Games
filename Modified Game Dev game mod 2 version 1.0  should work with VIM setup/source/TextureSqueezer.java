package mod2;

import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.nio.charset.Charset;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.Pixmap.Format;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.Array;

public class TextureSqueezer {

        // configuration
        final int xPadding = 0;
        final int yPadding = 0;
        final boolean duplicateBorder = false;
        final boolean allowRotate = false;

        class Node {
                Node child1, child2;
                SubTexture subTexture;

                Pixmap pixmap;
                String name;
                boolean rotate;

                public Node(TextureSqueezer squeezer, int left, int top, int width,
                                int height) {
                        this.subTexture = new SubTexture(squeezer, left, top, width, height);
                }

                public Node insert(Pixmap pixmap, String name, boolean rotate) {

                        if (this.pixmap != null) {
                                return null;
                        }

                        if (this.child1 != null) {
                                Node newNode = this.child1.insert(pixmap, name, rotate);
                                if (newNode != null) {
                                        return newNode;
                                }

                                return this.child2.insert(pixmap, name, rotate);
                        }

                        int imageWidth = pixmap.getWidth();
                        int imageHeight = pixmap.getHeight();
                        if (rotate) {
                                int temp = imageWidth;
                                imageWidth = imageHeight;
                                imageHeight = temp;
                        }

                        int neededWidth = imageWidth + xPadding;
                        int neededHeight = imageHeight + yPadding;
                        if (neededWidth > this.subTexture.width
                                        || neededHeight > this.subTexture.height) {
                                return null;
                        }

                        if (neededWidth == this.subTexture.width
                                        && neededHeight == this.subTexture.height) {
                                this.pixmap = pixmap;
                                this.name = name;
                                this.rotate = rotate;
                                return this;
                        }

                        int dw = this.subTexture.width - neededWidth;
                        int dh = this.subTexture.height - neededHeight;
                        if (dw > dh) {
                                this.child1 = new Node(this.subTexture.textureSqueezer,
                                                this.subTexture.left, this.subTexture.top, neededWidth,
                                                this.subTexture.height);
                                this.child2 = new Node(this.subTexture.textureSqueezer,
                                                this.subTexture.left + neededWidth,
                                                this.subTexture.top, this.subTexture.width
                                                                - neededWidth, this.subTexture.height);
                        } else {
                                this.child1 = new Node(this.subTexture.textureSqueezer,
                                                this.subTexture.left, this.subTexture.top,
                                                this.subTexture.width, neededHeight);
                                this.child2 = new Node(this.subTexture.textureSqueezer,
                                                this.subTexture.left, this.subTexture.top
                                                                + neededHeight, this.subTexture.width,
                                                this.subTexture.height - neededHeight);
                        }
                        return this.child1.insert(pixmap, name, rotate);
                }

                void draw(Pixmap pixmap) {
                        if (this.pixmap != null) {
                                pixmap.drawPixmap(this.pixmap, this.subTexture.left + xPadding,
                                                this.subTexture.top + yPadding, 0, 0,
                                                this.pixmap.getWidth(), this.pixmap.getHeight());
                        }

                        if (this.child1 != null) {
                                this.child1.draw(pixmap);
                        }

                        if (this.child2 != null) {
                                this.child2.draw(pixmap);
                        }
                }

                void write(OutputStreamWriter writer) throws IOException {
                        if (this.name != null) {

                                writer.write(this.name);
                                writer.write('\n');
                                writer.write(Integer.toString(this.subTexture.left));
                                writer.write('\n');
                                writer.write(Integer.toString(this.subTexture.top));
                                writer.write('\n');
                                writer.write(Integer.toString(this.subTexture.width));
                                writer.write('\n');
                                writer.write(Integer.toString(this.subTexture.height));
                                writer.write('\n');
                                writer.write(this.rotate ? 1 : 0);
                                writer.write('\n');
                        }

                        if (this.child1 != null) {
                                this.child1.write(writer);
                        }

                        if (this.child2 != null) {
                                this.child2.write(writer);
                        }
                }

                public void texture(Texture texture) {

                        this.subTexture.texture = texture;

                        if (this.child1 != null) {
                                this.child1.texture(texture);
                        }

                        if (this.child2 != null) {
                                this.child2.texture(texture);
                        }
                }
        }

        Node rootNode;
        Pixmap pixmap;

        public TextureSqueezer(int width, int height) {
                this.rootNode = new Node(this, 0, 0, width, height);
        }

        public SubTexture insert(Pixmap pixmap, String name, boolean draw) {
                Node node = this.rootNode.insert(pixmap, name, false);
                if (node == null) {
                        if (this.allowRotate)
                                node = this.rootNode.insert(pixmap, name, true);
                        if (node == null)
                                return null;
                }
                if (draw) {
                        checkPixmap();
                        node.draw(this.pixmap);
                }
                return node.subTexture;
        }

        public boolean save(FileHandle image, FileHandle data) {
                OutputStream stream = data.write(false);
                OutputStreamWriter writer = new OutputStreamWriter(stream,
                                Charset.forName("US-ASCII"));
                try {
                        writer.write(image.name());
                        writer.write('\n');

                        write(writer);

                        writer.close();

                } catch (IOException e) {
                        // e.printStackTrace();

                        try {
                                writer.close();
                        } catch (IOException e1) {
                                // e1.printStackTrace();
                        }
                        return false;
                }

                draw();
                // pixmap.getPixels();
                // TODO save to PNG file
                // this.pixmap.dispose();

                return true;
        }

        void checkPixmap() {
                if (this.pixmap == null) {
                        this.pixmap = new Pixmap(this.rootNode.subTexture.width,
                                        this.rootNode.subTexture.height, Format.RGBA8888);
                }
        }

        Pixmap draw() {
                checkPixmap();

                this.rootNode.draw(this.pixmap);

                return this.pixmap;
        }

        void write(OutputStreamWriter writer) throws IOException {
                this.rootNode.write(writer);
        }

        public void textureFinalize() {
                Texture texture = new Texture(this.pixmap);
                this.pixmap.dispose();
                rootNode.texture(texture);
        }
}
