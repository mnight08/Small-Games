package mod2;

import com.badlogic.gdx.ApplicationListener;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.Camera;
import com.badlogic.gdx.graphics.GL10;
import com.badlogic.gdx.graphics.Mesh;
import com.badlogic.gdx.graphics.VertexAttribute;
import com.badlogic.gdx.graphics.VertexAttributes.Usage;
import com.badlogic.gdx.graphics.PerspectiveCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.Input.Buttons;
import com.badlogic.gdx.Input.Keys;
import java.nio.FloatBuffer;

public class Game implements ApplicationListener {
	
	//camera and its properties
	//I changed the camera to public !!!!!!!!!!!!!!!
	public Camera camera;
	float cameraRotH, cameraRotV;

	Mesh terrain;
	Texture texture;
		
	//Sound variables
	Music music, rain;
	Sound crash;

	boolean TALKING;
	boolean NORTH=false;
	long lastTime = System.nanoTime();
	
	//Models.  
	//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!111
	//I should probably make a static array of models, then have each model array simply be a tuple for the set of integers
	//in the array it is using
	Player player;
	Player CyberDemon;
	Static_Model house[]=new Static_Model[1];
	Static_Model trees[] = new Static_Model[10];

	//This is probably nonsense
	int mov=70;
	int movz=0;
	int i = 0;

	GL10 gl;
	float sunPos[];
	float lightning[];
	Non_Static_Model Shark;
	float cameraangle;
	float cameradistance;
	Vector3 relativeCameraPosition;
	
	Static_Model Tree2;
	Static_Model Cabbage[]=new Static_Model[40];
	
	Storm storm;
	Static_Model Wall[]=new Static_Model[45];
	Static_Model Text;

	@Override
	public void create() {
		Load_Terrain();
		Load_Static_Models();
		Load_Non_Static_Models();
		Load_Weather();
		Initialize_Camera();
		Initialize_Lighting();
	}


	@Override
	public void resize(int width, int height) {
		float aspectRatio = (float) width / (float) height;
		camera = new PerspectiveCamera( 67, 2f * aspectRatio, 2f );
		camera.near = 0.1f;
		camera.far = 500;
		
		relativeCameraPosition.x=(float) (Math.sin(cameraangle)*cameradistance);
		relativeCameraPosition.z=(float) (Math.cos(cameraangle)*cameradistance);

		// set a nice initial view
		camera.position.set(relativeCameraPosition.x+ player.position.x, player.position.y+10,relativeCameraPosition.z+ player.position.z );
	}

	//Update stuff here
	//This is where all the game logic will happen
	public void update()
	{
		
		Handle_Input.handleInput(this,Gdx.input, Gdx.graphics.getDeltaTime());
		player.update();
	}

	@Override
	public void render() {
        	Gdx.gl.glClear( GL10.GL_COLOR_BUFFER_BIT | GL10.GL_DEPTH_BUFFER_BIT );
		
		camera.rotate( cameraRotV, 1.0f, 0.0f, 0.0f );
		camera.rotate( cameraRotH, 0.0f, 1.0f, 0.0f );
		
		// Rotate camera to look at player
		camera.lookAt(player.position.x, player.position.y, player.position.z);

        	camera.apply(Gdx.gl10);


		//Gdx.gl10.glLightfv(GL10.GL_LIGHT0, GL10.GL_POSITION, new float[]{0, 50.0f, 0, 1}, 0);
        
      		//LightingOperations.playerLight( player, gl );
        
		///!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!111
		//need to put lighting effects into a weather class, then enable 
		//certain effects in the creation, and have weather evolve over time.
		//I will simply update weather here.  something like weather.update();
        	// Sun setting
        	//if (System.nanoTime() - lastTime > 1000000) {
        	//	LightingOperations.sunSetting( gl, sunPos );
        	//}
        
        	// Lightning strike
		// !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
		// Change this to randomly strike, make probability to it is about once every 
		// 30sec
        	//if ( System.nanoTime() - lastTime > 1013999999 && lightning[1] == 1 ) {
        	//	LightingOperations.lightningStrike( gl, lightning );
        	//	crash.play(1.0f);
        	//} else if ( System.nanoTime() - lastTime > 10000 ) {
        	//	LightingOperations.lightningStrikeFade( gl, lightning );
	        //}

		texture.bind();
		
		terrain.render( GL10.GL_TRIANGLE_STRIP );
		
		if (System.nanoTime() - lastTime > 1000000000) {
			Gdx.app.log("TerrainTest", "fps: " + Gdx.graphics.getFramesPerSecond());
			lastTime = System.nanoTime();
		}
		


		//for(int i=0;i<=house.length-1;i++){
		//	house[i].render();
		//}
		//for(int i=0;i<Cabbage.length;i++){
		//	Cabbage[i].render();		
		//}
		Tree2.render();
		//for(int i=0;i<Wall.length-1;i++){
		//	Wall[i].render();	
		//}
	
		
		storm.render();
		//Shark.render();
		//CyberDemon.render();
		player.direction.x=camera.direction.x;
		player.direction.y=0;
		player.direction.z=camera.direction.z;
		player.direction.nor();
		player.render();
		
		camera.update();
		//player.Detectnear(player, CyberDemon);
		//if(player.TEXT && TALKING){
		//	Text.render();	
		//}
		
	}
	
	@Override
	public void pause() {
		// TODO Auto-generated method stub
		//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
		//When I do this, I think I will have each non static object simply have a pause mode,
		//and pause itself.   Then when resume hits, it will unpause itself
	}

	@Override
	public void resume() {
		// TODO Auto-generated method stub

	}

	@Override
	public void dispose() {
		// TODO Auto-generated method stub

	}

	public void Load_Terrain()
	{
		HeightMap hm = new HeightMap( "resources/heightmapnew2.bmp", 20 );		
		TerrainChunk chunk = new TerrainChunk( hm, 5.0f, 4.0f, 12.0f );

		terrain = new Mesh( true, chunk.m_vertices.length / 8, chunk.m_indices.length,
							new VertexAttribute(Usage.Position, 3, "a_position"),
							new VertexAttribute(Usage.Normal, 3, "a_normal"),
							new VertexAttribute(Usage.TextureCoordinates, 2, "a_texCoords")
				);

        	
		terrain.setVertices(chunk.m_vertices);
		terrain.setIndices(chunk.m_indices);
		FileHandle imageFileHandle = Gdx.files.internal( "resources/grass.jpg" ); 
		texture = new Texture( imageFileHandle, true );
        	texture.bind();
		
		//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!11111
		//this should probably be in the render function, and changed according to what is being rendered.		
		gl = Gdx.graphics.getGL10();
		gl.glTexEnvf( GL10.GL_TEXTURE_ENV, GL10.GL_TEXTURE_ENV_MODE, GL10.GL_MODULATE );
		// when texture area is small, bilinear filter the closest mipmap
		gl.glTexParameterf( GL10.GL_TEXTURE_2D, GL10.GL_TEXTURE_MIN_FILTER,
							GL10.GL_LINEAR_MIPMAP_NEAREST );
		// when texture area is large, bilinear filter the original
		gl.glTexParameterf( GL10.GL_TEXTURE_2D, GL10.GL_TEXTURE_MAG_FILTER, GL10.GL_LINEAR );

		// the texture wraps over at the edges (repeat)
		gl.glTexParameterf( GL10.GL_TEXTURE_2D, GL10.GL_TEXTURE_WRAP_S, GL10.GL_REPEAT );
		gl.glTexParameterf( GL10.GL_TEXTURE_2D, GL10.GL_TEXTURE_WRAP_T, GL10.GL_REPEAT );
		gl.glEnable( GL10.GL_TEXTURE_2D );
		gl.glShadeModel( GL10.GL_SMOOTH );
		gl.glEnable( GL10.GL_DEPTH_TEST );
		gl.glEnable( GL10.GL_CULL_FACE );
	
	}

	public void Load_Weather()
	{
		storm= new Storm();
		// Load background rain audio
        	//FileHandle soundFileHandle = Gdx.files.internal( "music/rain.mp3" );
        	//rain = Gdx.audio.newMusic(soundFileHandle);
        	//rain.play();
        	//rain.setLooping(true);
        	//rain.setVolume(0.25f);
        
		//I might want to move this to a different function
        	// Load background music
        	//FileHandle musicFileHandle = Gdx.files.internal( "music/music.mp3" );
        	//music = Gdx.audio.newMusic(musicFileHandle);
        	//music.play();
        	//music.setLooping(true);
        	//music.setVolume(1.0f);
        
        	// Lightning crash sound effect
        	//FileHandle crashFileHandle = Gdx.files.internal( "music/crash.mp3" );
		//crash = Gdx.audio.newSound(crashFileHandle);
		
		
		//set up some fog
		gl = Gdx.graphics.getGL10();
		float density = 0.03f;
		float fogColor[]={.9f, 0.9f, 0.9f, .01f};
		gl.glEnable(GL10.GL_FOG);
		//set fog mode
		gl.glFogf(GL10.GL_FOG_MODE, GL10.GL_EXP2);
		//set fog color.  Could not get the color to work
		//gl.glFogfv(GL10.GL_FOG_COLOR, BufferedFogColor);
		//set fog density
		gl.glFogf(GL10.GL_FOG_DENSITY, density);
		gl.glFogf(GL10.GL_FOG_START,0f);
		gl.glFogf(GL10.GL_FOG_END,200f);
		gl.glFogfv(GL10.GL_FOG_COLOR,fogColor,0);
		//make fog nice.  May cause lag.
		gl.glHint(GL10.GL_FOG_HINT, GL10.GL_NICEST);
		
	}

	public void Initialize_Camera()
	{

		cameraangle=(float) (3*Math.PI/2);
		cameradistance=20;
		relativeCameraPosition= new Vector3(0,0,0);

	}

	public void Load_Non_Static_Models()
	{
		player=new Player(this,"resources/knight.md2", "resources/knight.jpg",new Vector3(110,20,110),11,0,.1f);
		//CyberDemon=new Player(this,"resources/Archvile/Archvile.md2", "resources/Archvile/archvile.png",new Vector3(100,17,450),0,270,.12f);

	}

	public void Load_Static_Models()
	{
		//for(int i=0;i<=house.length-1;i++){
		//	if(i<=4){
		//		house[i]=new Static_Model(this,"resources/castle.obj","resources/castle_tex.bmp",new Vector3(400,19,450),4,0,0,0,0);
		//	}else{
		//		house[i]=new Static_Model(this,"resources/castle.obj","resources/castle_tex.bmp",new Vector3(400,19,450),4,0,0,0,0);	
		//	}
		//}
		//for(int i=0;i<Cabbage.length;i++){
		//	if(i<10){
		//		Cabbage[i]=new Static_Model(this,"resources/terrain/cabbage.obj","resources/terrain/cabbage_tex.bmp",new Vector3(215-(i*10)-mov,16,480+movz),2,0,0,0,0);
		//	}else if(i>=10 && i<20){
		//		Cabbage[i]=new Static_Model(this,"resources/terrain/cabbage.obj","resources/terrain/cabbage_tex.bmp",new Vector3(215-((i-10)*10)-mov,16,460+movz),2,0,0,0,0);
		//	}else if(i>=20 && i<30){
		//		Cabbage[i]=new Static_Model(this,"resources/terrain/cabbage.obj","resources/terrain/cabbage_tex.bmp",new Vector3(215-((i-20)*10)-mov,16,440+movz),2,0,0,0,0);
		//	}else if(i>=30){
		//		Cabbage[i]=new Static_Model(this,"resources/terrain/cabbage.obj","resources/terrain/cabbage_tex.bmp",new Vector3(215-((i-30)*10)-mov,16,420+movz),2,0,0,0,0);
		//	}
		//}
		Tree2=new Static_Model(this,"resources/mesh2/roundtreeA.obj","resources/mesh2/roundtreebark.bmp",new Vector3(100,16,150),new Vector3(1,1,1),new Vector3(1,0,0),0);
	
		//Wall[0]=new Static_Model(this,"resources/terrain_hedge/shelffence.obj","resources/terrain_hedge/shelffence.bmp",new Vector3(100-mov,21,400+movz),5,0,0,0,0);
		//for(int i=0;i<Wall.length-1;i++){
		//	if(i<5){
		//		Wall[i+1]=new Static_Model(this,"resources/terrain_hedge/shelffence.obj","resources/terrain_hedge/shelffence.bmp",
		//				new Vector3(110+(i*10)-mov,21,400+movz),5,0,0,0,0);
		//	}else if(i>=5 && i<10){
		//		Wall[i+1]=new Static_Model(this,"resources/terrain_hedge/shelffence.obj","resources/terrain_hedge/shelffence.bmp",
		//				new Vector3(130+(i*10)-mov,21,400+movz),5,0,0,0,0);	
		//	}else if(i>=10 && i<20){
		//		Wall[i+1]=new Static_Model(this,"resources/terrain_hedge/shelffence.obj","resources/terrain_hedge/shelffence.bmp",
		//				new Vector3(95-mov,21,405+((i-10)*10)+movz),5,90,0,1,0);
		//	}else if(i>=20 && i<33){
		//		Wall[i+1]=new Static_Model(this,"resources/terrain_hedge/shelffence.obj","resources/terrain_hedge/shelffence.bmp",
		//				new Vector3(100+((i-20)*10)-mov,21,500+movz),5,180,0,1,0);
		//	}else if(i>=33){
		//		Wall[i+1]=new Static_Model(this,"resources/terrain_hedge/shelffence.obj","resources/terrain_hedge/shelffence.bmp",
		//				new Vector3(225-mov,21,405+(((i-33))*10)+movz),5,270,0,1,0);		
		//	}
		//}
		//Shark=new Static_Model(this,"resources/terrain_shark/shark.obj","resources/terrain_shark/sharktexture.bmp",new Vector3(138,7,245),5,0,0,0,0);
		//Text=new Static_Model(this,"resources/terrain_hedge/shelffence.obj","resources/terrain_hedge/textwrapped.bmp",new Vector3(100,30,450),6,90,0,1,0);
	
		

		//////////////////
		//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!1
		//This should randomly generate some trees probably.  Also should pick random trees to generate etc.  
		// Place tree models in map
		// THis fails right now because the texture is non square and we are using mipmapping
		//for(int i=0;i<=trees.length-1;i++){			
		//	trees[i]=new Static_Model(this,"resources/treeA.obj","resources/bark_NM.png",new Vector3(200,19,250),4,0,0,0,0);
		//}

	}

	public void Initialize_Lighting()
	{
		gl = Gdx.graphics.getGL10();
		gl.glEnable(GL10.GL_LIGHTING);
		// Character lighting
		gl.glEnable(GL10.GL_LIGHT0);
		// Sun
		gl.glEnable(GL10.GL_LIGHT1);

		// Set up character light source
		gl.glLightfv(GL10.GL_LIGHT1, GL10.GL_AMBIENT, new float[]{ 1.0f, 0.2f, 0.2f, 0.1f }, 0);
		// Color is red to simulate torch light
		gl.glLightfv(GL10.GL_LIGHT0, GL10.GL_DIFFUSE, new float[]{ 2f, 1f, 1f, 3f }, 0);
	
		// Set up Sun light source
		gl.glLightfv(GL10.GL_LIGHT1, GL10.GL_AMBIENT, new float[]{ 1.6f, 1.6f, 1.6f, 1f }, 0);
		gl.glLightfv(GL10.GL_LIGHT1, GL10.GL_DIFFUSE, new float[]{ 1.6f, 1.6f, 1.9f, 130f }, 0);
		// Set initial position of Sun
		sunPos = new float[]{ 500, 500, 500 };
		LightingOperations.sunSetting(gl, sunPos);
	
		// Initial setup of lightning strike intensity variable
		lightning = new float[]{ 1.0f, 1 };		
		gl.glEnable(GL10.GL_BLEND);

	}
}
