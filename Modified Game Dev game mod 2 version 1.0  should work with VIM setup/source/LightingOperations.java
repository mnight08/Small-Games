package mod2;

import com.badlogic.gdx.graphics.GL10;

public class LightingOperations {
	
	/*
	 * Environment and weather
	 * 
	 */
	
	// Create the effect of the sun setting
	public static void sunSetting( GL10 gl, float[] sun ) {
		if ( sun[1] > 1 ) {
			gl.glLightfv(GL10.GL_LIGHT1, GL10.GL_POSITION, new float[]{ sun[0], sun[1], sun[2], 1}, 0);
			sun[1]-=1;
		} else {
			gl.glDisable(GL10.GL_LIGHT1);
		}
		
	}
	
	// Simulate lightning striking
	public static void lightningStrike( GL10 glParam, float[] lightning ) {
		lightning[0] = 1.0f;
		// Lightning cannot strike again if zero
		lightning[1] = 0;
		
		// Set up lightning light source
		glParam.glEnable(GL10.GL_LIGHT2);
		glParam.glLightfv(GL10.GL_LIGHT2, GL10.GL_AMBIENT, new float[]{ 1.0f, 1.0f, 1.0f, 1f }, 0);
		glParam.glLightfv(GL10.GL_LIGHT2, GL10.GL_DIFFUSE, new float[]{ 1.0f, 1.0f, 1.0f, 1.0f }, 0);
		// Strike at random position
		glParam.glLightfv(GL10.GL_LIGHT2, GL10.GL_POSITION, new float[]{ 1.0f, 1.0f, 1.0f, 1.0f}, 0);
	
	}
	
	// Lowers lightning strike intensity and finally removes light source
	public static void lightningStrikeFade( GL10 glParam, float[] lightning ) {
		if ( lightning[0] > 0.0f ) {
			glParam.glLightfv(GL10.GL_LIGHT2, GL10.GL_DIFFUSE, new float[]{ 1.0f, 1.0f, 1.0f, lightning[0] }, 0);
			lightning[0] -= 0.1f;
		} else {
			// Lightning now free to strike again
			lightning[1] = 1;
			// Remove light source from world
			glParam.glDisable(GL10.GL_LIGHT2);
		}
	}
	
	/*
	 * Local and character-related lighting
	 * 
	 */
	
	// Set up light source that follows player's position
	public static void playerLight( Player player, GL10 gl ) {
		gl.glLightfv(GL10.GL_LIGHT0, GL10.GL_POSITION, new float[]{player.position.x-2, player.position.y+5, player.position.z+2, 1}, 0);
	}
	
	

}
