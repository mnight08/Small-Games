package mod2;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g3d.loaders.wavefront.ObjLoader;
import com.badlogic.gdx.graphics.g3d.materials.Material;
import com.badlogic.gdx.graphics.g3d.materials.TextureAttribute;
import com.badlogic.gdx.graphics.g3d.model.Animation;
import com.badlogic.gdx.graphics.g3d.model.still.StillModel;
import com.badlogic.gdx.math.collision.BoundingBox;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.math.MathUtils;
public class Static_Model {
	StillModel model;
	Texture texture = null;
	boolean hasNormals = false;
	//This is the angle rotated around the direction vector
	float angle = 0;
	float azimuth=0;
	float elevation=0;

	Vector3 scale;
	Game game;
	//The position of the model
	Vector3 position;
	//The direction the model will be facing.  Handles two angles.  Should be a unit vector
	Vector3 direction;
	public Static_Model (Game game,String fileName, String textureFileName,Vector3 position,Vector3 scale,Vector3 direction,float angle) {
	this.game=game;
	this.direction=new Vector3(direction);
	this.position=position;
	this.scale=scale;
	 texture = new Texture(Gdx.files.internal(textureFileName), true);
	 ObjLoader Load=new ObjLoader();
	 model=Load.loadObj(Gdx.files.internal(fileName),true);
	 model.setMaterial(new Material("default", new TextureAttribute(texture, 0, "skin")));
	}
	
	public void render(){
		//i dont conpletely understand this
		//Save camera matrix
		Gdx.gl10.glPushMatrix();
		//translate to the position the model will be rendered at.  It will be rendered in local coordinates. 
		Gdx.gl10.glTranslatef(position.x,position.y, position.z);
		//rescale the model		
		Gdx.gl10.glScalef(scale.x, scale.y, scale.z);
		//rotate the world around the model
		Gdx.gl10.glRotatef(angle, direction.x, direction.y, direction.z);
		Gdx.gl10.glRotatef(azimuth,1,0,0);
		Gdx.gl10.glRotatef(elevation,0,1,0);
		model.render();
		//reload camera matrix
		Gdx.gl10.glPopMatrix();
	}
}
