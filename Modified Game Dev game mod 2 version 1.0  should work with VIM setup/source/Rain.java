package mod2;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Random;

import com.badlogic.gdx.math.Vector3;

public class Rain {
	//List<RainDrop> rainPool;
	List<RainDrop> activeRain;
	List<RainDrop> restartlist;
	//intensity of rain raining 0 to 1
	int numberofraindrops;
	Vector3 center;
	float range;
	Random random;
	long lastframetime;
	float currentTime;
	
	
	//This class randomly generates rain around the point center, with range 
	public Rain(Vector3 cloudcenter,float cloudrange, int amountofraindrops )
	{
		random= new Random();
		center=cloudcenter;
		range=cloudrange;
		numberofraindrops=amountofraindrops;
		//rainPool=new LinkedList<RainDrop>();
		activeRain=new LinkedList<RainDrop>();
		restartlist=new LinkedList<RainDrop>();
		
		
		for( int i=0;i<amountofraindrops;i++)
			activeRain.add(new RainDrop(3,3));
		
		RainDrop raindrop;
		for(Iterator<RainDrop> it=activeRain.iterator();it.hasNext();)
		{
			raindrop=it.next();
			getRandomRainDrop(raindrop);
		}

		lastframetime=System.nanoTime();
	}
	
	

	public void respawnDrops()
	{
		RainDrop raindrop;
		//System.out.println(respawned+ " many drops respawned ");
		//	System.out.println("before:" +rainPool.size());
		for(Iterator<RainDrop> it=restartlist.iterator();it.hasNext();)
		{
			raindrop=it.next();
			getRandomRainDrop(raindrop);
			//activeRain.remove(raindrop);
		}
		restartlist.clear();
		
	}
	public void clearOldDrops()
	{
		RainDrop raindrop;
		for(Iterator<RainDrop> it=activeRain.iterator();it.hasNext();)
		{
			raindrop=it.next();
			if((raindrop.position.y<22))
			{
				//System.out.println("raindropnottoolow");
				restartlist.add(raindrop);

			}
		}	
	}
	public void update(float delta)
	{
		
		RainDrop raindrop;
		
		for(Iterator<RainDrop> it=activeRain.iterator();it.hasNext();)
		{
			
			raindrop=it.next();
			raindrop.update(delta);
		}


	}
	public void render()
	{
		float delta=(float)(System.nanoTime()-lastframetime)/(float)1000000000;
		clearOldDrops();
		respawnDrops();
		currentTime+=delta;
		//System.out.println(activeRain.size());
		update(delta);
		//System.out.println("Rendering");
		//System
		RainDrop raindrop;
		for(Iterator<RainDrop> it=activeRain.iterator();it.hasNext();)
		{
			raindrop=it.next();
			//System.out.println(raindrop.position);
			raindrop.render();
		}
		
	}
	
	public void getRandomRainDrop(RainDrop raindrop)
	{
		//System.out.println(center);
		float elevation=(float) (random.nextDouble()*Math.PI);
		float azimuth= (float)(random.nextDouble()*2*Math.PI);
		float rho=(float)(random.nextDouble()*range);
		float raindropsize=(float)(random.nextDouble())*.2f+.2f;
		float gravity=-(float)(random.nextDouble())*.0002f;///-.0005f;
		//System.out.println(rho+" "+azimuth+" "+elevation);

		raindrop.SetStartPosition(center.cpy().add(new Vector3((float)(rho*Math.cos(elevation)*Math.sin(azimuth)),(float)(rho*Math.cos(azimuth)),(float)(rho*Math.sin(elevation)*Math.sin(azimuth)))));

		raindrop.scaleRainDrop((float) raindropsize);
		raindrop.restart();
		raindrop.flip();
		raindrop.SetAcceleration(new Vector3(0,gravity,0));
		//System.out.println(raindrop.position);
		//return raindrop;
		
	}
	
	
}
