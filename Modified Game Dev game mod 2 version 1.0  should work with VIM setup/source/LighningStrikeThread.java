package mod2;

import com.badlogic.gdx.graphics.GL10;

public class LighningStrikeThread implements Runnable {
	GL10 glT;
	
	public LighningStrikeThread(GL10 glP) {
		glT = glP;
	}

	@Override
	public void run() {
		float intensity = 1.0f;
		
		// Test if glT is null
		if (glT == null) {
			System.out.println("null");
		}
		
		// Set up lightning light source
		glT.glEnable(GL10.GL_LIGHT2);
		glT.glLightfv(GL10.GL_LIGHT2, GL10.GL_AMBIENT, new float[]{ 1.0f, 1.0f, 1.0f, 1f }, 0);
		glT.glLightfv(GL10.GL_LIGHT2, GL10.GL_DIFFUSE, new float[]{ 1.0f, 1.0f, 1.0f, 1.0f }, 0);
		// Strike at random position
		glT.glLightfv(GL10.GL_LIGHT2, GL10.GL_POSITION, new float[]{ 1.0f, 1.0f, 1.0f, 1.0f}, 0);
		
		try {
			while ( intensity > 0.0f ) {
				glT.glLightfv(GL10.GL_LIGHT2, GL10.GL_DIFFUSE, new float[]{ 1.0f, 1.0f, 1.0f, intensity }, 0);
				intensity -= 0.01f;
				//wait(100);
				System.out.println(intensity);
			}
		//} catch (InterruptedException e) {
			// Do nothing
		} finally {
			glT.glDisable(GL10.GL_LIGHT2);
		}
		
	}
	
}
