package mod2;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g3d.loaders.wavefront.ObjLoader;
import com.badlogic.gdx.graphics.g3d.materials.Material;
import com.badlogic.gdx.graphics.g3d.materials.TextureAttribute;
import com.badlogic.gdx.graphics.g3d.model.Animation;
import com.badlogic.gdx.graphics.g3d.model.still.StillModel;
import com.badlogic.gdx.graphics.glutils.ImmediateModeRenderer10;
import com.badlogic.gdx.math.collision.BoundingBox;
import com.badlogic.gdx.math.Vector3;
public class Collidable_Model {
	StillModel model;
	Texture texture = null;
	boolean hasNormals = false;
	BoundingBox bounds = new BoundingBox();
	ImmediateModeRenderer10 renderer;
	float angle = 0;
	public float extrarot=0;
	boolean RIGHT;
	boolean NORTH=true;
	public int counter=0;
	float resize=0;
	Animation anim;
	float animTime = 0;
	Vector3 position=new  Vector3(0,0,0);
	public float anglex;
	public float angley;
	public float anglez;
	public float rotate;
	Game game;
	public Collidable_Model (Game game,String fileName, String textureFileName,Vector3 pos,int resize,float rotate,float anglex,float angley,float anglez) {
	this.game=game;
	this.rotate=rotate;
	this.anglex=anglex;
	this.angley=angley;
	this.anglez=anglez;

	this.resize=resize;
	 texture = new Texture(Gdx.files.internal(textureFileName), true);
	 ObjLoader Load=new ObjLoader();
	 model=Load.loadObj(Gdx.files.internal(fileName),true);
	 model.setMaterial(new Material("default", new TextureAttribute(texture, 0, "skin")));
	 model.getBoundingBox(bounds);
	 
	
	 
	}
	
	public void render(){
		
		Gdx.gl10.glPushMatrix();
		//System.out.println(game.player.angle);
		Gdx.gl10.glTranslatef(position.x,position.y, position.z);
		Gdx.gl10.glScalef(resize, resize, resize);
		Gdx.gl10.glRotatef(rotate, anglex, angley, anglez);
		model.render();
		Gdx.gl10.glPopMatrix();
		
	}
	public void renderplay(){
		if(RIGHT){
		rotate+=1;
		}else{
		rotate-=1;
		}
		if(rotate>=20){
		RIGHT=false;
		}else if(rotate<=-20){
		RIGHT=true;
		}
		counter++;
		if(counter>=100){
		counter=0;
		if(NORTH){
			NORTH=false;
			extrarot=180;
		}else{
			NORTH=true;
			extrarot=0;
		}
		}
		if(NORTH){
		position.z+=.5;
		position.x+=.5;
		}else{
		position.z-=.5;
		position.x-=.5;
		}
	
		Gdx.gl10.glPushMatrix();
		
		Gdx.gl10.glTranslatef(position.x,position.y,position.z);
		Gdx.gl10.glScalef(resize, resize, resize);
		Gdx.gl10.glRotatef(rotate+extrarot, 0, 1, 0);
		model.render();
		Gdx.gl10.glPopMatrix();
	}
	
	

	
}
