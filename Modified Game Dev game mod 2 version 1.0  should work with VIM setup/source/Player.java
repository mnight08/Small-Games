package mod2;

import java.io.InputStream;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GL10;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.Pixmap.Format;
import com.badlogic.gdx.graphics.g3d.loaders.ModelLoaderRegistry;
import com.badlogic.gdx.graphics.g3d.loaders.md2.MD2Loader;
import com.badlogic.gdx.graphics.g3d.materials.Material;
import com.badlogic.gdx.graphics.g3d.materials.TextureAttribute;
import com.badlogic.gdx.graphics.g3d.model.keyframe.KeyframedAnimation;
import com.badlogic.gdx.graphics.g3d.model.keyframe.KeyframedModel;
import com.badlogic.gdx.graphics.glutils.ImmediateModeRenderer10;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.math.collision.BoundingBox;

public class Player {
	public float rotH,rotV;
	KeyframedModel model;
	Texture texture = null;
	boolean hasNormals = false;
	BoundingBox bounds = new BoundingBox();
	ImmediateModeRenderer10 renderer;
	float angle = 0;
	float scale;
	public boolean TEXT;
	KeyframedAnimation anim;
	float animTime = 0;
	Vector3 position=new Vector3(0,0,0);
	public int length;
	public float rotated;
	Vector3 direction;
	Game game;
	public Player (Game game,String fileName, String textureFileName,Vector3 pos,int numberofanimation,float rotated,float scale) {
	this.game=game;
	position=pos;
	this.scale=scale;
	this.rotated=rotated;
	direction= new Vector3(0,0,1);
	model = ModelLoaderRegistry.loadKeyframedModel(Gdx.files.internal(fileName));
	if (textureFileName != null) texture = new Texture(Gdx.files.internal(textureFileName), Format.RGB565, true);
	model.setMaterial(new Material("default", new TextureAttribute(texture, 0, "skin")));
	 model.getBoundingBox(bounds);
	 anim=model.getAnimations()[numberofanimation];
	 
	}
	public void rendernpc(){
		animTime += Gdx.graphics.getDeltaTime();
		
		if (animTime >= anim.totalDuration) {
			animTime = 0;
		}
		model.setAnimation(anim.name, animTime, false);
		
		Gdx.gl10.glPushMatrix();
	
		Gdx.gl10.glTranslatef(position.x,position.y,position.z);
		Gdx.gl10.glRotatef(rotated, 0, 1, 0);
		Gdx.gl10.glScalef(scale,scale,scale);
		
	
		Gdx.gl10.glFrontFace(GL10.GL_CW);
		model.render();
		Gdx.gl10.glFrontFace(GL10.GL_CCW);
	
		Gdx.gl10.glPopMatrix();
	}
	public void Detectnear(Player player,Player npc){
		if(player.position.z>npc.position.z-3 && player.position.z<npc.position.z+3 && player.position.x<npc.position.x && player.position.x>npc.position.x-50){
			TEXT=true;
		}else{
			TEXT=false;
		}
	}
	public void render(){
	
		Vector3 playernormal=(new Vector3(0,1,0));
		float angle=(float)( 180/Math.PI*Math.acos(direction.dot(new Vector3(0,0,1))));
		
		//If the player is in the second or third quadrant, negate the angle.  That is, if the angle is below the x axis we need to add pi  
		//according to arctan formula
		if(direction.x<=0&&direction.z>0||direction.x<0&&direction.z<=0){
			angle=-angle;
		}
		
		animTime += Gdx.graphics.getDeltaTime();
		
		if (animTime >= anim.totalDuration) {
			animTime = 0;
		}
		model.setAnimation(anim.name, animTime, false);
		
		Gdx.gl10.glPushMatrix();
		Gdx.gl10.glTranslatef(position.x,position.y,position.z);
		Gdx.gl10.glRotatef(angle, playernormal.x, playernormal.y, playernormal.z);
		Gdx.gl10.glScalef(scale,scale,scale);

	
		Gdx.gl10.glFrontFace(GL10.GL_CW);
		model.render();
		Gdx.gl10.glFrontFace(GL10.GL_CCW);
	
		Gdx.gl10.glPopMatrix();
		
	}
	public void update(){
		if(game.NORTH){
			position.z+=1;
		}
	}
	
	public void setAnimation( int numberOfAnimation ) {
		anim=model.getAnimations()[numberOfAnimation];
	}

	
}
