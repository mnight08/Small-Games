package mod2;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Random;

import com.badlogic.gdx.math.Vector3;

public class Storm {
	Waves waves;
	//Rain rain;
	float intensity;
	List<Rain> clouds;
	
	public Storm()
	{

		waves= new Waves();
		waves.Translate(new Vector3(80,7,140));
		
		waves.create(50, 50, 250, 250);
		waves.addWave(0f, (new Vector3(1,0,0)).nor(), 5, (float) 3, (float) 3);
		waves.addWave(0f, (new Vector3(-1,0,1)).nor(),25, (float) .01, (float) 5);
		waves.addWave(0f, (new Vector3(0,0,-1)).nor(), 5, (float) .1, (float) 3);
		//waves.addWave(0f, (new Vector3(.6f,0,.6f)).nor(), 15, (float) .1, (float) 3);
		clouds=new LinkedList<Rain>();
		float xsample=5;
		float xstep=((30+50)-(615-50))/xsample;
		float zsample=5;
		float zstep=((604-50)-(28+50))/zsample;
		for( float x=615-50;x>30+50;x+=xstep)
		{
			for( float z=30+50;z<604-50;z+=zstep)
			{
				clouds.add(new Rain(new Vector3(x,80,z),50,60));
			}
		}
//		Random random= new Random(1213);
//		float xrange=600;
//		float zrange=600;
//		float x=0;//(float) (30+(xrange/2-random.nextDouble()*xrange));
//		float z=0;//(float) (604+(zrange/2-random.nextDouble()*zrange));
		//for(int i=0;i<20;i++)
		//{
	//		x=(float) (30+(xrange/2-random.nextDouble()*xrange));
	//		z=(float) (604+(zrange/2-random.nextDouble()*zrange));
	//		waves.addRipple(x,z,100);
		//}
//		x=110;
//		z=597;
		//waves.addRipple(x,z,100);
		
		
	}
	
	public void render()
	{
		waves.render();
		for(Iterator<Rain> it=clouds.iterator();it.hasNext(); (it.next()).render());
			
		
	}
}
