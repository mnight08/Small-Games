package mod2;

import com.badlogic.gdx.math.Vector3;

public class Wave {
	//should vary from 0 to 1
	float steepness;
	Vector3 direction;
	float amplitude;
	float frequency;
	float wavespeed;
	float currenttime;
	
	public Wave(float s,Vector3 dir, float amp,float freq,float ws)
	{
		currenttime=0;
		steepness=s;
		direction=new Vector3(dir);
		amplitude=amp;
		frequency=freq;
		wavespeed=ws;
	
	}
	public void updateWave(float delta)
	{
		currenttime+=delta;
	}
	public Vector3 getPositionOnWave(float x, float z,int numWaves)
	{
	float qi=steepness/(frequency*amplitude*numWaves);
		
		//return new Vector3(x,(float)Math.sin(frequency*currenttime),z);
		return new Vector3((float)(qi*amplitude*direction.x*Math.cos(frequency*(direction.x*x+direction.z*z)+wavespeed*currenttime)),
				  (float)(amplitude*Math.sin((frequency*(direction.x*x+direction.z*z)+wavespeed*currenttime))/(2*Math.PI)),
					 (float)(qi*amplitude*direction.z*Math.cos(frequency*(direction.x*x+direction.z*z)+wavespeed*currenttime)));
	}

}
