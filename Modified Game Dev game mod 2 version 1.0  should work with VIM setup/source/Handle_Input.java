package mod2;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.Input.Buttons;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.math.Vector3;

public class Handle_Input{

	//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!1111
	//This should be broken down to a set of specific functions that specify what is begin handled.
	public static void handleInput(Game game,Input input, float delta)
	{
		Vector3 pos=new Vector3(game.player.position.x,game.player.position.y,game.player.position.z);
		Vector3 movementdirection;
		if(input.isKeyPressed(Keys.T)){
		
		if(game.TALKING){
		game.TALKING=false;
		
		}else{
		game.TALKING=true;	
		}
		try {
			Thread.sleep(100);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		}
		if(game.TALKING && game.player.TEXT){
			
		}else{
		
		if( input.isButtonPressed( Buttons.LEFT ) ) {
			
			game.cameraRotH -= input.getDeltaX()/10.0f;
			game.cameraRotH %= 360.0f;
			game.player.angle-=input.getDeltaX()/10.0f;
			game.player.angle %= 360.0f;
			game.cameraRotV -= input.getDeltaY()/10.0f;
			if( game.cameraRotV > 90.0f ) {
				game.cameraRotV = 90.0f;
			}
			else if( game.cameraRotV < -90.0f ) {
				game.cameraRotV = -90.0f;
			}
		}
		if ( input.isKeyPressed( Keys.W ) || input.isKeyPressed( Keys.A ) || 
				input.isKeyPressed( Keys.S ) || input.isKeyPressed( Keys.D ) ) {
			// Set running animation
			game.player.setAnimation(4);
		} else {
			// Stop player animation after all input is processed
			game.player.setAnimation(11);
		}
		if( input.isKeyPressed( Keys.W ) ){
			game.player.position.x += game.camera.direction.x*2;
			game.player.position.z += game.camera.direction.z*2;
		}
		
		if( input.isKeyPressed( Keys.S ) ){
			game.player.position.x-= game.camera.direction.x*2;
			game.player.position.z -= game.camera.direction.z*2;
		}
		
		if( input.isKeyPressed( Keys.A ) ){
			movementdirection=game.camera.direction.cpy().crs(game.camera.up).mul(-1);
			game.player.position.x=game.player.position.x+movementdirection.x;
			game.player.position.z=game.player.position.z+movementdirection.z;
		}
		
		if( input.isKeyPressed( Keys.D ) ){
			movementdirection=game.camera.direction.cpy().crs(game.camera.up).mul(1);
			game.player.position.x = game.player.position.x +movementdirection.x;
			game.player.position.z=game.player.position.z+movementdirection.z;
		
		
		}		
		if( input.isKeyPressed( Keys.U ) ){
			game.camera.position.add( game.camera.direction.cpy().mul( 1.0f ) );
			}
		
		if( input.isKeyPressed( Keys.H ) ){
			game.cameraangle+=.05;
		}
		
		if( input.isKeyPressed( Keys.K ) ){
			game.cameraangle-=.05;//game.camera.position.add( game.camera.direction.cpy().crs( game.camera.up ).mul( 1.0f ) );
		}
		
		if( input.isKeyPressed( Keys.J ) ){
			game.camera.position.add( game.camera.direction.cpy().mul( -1.0f ) );
		}
		
		
		if( input.isKeyPressed( Keys.UP ) ){
			game.camera.lookAt(game.player.position.x, game.player.position.y, game.player.position.z);
			game.camera.position.add( game.camera.direction.cpy().mul( 1.0f ) );
			game.cameradistance=(float) Math.sqrt((game.camera.position.x-game.player.position.x)*(game.camera.position.x-game.player.position.x)
					+(game.camera.position.z-game.player.position.z)*(game.camera.position.z-game.player.position.z));
			}
		
		if( input.isKeyPressed( Keys.LEFT ) ){
			game.cameraangle+=.05;
		}
		
		if( input.isKeyPressed( Keys.RIGHT) ){
			game.cameraangle-=.05;//game.camera.position.add( game.camera.direction.cpy().crs( game.camera.up ).mul( 1.0f ) );
		}
		
		if( input.isKeyPressed( Keys.DOWN ) ){
			game.camera.lookAt(game.player.position.x, game.player.position.y, game.player.position.z);
			game.camera.position.add( game.camera.direction.cpy().mul( -1.0f ) );
			game.cameradistance=(float) Math.sqrt((game.camera.position.x-game.player.position.x)*(game.camera.position.x-game.player.position.x)
					+(game.camera.position.z-game.player.position.z)*(game.camera.position.z-game.player.position.z));
		}
		}
		
		// Lightning strike
		if( input.isKeyPressed( Keys.L ) )
		{
			game.lightning[1] = 1;
		}
		
		//game.cameradistance=(float) Math.sqrt((game.camera.position.x-game.player.position.x)*(game.camera.position.x-game.player.position.x)
		//+(game.camera.position.z-game.player.position.z)*(game.camera.position.z-game.player.position.z));
		
		game.relativeCameraPosition.x=(float) (Math.sin(game.cameraangle)*game.cameradistance);
		game.relativeCameraPosition.z=(float) (Math.cos(game.cameraangle)*game.cameradistance);
		// set a nice initial view
		game.camera.position.set(game.relativeCameraPosition.x+ game.player.position.x, game.player.position.y+10,game.relativeCameraPosition.z+ game.player.position.z );
	
		
	};

}
