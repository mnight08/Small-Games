package mod2;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL10;
import com.badlogic.gdx.graphics.Mesh;
import com.badlogic.gdx.graphics.VertexAttribute;
import com.badlogic.gdx.graphics.VertexAttributes.Usage;
import com.badlogic.gdx.math.Vector3;

public class RainDrop {
	Vector3 startPosition;
	Vector3 velocity;
	Vector3 position;
	Vector3 acceleration;
	float maxvelocity;
	//float currentTime;
	int numberOfVertices;
	int numberOfIndices;
	int ySamples;
	int angleSamples;
	float[] vertices;
	short[] indices;
	float scale;
	float center;
	float width;
	
	Mesh mesh;
	
	//each raindrop is given by a circle for a fixed y. 
	//equation for the raindrop is given by p(t,y)=<f(y)sin(t),y,f(y)cos(t)>.
	//f(y) will be some probability distribution.  it will allow giving the raindrops different shapes easily.
	//i decided to use lognormal distribution cause of its shape.  center is the mean, width is standard deviation
	//after creating the shape shape of the rain drop each point must be scaled to the proper size
	//partials are Dy(p)=
	public RainDrop(int angleSampleCount,int ySampleCount)
	{
		angleSamples=angleSampleCount;
		ySamples=ySampleCount;
		
		numberOfVertices=ySamples*angleSamples;

		numberOfIndices=(2*angleSamples-1)*(ySamples-1)+1;

		mesh = new Mesh( true, numberOfVertices, numberOfIndices,
							new VertexAttribute(Usage.Position, 3, "wave_position"),
							new VertexAttribute(Usage.Normal, 3, "wave_normal"),
							new VertexAttribute(Usage.ColorPacked,4,"color")
							/*new VertexAttribute(Usage.TextureCoordinates,2,"uv")*/
						);
		
		vertices=new float[mesh.getMaxVertices()*mesh.getVertexSize()/4];
		indices=new short[mesh.getMaxIndices()];
		
		startPosition= new Vector3();
		velocity= new Vector3();
		position= new Vector3();
		acceleration= new Vector3();
		maxvelocity= .4f;
		scale=1;
		mesh.setAutoBind(true);
		buildShape();
		flip();
		
	}
	
	public void SetAcceleration(Vector3 a)
	{
		acceleration=a.cpy();
	}
	
	public void SetStartPosition(Vector3 p)
	{
		startPosition=p;
	}
	
	public void SetMaxVelocity(float max)
	{
		
		maxvelocity=max;
	}
	
	//move the shape from the current location to the start position
	public void restart()
	{
		
		for(int vertex=0;vertex<numberOfVertices;vertex++)
		{
			setPositionInBuffer(vertex,getPositionInBuffer(vertex).sub(position).add(startPosition));
			velocity.x=0;
			velocity.y=0;
			velocity.z=0;
			acceleration.x=0;
			acceleration.y=0;
			acceleration.z=0;
			//System.out.println("reset position "+ getPositionInBuffer(vertex));
		}
		position=startPosition;

	}
	
	public void buildShape()
	{
		//System.out.println("building shape");
		buildVertices();
		buildIndices();
	}
	
	
	//scales the original raindrop to the given scale
	public void scaleRainDrop(float size)
	{
		
		for(int vertex=0;vertex<numberOfVertices;vertex++)
		{
			setPositionInBuffer(vertex,getPositionInBuffer(vertex).mul(size*(1/scale)));
		}
		scale=size;
		
	}
	
	public void buildIndices()
	{

		boolean up=false;
		short y=0;
		short workingy=0;
		short angle=0;
		short workingangle=0;
	//	System.out.println("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");
		for( short currentIndex=0;currentIndex<numberOfIndices;currentIndex++)
		{
				//working row should never get to yGridNumPoints-1
				if(workingangle>=angleSamples)
				{
					workingangle=0;
					workingy++;
					up=true;
				}

				angle=workingangle;
				
				if(up)
				{
					y=(short) (workingy+1);
					
						workingangle++;
					
				}
				else
				{
					y=workingy;
					
				}
				
				up=!up;
				

				
				indices[currentIndex]=getVertexNumber( angle, y);
				//System.out.println("angle "+angle+" y "+y);
		}
		mesh.setIndices(indices);	
	}
	
	//only for y in range [0,1]  got the function by trial and error and wolfram alpha.  probably spent too much time on it
	//y should be from [0,1]
	public float Radius(float y)
	{
		return (y*(y-1)*(y-4))/(y+1);
		
	}
	
	//build vertices at the origin
	public void buildVertices()
	{
		//currentTime+=delta;
		float angle;
		float y;
		Vector3 position;
		for( int vertex=0;vertex<numberOfVertices;vertex++)
		{
			angle=getVertexAngle(vertex);
			y=getVertexY(vertex);
			position=new Vector3((float)(Radius(y)*Math.sin(angle)),y,(float)(Radius(y)*Math.cos(angle)));
		//	System.out.println(position);
			setPositionInBuffer(vertex,position);
			setNormalInBuffer(vertex,getNormal(vertex));
			setColorInBuffer(vertex,1, 0, 0, .4f);
		}
		
	}
	
	public void render()
	{
		//printVertexParameters();
		//printVertexPositions();
		mesh.render(GL10.GL_TRIANGLE_STRIP);
	}
	
	public void update(float delta)
	{
		Vector3 vertexPosition;
//System.out.println(delta);
		if(velocity.len()<maxvelocity)
		{
			velocity.add(acceleration.cpy().mul(delta));
		}
		position.add(velocity.cpy().mul(delta));
		for( int vertex=0;vertex<numberOfVertices;vertex++)
		{
			vertexPosition=getPositionInBuffer(vertex);
			vertexPosition.add(velocity.cpy().mul(delta));
			
			//System.out.println(position);
			setPositionInBuffer(vertex,vertexPosition);
			setNormalInBuffer(vertex,getNormal(vertex));
			setColorInBuffer(vertex,1f,0f,0f,.4f);//*/
		}
		
		
		
		flip();
	}
	
	public void flip()
	{
		mesh.setVertices(vertices);
	}
	
	//moves the vertices in buffer to new position
	public void setPositionInBuffer(int vertex,Vector3 here)
	{
		vertices[vertex*mesh.getVertexSize()/4]=here.x;
		vertices[vertex*mesh.getVertexSize()/4+1]=here.y;
		vertices[vertex*mesh.getVertexSize()/4+2]=here.z;
	
	}
	

	public void setColorInBuffer(int vertex,float r, float g, float b, float a)
	{
		vertices[vertex*mesh.getVertexSize()/4+6]=Color.toFloatBits(r, g, b, a);
	
	}
	
	//moves the vertices in buffer to new position
	public void setNormalInBuffer(int vertex,Vector3 normal)
	{
		vertices[vertex*mesh.getVertexSize()/4+3]=normal.x;
		vertices[vertex*mesh.getVertexSize()/4+4]=normal.y;
		vertices[vertex*mesh.getVertexSize()/4+5]=normal.z;
	}
	
	
	public void printVertexParameters()
	{
		for(int vertex=0;vertex<numberOfVertices;vertex++)
		{
			System.out.println("angle0 "+getVertexAngle(vertex)+" Y "+getVertexY(vertex));
		}
	}
	public void printVertexPositions()
	{
		for(int vertex=0;vertex<numberOfVertices;vertex++)
		{
			Vector3 pos=getPositionInBuffer(vertex);
			System.out.println("x "+pos.x+" y "+pos.y+ " z "+pos.z);
		}
	}
	
	public void setUVInBuffer(int vertex, float U, float V)
	{
		vertices[vertex*mesh.getVertexSize()/4+6]=U;
		vertices[vertex*mesh.getVertexSize()/4+7]=V;
	}
	

	public Vector3 getPositionInBuffer(int vertex)
	{
		//System.out.println(vertex);
		//System.out.println(vertices.length);

		return new Vector3(	vertices[vertex*mesh.getVertexSize()/4],vertices[vertex*mesh.getVertexSize()/4+1],vertices[vertex*mesh.getVertexSize()/4+2]);
	}
	
	public float getVertexAngle(int vertex)
	{
		return (float) ((2*Math.PI/(float)angleSamples)*(vertex%angleSamples));
	}
	
	public float getVertexY(int vertex)
	{
		return (vertex/angleSamples)/((float)ySamples);
	}
	
	public Vector3 getNormalInBuffer(int vertex)
	{
		return new Vector3(	vertices[vertex*mesh.getVertexSize()/4+3],vertices[vertex*mesh.getVertexSize()/4+4],vertices[vertex*mesh.getVertexSize()/4+5]);
	}
	public short getVertexNumber(short angleSample,short ySample)
	{
		return (short) (ySample*angleSamples+angleSample);
		
	}
	
	//derivative of radius
	public float RadiusDerivative(float y)
	{
		return (2*y*y*y-10*y*y-9*y+1)/((y+1)*(y+1));
		
	}
	
	//if you want the  right normals, you need to update vertices in buffer before calling this
	public Vector3 getNormal(int vertex)
	{
		float angle=getVertexAngle(vertex);
		float Y=getVertexY(vertex);
		return new Vector3((float)(-Math.sin(angle)*Radius(Y)),Radius(Y)*RadiusDerivative(Y),(float)(-Radius(Y)*Math.cos(angle)));
		
			
	}
	
}
