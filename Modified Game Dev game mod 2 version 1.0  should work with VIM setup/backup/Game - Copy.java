package mod2;

import com.badlogic.gdx.ApplicationListener;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.Camera;
import com.badlogic.gdx.graphics.GL10;
import com.badlogic.gdx.graphics.Mesh;
import com.badlogic.gdx.graphics.VertexAttribute;
import com.badlogic.gdx.graphics.VertexAttributes.Usage;
import com.badlogic.gdx.graphics.PerspectiveCamera;
import com.badlogic.gdx.graphics.Texture;

import com.badlogic.gdx.math.Vector3;

import com.badlogic.gdx.Input;
import com.badlogic.gdx.Input.Buttons;
import com.badlogic.gdx.Input.Keys;

public class Game implements ApplicationListener {
	
	//camera and its properties
	private Camera camera;
	float cameraRotH, cameraRotV;


	Mesh mesh;
	Texture texture;
	
	
	//Sound variables
	Music music, rain;
	Sound crash;


	boolean TALKING;
	boolean NORTH=false;
	long lastTime = System.nanoTime();
	
	//Models.  
	Player player;
	Player CyberDemon;
	Objects house[]=new Objects[1];
	Objects trees[] = new Objects[10];
	
	//Probably gonna scrap this for vector
	float xpos=0;
	float ypos=0;
	float zpos=0;

	//This is probably nonsense
	int mov=70;
	int movz=0;
	int i = 0;


	GL10 gl;
	float sunPos[];
	float lightning[];
	Objects Shark;
	float cameraangle;
	float cameradistance;
	Vector3 relativeCameraPosition;
	
	Objects Tree2;
	Objects Cabbage[]=new Objects[40];
	
	Storm storm;
	Objects Wall[]=new Objects[45];
//	RainDrop raindrop;
	Objects Text;
	@Override
	public void create() {
		HeightMap hm = new HeightMap( "resources/heightmapnew2.bmp", 20 );		
		TerrainChunk chunk = new TerrainChunk( hm, 5.0f, 4.0f, 12.0f );

		mesh = new Mesh( true, chunk.m_vertices.length / 8, chunk.m_indices.length,
							new VertexAttribute(Usage.Position, 3, "a_position"),
							new VertexAttribute(Usage.Normal, 3, "a_normal"),
							new VertexAttribute(Usage.TextureCoordinates, 2, "a_texCoords")
				);
		
		Load_Models();
		
		cameraangle=(float) (3*Math.PI/2);
		cameradistance=20;
		relativeCameraPosition= new Vector3(0,0,0);
		
		player=new Player(this,"resources/knight.md2", "resources/knight.jpg",110,20,110,11,0,.1f);
		CyberDemon=new Player(this,"resources/Archvile/Archvile.md2", "resources/Archvile/archvile.png",100,17,450,0,270,.12f);
		// idle 11
		// run 4

		storm= new Storm();
		
		FileHandle imageFileHandle = Gdx.files.internal( "resources/grass.jpg" ); 
		texture = new Texture( imageFileHandle, true );
        	texture.bind();

        	// Load background rain audio
        	FileHandle soundFileHandle = Gdx.files.internal( "music/rain.mp3" );
        	rain = Gdx.audio.newMusic(soundFileHandle);
        	rain.play();
        	rain.setLooping(true);
        	rain.setVolume(0.25f);
        
        	// Load background music
        	FileHandle musicFileHandle = Gdx.files.internal( "music/music.mp3" );
        	music = Gdx.audio.newMusic(musicFileHandle);
        	music.play();
        	music.setLooping(true);
        	music.setVolume(1.0f);
        
        	// Lightning crash sound effect
        	FileHandle crashFileHandle = Gdx.files.internal( "music/crash.mp3" );
		crash = Gdx.audio.newSound(crashFileHandle);
		
		mesh.setVertices(chunk.m_vertices);
		mesh.setIndices(chunk.m_indices);

		gl = Gdx.graphics.getGL10();
		gl.glTexEnvf( GL10.GL_TEXTURE_ENV, GL10.GL_TEXTURE_ENV_MODE, GL10.GL_MODULATE );
		// when texture area is small, bilinear filter the closest mipmap
		gl.glTexParameterf( GL10.GL_TEXTURE_2D, GL10.GL_TEXTURE_MIN_FILTER,
			GL10.GL_LINEAR_MIPMAP_NEAREST );
		// when texture area is large, bilinear filter the original
		gl.glTexParameterf( GL10.GL_TEXTURE_2D, GL10.GL_TEXTURE_MAG_FILTER, GL10.GL_LINEAR );

		// the texture wraps over at the edges (repeat)
		gl.glTexParameterf( GL10.GL_TEXTURE_2D, GL10.GL_TEXTURE_WRAP_S, GL10.GL_REPEAT );
		gl.glTexParameterf( GL10.GL_TEXTURE_2D, GL10.GL_TEXTURE_WRAP_T, GL10.GL_REPEAT );
		gl.glEnable( GL10.GL_TEXTURE_2D );
		gl.glShadeModel( GL10.GL_SMOOTH );
		gl.glEnable( GL10.GL_DEPTH_TEST );
		gl.glEnable( GL10.GL_CULL_FACE );
	
		gl.glEnable(GL10.GL_LIGHTING);
		// Character lighting
		gl.glEnable(GL10.GL_LIGHT0);
		// Sun
		gl.glEnable(GL10.GL_LIGHT1);

		// Set up character light source
		gl.glLightfv(GL10.GL_LIGHT1, GL10.GL_AMBIENT, new float[]{ 1.0f, 0.2f, 0.2f, 0.1f }, 0);
		// Color is red to simulate torch light
		gl.glLightfv(GL10.GL_LIGHT0, GL10.GL_DIFFUSE, new float[]{ 2f, 1f, 1f, 3f }, 0);
	
		// Set up Sun light source
		gl.glLightfv(GL10.GL_LIGHT1, GL10.GL_AMBIENT, new float[]{ 1.6f, 1.6f, 1.6f, 1f }, 0);
		gl.glLightfv(GL10.GL_LIGHT1, GL10.GL_DIFFUSE, new float[]{ 1.6f, 1.6f, 1.9f, 130f }, 0);
		// Set initial position of Sun
		sunPos = new float[]{ 500, 500, 500 };
		LightingOperations.sunSetting(gl, sunPos);
	
		// Initial setup of lightning strike intensity variable
		lightning = new float[]{ 1.0f, 1 };
		
		gl.glEnable(GL10.GL_BLEND);
	}


	@Override
	public void resize(int width, int height) {
		float aspectRatio = (float) width / (float) height;
		camera = new PerspectiveCamera( 67, 2f * aspectRatio, 2f );
		camera.near = 0.1f;
		camera.far = 500;
		
		relativeCameraPosition.x=(float) (Math.sin(cameraangle)*cameradistance);
		relativeCameraPosition.z=(float) (Math.cos(cameraangle)*cameradistance);
		// set a nice initial view
		camera.position.set(relativeCameraPosition.x+ player.x, player.y+10,relativeCameraPosition.z+ player.z );

	}

	
	@Override
	public void render() {

		// init camera back to "0"
		//camera.direction.set( 0, 0, -1f );
		//camera.up.set( 0, 1f, 0 );
		// and rotate from there
		camera.rotate( cameraRotV, 1.0f, 0.0f, 0.0f );
		camera.rotate( cameraRotH, 0.0f, 1.0f, 0.0f );
		
		// Rotate camera to look at player
		camera.lookAt(player.x, player.y, player.z);

        	camera.apply(Gdx.gl10);
        	Gdx.gl.glClear( GL10.GL_COLOR_BUFFER_BIT | GL10.GL_DEPTH_BUFFER_BIT );

		//Gdx.gl10.glLightfv(GL10.GL_LIGHT0, GL10.GL_POSITION, new float[]{0, 50.0f, 0, 1}, 0);
        
      		LightingOperations.playerLight( player, gl );
        
        	// Sun setting
        	if (System.nanoTime() - lastTime > 1000000) {
        		LightingOperations.sunSetting( gl, sunPos );
        	}
        
        	// Lightning strike
        	if ( System.nanoTime() - lastTime > 1013999999 && lightning[1] == 1 ) {
        		LightingOperations.lightningStrike( gl, lightning );
        		crash.play(1.0f);
        	} else if ( System.nanoTime() - lastTime > 10000 ) {
        		LightingOperations.lightningStrikeFade( gl, lightning );
	        }

		texture.bind();
		
		mesh.render( GL10.GL_TRIANGLE_STRIP );
		
		handleInput(Gdx.input, Gdx.graphics.getDeltaTime());

		if (System.nanoTime() - lastTime > 1000000000) {
			Gdx.app.log("TerrainTest", "fps: " + Gdx.graphics.getFramesPerSecond());
			lastTime = System.nanoTime();
		}
		


		for(int i=0;i<=house.length-1;i++){
			house[i].render();
		}
		for(int i=0;i<Cabbage.length;i++){
			Cabbage[i].render();
				
		}
		Tree2.render();
		for(int i=0;i<Wall.length-1;i++){
			Wall[i].render();	
		}
	
		//gl.glDisable(GL10.GL_TEXTURE_2D);
		//gl.glDisable(GL10.GL_LIGHTING);
		//raindrop.render();
		//storm.render();
		//gl.glEnable(GL10.GL_LIGHTING);
		//gl.glEnable(GL10.GL_TEXTURE_2D);
		
		//System.out.println(player.x+","+player.y+","+player.z);
		
		
		
		
		
		
		storm.render();
		Shark.renderplay();
		CyberDemon.rendernpc();
		player.direction.x=camera.direction.x;
		player.direction.y=0;
		player.direction.z=camera.direction.z;
		player.direction.nor();
		player.render();
		player.update();
		camera.update();
		player.Detectnear(player, CyberDemon);
		if(player.TEXT && TALKING){
		
		Text.render();
		
		}
	//	System.out.println(player.x+ "  "+player.y+"   "+player.z);
	}
	
	private void handleInput ( Input input, float delta ) {
		Vector3 pos=new Vector3(player.x,player.y,player.z);
		Vector3 movementdirection;
		if(input.isKeyPressed(Keys.T)){
		
		if(TALKING){
		TALKING=false;
		
		}else{
		TALKING=true;	
		}
		try {
			Thread.sleep(100);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		}
		if(TALKING && player.TEXT){
			
		}else{
		
		if( input.isButtonPressed( Buttons.LEFT ) ) {
			
			cameraRotH -= input.getDeltaX()/10.0f;
			cameraRotH %= 360.0f;
			player.angle-=input.getDeltaX()/10.0f;
			player.angle %= 360.0f;
			cameraRotV -= input.getDeltaY()/10.0f;
			if( cameraRotV > 90.0f ) {
				cameraRotV = 90.0f;
			}
			else if( cameraRotV < -90.0f ) {
				cameraRotV = -90.0f;
			}
		}
		if ( input.isKeyPressed( Keys.W ) || input.isKeyPressed( Keys.A ) || 
				input.isKeyPressed( Keys.S ) || input.isKeyPressed( Keys.D ) ) {
			// Set running animation
			player.setAnimation(4);
		} else {
			// Stop player animation after all input is processed
			player.setAnimation(11);
		}
		if( input.isKeyPressed( Keys.W ) ){
			player.x += camera.direction.x*2;
			player.z += camera.direction.z*2;
		}
		
		if( input.isKeyPressed( Keys.S ) ){
			player.x-= camera.direction.x*2;
			player.z -= camera.direction.z*2;
		}
		
		if( input.isKeyPressed( Keys.A ) ){
			movementdirection=camera.direction.cpy().crs(camera.up).mul(-1);
			player.x=player.x+movementdirection.x;
			player.z=player.z+movementdirection.z;
		}
		
		if( input.isKeyPressed( Keys.D ) ){
			movementdirection=camera.direction.cpy().crs(camera.up).mul(1);
			player.x = player.x +movementdirection.x;
			player.z=player.z+movementdirection.z;
		
		
		}
/*		
		if( input.isKeyPressed( Keys.X ) )
		{
			//Call function to shake camera dependent on i even or odd.
			if (i % 2 == 0)
			{
			  CameraOperations.shakeCameraLeft( camera );
			  player.z += 1;
			  i++;
			}
			else
			{
				player.z -= 1;
			  CameraOperations.shakeCameraRight( camera );
			  i = 0;
			}
			
		}
*/		
		if( input.isKeyPressed( Keys.U ) ){
			camera.position.add( camera.direction.cpy().mul( 1.0f ) );
			}
		
		if( input.isKeyPressed( Keys.H ) ){
			cameraangle+=.05;
		}
		
		if( input.isKeyPressed( Keys.K ) ){
			cameraangle-=.05;//camera.position.add( camera.direction.cpy().crs( camera.up ).mul( 1.0f ) );
		}
		
		if( input.isKeyPressed( Keys.J ) ){
			camera.position.add( camera.direction.cpy().mul( -1.0f ) );
		}
		
		
		if( input.isKeyPressed( Keys.UP ) ){
			camera.lookAt(player.x, player.y, player.z);
			camera.position.add( camera.direction.cpy().mul( 1.0f ) );
			cameradistance=(float) Math.sqrt((camera.position.x-player.x)*(camera.position.x-player.x)+(camera.position.z-player.z)*(camera.position.z-player.z));
			}
		
		if( input.isKeyPressed( Keys.LEFT ) ){
			cameraangle+=.05;
		}
		
		if( input.isKeyPressed( Keys.RIGHT) ){
			cameraangle-=.05;//camera.position.add( camera.direction.cpy().crs( camera.up ).mul( 1.0f ) );
		}
		
		if( input.isKeyPressed( Keys.DOWN ) ){
			camera.lookAt(player.x, player.y, player.z);
			camera.position.add( camera.direction.cpy().mul( -1.0f ) );
			cameradistance=(float) Math.sqrt((camera.position.x-player.x)*(camera.position.x-player.x)+(camera.position.z-player.z)*(camera.position.z-player.z));
		}
		}
		
		// Lightning strike
		if( input.isKeyPressed( Keys.L ) )
		{
			lightning[1] = 1;
		}
		
		//cameradistance=(float) Math.sqrt((camera.position.x-player.x)*(camera.position.x-player.x)+(camera.position.z-player.z)*(camera.position.z-player.z));
		
		relativeCameraPosition.x=(float) (Math.sin(cameraangle)*cameradistance);
		relativeCameraPosition.z=(float) (Math.cos(cameraangle)*cameradistance);
		// set a nice initial view
		camera.position.set(relativeCameraPosition.x+ player.x, player.y+10,relativeCameraPosition.z+ player.z );
		
		
	}
	
	@Override
	public void pause() {
		// TODO Auto-generated method stub

	}

	@Override
	public void resume() {
		// TODO Auto-generated method stub

	}

	@Override
	public void dispose() {
		// TODO Auto-generated method stub

	}

	public void Load_Models()
	{
		for(int i=0;i<=house.length-1;i++){
			if(i<=4){
				house[i]=new Objects(this,"resources/castle.obj","resources/castle_tex.bmp",400,19,450,4,0,0,0,0);
			}else{
				house[i]=new Objects(this,"resources/castle.obj","resources/castle_tex.bmp",400,19,450,4,0,0,0,0);	
			}
		}
		for(int i=0;i<Cabbage.length;i++){
			if(i<10){
				Cabbage[i]=new Objects(this,"resources/mesh/cabbage.obj","resources/mesh/cabbage_tex.bmp",215-(i*10)-mov,16,480+movz,2,0,0,0,0);
			}else if(i>=10 && i<20){
				Cabbage[i]=new Objects(this,"resources/mesh/cabbage.obj","resources/mesh/cabbage_tex.bmp",215-((i-10)*10)-mov,16,460+movz,2,0,0,0,0);
			}else if(i>=20 && i<30){
				Cabbage[i]=new Objects(this,"resources/mesh/cabbage.obj","resources/mesh/cabbage_tex.bmp",215-((i-20)*10)-mov,16,440+movz,2,0,0,0,0);
			}else if(i>=30){
				Cabbage[i]=new Objects(this,"resources/mesh/cabbage.obj","resources/mesh/cabbage_tex.bmp",215-((i-30)*10)-mov,16,420+movz,2,0,0,0,0);
			}
		}
		Tree2=new Objects(this,"resources/mesh2/roundtreeA.obj","resources/mesh2/roundtreebark.bmp",100,19,150,1,0,0,0,0);
	
		Wall[0]=new Objects(this,"resources/mesh_hedge/shelffence.obj","resources/mesh_hedge/shelffence.bmp",100-mov,21,400+movz,5,0,0,0,0);
		for(int i=0;i<Wall.length-1;i++){
			if(i<5){
				Wall[i+1]=new Objects(this,"resources/mesh_hedge/shelffence.obj","resources/mesh_hedge/shelffence.bmp",110+(i*10)-mov,21,400+movz,5,0,0,0,0);
			}else if(i>=5 && i<10){
				Wall[i+1]=new Objects(this,"resources/mesh_hedge/shelffence.obj","resources/mesh_hedge/shelffence.bmp",130+(i*10)-mov,21,400+movz,5,0,0,0,0);	
			}else if(i>=10 && i<20){
				Wall[i+1]=new Objects(this,"resources/mesh_hedge/shelffence.obj","resources/mesh_hedge/shelffence.bmp",95-mov,21,405+((i-10)*10)+movz,5,90,0,1,0);
			}else if(i>=20 && i<33){
				Wall[i+1]=new Objects(this,"resources/mesh_hedge/shelffence.obj","resources/mesh_hedge/shelffence.bmp",100+((i-20)*10)-mov,21,500+movz,5,180,0,1,0);
			}else if(i>=33){
				Wall[i+1]=new Objects(this,"resources/mesh_hedge/shelffence.obj","resources/mesh_hedge/shelffence.bmp",225-mov,21,405+(((i-33))*10)+movz,5,270,0,1,0);		
			}
		}
		Shark=new Objects(this,"resources/mesh_shark/shark.obj","resources/mesh_shark/sharktexture.bmp",138,7,245,5,0,0,0,0);
		Text=new Objects(this,"resources/mesh_hedge/shelffence.obj","resources/mesh_hedge/textwrapped.bmp",100,30,450,6,90,0,1,0);
	
		/*
		// Place tree models in map
		for(int i=0;i<=trees.length-1;i++){
		if(i<=4){
			trees[i]=new Objects(this,"resources/treeA.obj","resources/bark_NM.png",200,19,250,4);
		}else{
			trees[i]=new Objects(this,"resources/treeA.obj","resources/bark_NM.png",200,19,250,4);	
		}
		}
		*/
		


	}
}
