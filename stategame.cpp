#include <vector>
#include <set>
#include <queue>
#include <math.h>
using namespace std;

struct STATE
{
	int state;
	int option;
	STATE* parent;
};

int getposfree(int state)
{
	for(int i=0;i<9;i++)
	{


	}
	return 0;
}

vector<int> getpath(int STARTSTATE,STATE DONE)
{
	vector<int> out;
	vector<int>::iterator it;
	STATE=DONE.parent;
	STATE* CURRENT;
	CURRENT=DONE.parent;
	
	while(CURRENT!=NULL)
	{
		it=out.begin;
		out.insert(it,CURRENT->option);
		CURRENT=CURRENT->parent;
	}

	return out;
}
//swap the the i, and jth positions of a number starting from the left, counting from 1 up
STATE swap(STATE CURRENTSTATE,int i, int j)
{
	i=9-i;
	j=9-j;
	int a=CURRENTSTATE.state;
	int b=(int)pow(10.0,i)%10;
	int c=(int)pow(10.0,j)%10;

	CURRENTSTATE.state=a+c*(int)pow(10.0,i)-b*(int)pow(10.0,i)-c*(int)pow(10.0,j)+b*(int)pow(10.0,j);
	return CURRENTSTATE;
}

STATE nextstate(STATE CURRENTSTATE, int position, int option)
{
	switch(position)
	{
		case 1:
			switch(option)
			{
				//nothing happens
				case 1:
					break;
				//swap 1, and 2
				case 2:
					CURRENTSTATE=swap(CURRENTSTATE,1,2);
					break;
				//swap 1 and 4
				case 3:
					CURRENTSTATE=swap(CURRENTSTATE,1,4);
					break;
				//nothing happens
				case 4:
					break;
			}
			break;
		case 2:
			switch(option)
			{
				//nothing
				case 1:
					break;
				//swap 2 and 3
				case 2:
					CURRENTSTATE=swap(CURRENTSTATE,2,3);
					break;
				//swap 2 and 5
				case 3:
					CURRENTSTATE=swap(CURRENTSTATE,2,5);
					break;
				//swap 2 and 1
				case 4:
					CURRENTSTATE=swap(CURRENTSTATE,2,1);
					break;
			}			
			break;
		case 3:
			switch(option)
			{
				//nothing
				case 1:
					break;
				//nothing
				case 2:
					break;
				//swap 3 and 6
				case 3:
					CURRENTSTATE=swap(CURRENTSTATE,3,6);
					break;
				//swap 3 and 2
				case 4:
					CURRENTSTATE=swap(CURRENTSTATE,3,2);
					break;

			}
			break;	
		case 4:
			switch(option)
			{
				//swap 4 and 1
				case 1:
					CURRENTSTATE=swap(CURRENTSTATE,4,1);
					break;
				//swap 4 and 5
				case 2:		
					CURRENTSTATE=swap(CURRENTSTATE,4,5);
					break;
				//swap 4 and 7
				case 3:
					CURRENTSTATE=swap(CURRENTSTATE,4,7);
					break;
				//nothing
				case 4:
					break;
			}
			break;	
		case 5:
			switch(option)
			{
				//swap 2 and 5
				case 1:
					CURRENTSTATE=swap(CURRENTSTATE,2,5);
					break;
				//swap 5 and 6
				case 2:
					CURRENTSTATE=swap(CURRENTSTATE,5,6);
					break;
				//swap 5 and 8
				case 3:
					CURRENTSTATE=swap(CURRENTSTATE,5,8);
					break;
				//swap 5 and 4
				case 4:
					CURRENTSTATE=swap(CURRENTSTATE,5,4);
					break;

			}
			break;	
		case 6:
			switch(option)
			{
				//swap 6 and 3
				case 1:
					CURRENTSTATE=swap(CURRENTSTATE,6,3);
					break;
				//nothing
				case 2:
					break;
				//swap 6 and 9
				case 3:
					CURRENTSTATE=swap(CURRENTSTATE,6,9);
					break;
				//swap 6 and 5
				case 4:
					CURRENTSTATE=swap(CURRENTSTATE,6,5);
					break;
			}
			break;	
		case 7:
			switch(option)
			{
				//7 and 4
				case 1:
					CURRENTSTATE=swap(CURRENTSTATE,7,4);
					break;
				//7 and 8
				case 2:
					CURRENTSTATE=swap(CURRENTSTATE,7,8);
					break;
				//nothing
				case 3:
					break;
				//nothing
				case 4:
					break;

			}
			break;	
		case 8:
			switch(option)
			{
				//swap 8 and 5
				case 1:
					CURRENTSTATE=swap(CURRENTSTATE,8,5);
					break;
				//swap 8 and 9
				case 2:
					CURRENTSTATE=swap(CURRENTSTATE,8,9);
					break;
				//nothing
				case 3:
					break;
				//swap 8 and 7
				case 4:
					CURRENTSTATE=swap(CURRENTSTATE,8,7);
					break;

			}
			break;	
		case 9:
			switch(option)
			{
				//swap 9 and 6
				case 1:
					CURRENTSTATE=swap(CURRENTSTATE,9,6);
					break;
				//nothing
				case 2:
					break;
				//nothing
				case 3:
					break;
				//swap 9 and 8
				case 4:
					CURRENTSTATE=swap(CURRENTSTATE,9,8);
					break;

			}
			break;	
		default:
			break;
	}
	return CURRENTSTATE;
}

vector<int> stategame(int STARTSTATE,int ENDSTATE)
{
	set<STATE> visited;
	queue<STATE> checkus;
	STATE CURRENTSTATE;
	STATE DONE;
	int position=0;

	STATE NEWSTATE1;
	STATE NEWSTATE2;
	STATE NEWSTATE3;
	STATE NEWSTATE4;

	CURRENTSTATE.state=STARTSTATE;
	CURRENTSTATE.parent=NULL;
	CURRENTSTATE.option=-1;
	
	DONE.parent=NULL;
	DONE.state=-1;
	DONE.option=-1;
	
	checkus.push(CURRENTSTATE);
	visited.insert(CURRENTSTATE);

	while(!checkus.empty())
	{
		CURRENTSTATE=checkus.pop();
		//found the end state
		if(CURRENTSTATE.state==ENDSTATE)
		{
			DONE=CURRENTSTATE;
			break;
		}
		else
		{
			position=getposfree(CURRENTSTATE.state);
		
			//4 possible new states to go to
			//add children to queue

			NEWSTATE1=nextstate(CURRENTSTATE,position,1);
			NEWSTATE1.parent=&CURRENTSTATE;
			NEWSTATE1.option=1;
			if(!VISITED(NEWSTATE1,visited))
				checkus.enqueue(NEWSTATE1);
		
			NEWSTATE2=nextstate(CURRENTSTATE,position,2);
			NEWSTATE2.parent=CURRENTSTATE.state;
			NEWSTATE2.option=2;
			if(!VISITED(NEWSTATE2,visited))
				checkus.enqueue(NEWSTATE2);
				
			NEWSTATE3=nextstate(CURRENTSTATE,position,3);
			NEWSTATE3.parent=CURRENTSTATE.state;
			NEWSTATE3.option=3;
			if(!VISITED(NEWSTATE3,visited))
				checkus.enqueue(NEWSTATE3);

			NEWSTATE4=nextstate(CURRENTSTATE,position,4);
			NEWSTATE4.parent=CURRENTSTATE.state;
			NEWSTATE4.option=4;
			if(!VISITED(NEWSTATE4,visited))	
				checkus.enqueue(NEWSTATE4);
		}
	}
	return getpath(STARTSTATE,DONE);
}

int main()
{
	return 0;
}
