/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author afmartinez4
 */
import javax.swing.*;
import java.awt.*;
public class ball {
    int xpos;
    int ypos;
    int width;
    int height;
    int xvel;
    int yvel;

    ball(int x, int y, int xv, int yv, int w, int h)
    {
         xpos=x;
     ypos=y;
     width=w;
     height=h;
     xvel=xv;
     yvel=yv;

    }
    public void updatevel(int xv, int yv)
    {
        xvel=xv;
        yvel=yv;

    }

    public void update()
    {
        xpos=xpos+xvel;
        ypos=ypos+yvel;
    }
    
    public void draw( Graphics g)
    {
        g.fillOval(xpos, ypos, width, height);
        
    }
}
