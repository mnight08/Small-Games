


import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.util.LinkedList;
import java.util.*;
public class CollisionRectangle
{
	//location
	int x;
	int y;
        double dx;
        double dy;
	//dimension
	int width;
	int height;

	//picture
	ImageIcon pic;


	CollisionRectangle()
	{

		loadImage("joker.gif");
		width = 30;
		height = 30;
                
	}

	//return true if we overlap ob, false if not
	public boolean overlap(CollisionRectangle ob)
	{
		//alternate approach
		if( x+width >= ob.x &&
			x <= ob.x + ob.width &&
			y+height >= ob.y &&
			y <= ob.y + ob.height)
		{
			return true;
		}

		return false;

	}

	public void loadImage(String picname)
	{
		pic = new ImageIcon(this.getClass().getResource(picname));
	}

	public void move()
	{
            x=x+(int)dx;
            y=y+(int)dy;

	}

	public void draw(Graphics g, Component c)
	{
		g.drawImage(pic.getImage(),x,y,width,height,c);
	}


}