


import java.util.ListIterator;
import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.util.LinkedList;
import java.util.*;

public class GalagaPanel extends JPanel implements KeyListener
{
	Ship ship;

	LinkedList<CollisionRectangle> alienList;

	//Bullet bullet;

	ImageIcon background;

	GalagaPanel()
	{
		//create the ship
		ship = new Ship();

		//load up aliens
		loadAliens();

		//create a test bullet
		//bullet = new Bullet();

		//create a background
		background = new ImageIcon(this.getClass().getResource("space.gif"));

		//start a thread to move all the aliens, repaint panel
		UpdaterThread ut = new UpdaterThread(this);
		ut.start();

		addKeyListener(this);
		setFocusable(true);

	}
        public void checkcollision(LinkedList<CollisionRectangle> alienList)
       {
         CollisionRectangle temp;

            for( ListIterator<CollisionRectangle> i=alienList.listIterator(); i.hasNext();i.remove())
            {
                //fix this slkfjalskdjflkasjdl;fkajsdlkfjasl;kdflaksdfo;iqwh;ilehfljnvlnx;,mnlkjslkdjflkasjdlfkjasldjflaksjdlfjsldfj
                //kasjdflkjhktlhlskdflkjasdlkjflk look here
                //*********************************************************************
                temp=i.next();
                if(a.overlap(temp));
                {
                    destroy(alienList,a);
                    destroy(alienList,temp);
                    break;

                }
            }
        }
        public void checkcollision(LinkedList<CollisionRectangle> alienList, CollisionRectangle a)
        {
            CollisionRectangle temp;

            for( ListIterator<CollisionRectangle> i=alienList.listIterator(); i.hasNext();i.remove())
            {
                temp=i.next();
                if(a.overlap(temp));
                {
                    destroy(alienList,a);
                    destroy(alienList,temp);
                    break;

                }
            }
        }
        public void destroy(LinkedList<CollisionRectangle> alienList, CollisionRectangle a)
        {
            alienList.remove(a);
        }
        
	//load up a bunch of aliens into the game
	public void loadAliens()
	{
		Random dice = new Random();

		//create a new linkedlist
		alienList = new LinkedList<CollisionRectangle>();

		//add a bunch of aliens to list
		for(int i=0; i<5; i++)
		{
			SeekerAlien a = new SeekerAlien();
			a.setPrey(ship);
			a.x = dice.nextInt(400);
			a.y = dice.nextInt(500);
			alienList.add(a);
		}
                alienList.add(ship);
	}

	public void paintComponent(Graphics g)
	{
		//draw background
		g.drawImage(background.getImage(), 0,0, getWidth(), getHeight(), this);

		//draw the ship
		ship.draw(g, this);

		//draw all the aliens in the game
		for( CollisionRectangle a : alienList )
			a.draw(g,this);

		//draw bullet
		//bullet.draw(g,this);
	}


	//key Listener methods

	public void keyPressed(KeyEvent k)
	{
		char c = k.getKeyChar();

		//figure out which key was pressed
		if( c == 'd' )
			ship.dx= 5;
		if( c == 'a' )
			ship.dx= -5;
		if( c == 'w' )
			ship.dy = -5;
		if( c == 's' )
			ship.dy = 5;

		//check for fire button
		if( c == ' ' )
		{
                    ship.fire(alienList);
			
		}
	}

	public void keyReleased(KeyEvent k)
	{

	}

	public void keyTyped(KeyEvent k)
	{
		//???
	}


}