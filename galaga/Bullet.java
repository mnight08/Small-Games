


import java.awt.*;

public class Bullet extends CollisionRectangle
{
	//bullet velocity
	int dx;
	int dy;

	Bullet()
	{
		x=200;
		y=300;

		width =10;
		height =10;

		dx=0;
		dy= -5;

	}
        Bullet(int x1, int y1, int dx1,int dy1)
        {
            x=x1;
            y=y1;
            dx=dx1;
            dy=dy1;
            width =10;
            height =10;
        }
	public void move()
	{
		x += dx;
		y += dy;
	}

	public void draw(Graphics g, Component c)
	{
		g.setColor(Color.YELLOW);
		g.fillOval(x,y,width,height);
	}


}