



public class SeekerAlien extends Alien
{

	Ship prey;
	int dx;
	int dy;

	SeekerAlien()
	{
		x=50;
		y=20;

		dx= 5;
		dy= 5;

		loadImage("bullet.jpg");
	}

	public void move()
	{
		if( prey != null )
		{
			if( prey.x > x )
				x += dx;
			else if( prey.x < x )
				x -= dx;

			if( prey.y < y )
				y -= dy;
			else if( prey.y > y )
				y += dy;
		}
	}


	public void setPrey( Ship s )
	{
		prey = s;
	}

}