import pygame
import copy
import random
import sys
from pygame.locals import Color, KEYUP, KEYDOWN, K_ESCAPE, K_RETURN,K_LEFT,K_RIGHT,K_UP,K_a,K_d,K_w,K_s,K_DOWN



pygame.init()
screen_width=600
screen_height=600

screen = pygame.display.set_mode([screen_width,screen_height])
Clock = pygame.time.Clock()




#game configuration information
frame_rate=30;
drop_rate=20;
input_rate=3;
number_of_grid_columns=40
number_of_grid_rows=40
#spacing in pixels
column_spacing=screen_width/number_of_grid_columns
row_spacing=screen_height/number_of_grid_rows
#the first row and column  correspond to the top left corner of game 
#color definitions
square_color=pygame.Color(0, 183, 235, 0)
z_color=pygame.Color(0, 255, 0, 0)
s_color=pygame.Color(0, 0, 255, 0)
line_color=pygame.Color(255, 0, 0, 0)
r_color=pygame.Color(255, 0, 255, 0)
gamma_color=pygame.Color(255, 255, 0, 0)
T_color=pygame.Color(128, 128, 128, 0)
game_is_not_done=True
there_is_a_block_falling=False
the_set_of_blocks_on_ground=[]
test_block=[]
time_since_block_dropped=0;
time_since_input_read=0;
#this is a tuple.  The first number maps to the puzzle piece to use. 
#second number is the orientation it has, i.e.  up, down, left ,right.  
#third is current grid row
#fourth is grid column

the_falling_block=[0,0,0,0]
score=0
down_key=K_LEFT
key_is_down=False
def read_input():
	global key_is_down
	global down_key
	test_block=the_falling_block
	movements=0;
	#I could change this to make the tetrinos wrap around the screen rather than simply not move
	#must also check collisions to the direction the thing is moving
	if key_is_down:
		#move left
		if down_key==K_LEFT or down_key==K_a:
			test_block[3]=test_block[3]-1
			if (not block_is_colliding_with_walls(test_block)) and (not block_is_colliding(test_block)):
				print("cat")
				the_falling_block[3]=the_falling_block[3]-1
		#move right
		elif down_key==K_RIGHT or down_key==K_d:
			test_block[3]=test_block[3]+1
			if (not block_is_colliding_with_walls(test_block)) and (not block_is_colliding(test_block)):
				print("cat")
				the_falling_block[3]=the_falling_block[3]+1
	for e in pygame.event.get():
		if e.type == KEYDOWN:
			if not key_is_down:
				down_key=e.key
				#move left
				if down_key==K_LEFT or down_key==K_a:
					test_block[3]=test_block[3]-1
					if (not block_is_colliding_with_walls(test_block)) and (not block_is_colliding(test_block)):
						print("cat")
						the_falling_block[3]=the_falling_block[3]-1
				#move right
				elif down_key==K_RIGHT or down_key==K_d :
					test_block[3]=test_block[3]+1
					if (not block_is_colliding_with_walls(test_block)) and (not block_is_colliding(test_block)):
						print("cat")
						the_falling_block[3]=the_falling_block[3]+1
				#rotate left.
				elif down_key==K_UP or down_key==K_w:
					the_falling_block[1]=(the_falling_block[1]+1)%4
				#rotate right.
				elif down_key==K_DOWN or down_key==K_s:
					the_falling_block[1]=(the_falling_block[1]-1)%4
				key_is_down=True
		elif e.type == KEYUP:
			key_is_down=False
		elif e.type == pygame.QUIT:
			return False
	return True
def delay_game():
	Clock.tick(frame_rate)
def move_falling_block_down():
	the_falling_block[2]=the_falling_block[2]+1;
#returns a tuple of form ((r1,c1),(r2,c2),(r3,c3),(r4,c4))  only rows and columns are needed to identify squares 
#that fill cells in the grid . 
#I should go back and rewrite the drawing function using the squares from this function
def generate_new_block():
	#pick random number from 0-6.  Default orientation is up.
	random.seed()
	block_type=random.randint(0,6)
	block_orientation=0
	#start the given block just off screen
	if block_type==0:
		block_row=-2
	elif block_type==1:
		block_row=-2
	elif block_type==2:
		block_row=-2
	elif block_type==3:
		block_row=-4
	elif block_type==4:
		block_row=-3
	elif block_type==5:
		block_row=-3
	elif block_type==6:
		block_row=-2
	block_column=number_of_grid_rows/2
	return [block_type,block_orientation,block_row,block_column]
def get_sub_squares_from_block(block):
	block_type=block[0]
	block_orientation=block[1]
	block_row=block[2]
	block_column=block[3]
	#this is the square.  Looks the same no matter the orientation
	if block_type==0:
		return [[block_row,block_column],[block_row+1,block_column],[block_row,block_column+1],[block_row+1,block_column+1]]

	#this is the z
	elif block_type==1:
		if block_orientation==0 or block_orientation==2:
			return [[block_row,block_column],[block_row,block_column+1],[block_row+1,block_column],[block_row+1,block_column-1]]
		else:
			return [[block_row+1,block_column],[block_row,block_column+1],[block_row+1,block_column+1],[block_row,block_column+2]]			
	#this is s
	elif block_type==2:
		if block_orientation==0 or block_orientation==2:
			return [[block_row,block_column+1],[block_row,block_column+2],[block_row+1,block_column],[block_row+1,block_column+1]]
		else:
			return [[block_row,block_column],[block_row+1,block_column],[block_row+1,block_column+1],[block_row+2,block_column+1]]
	#this is line
	elif block_type==3:
		if block_orientation==0:
			return [[block_row,block_column],[block_row+1,block_column],[block_row+2,block_column],[block_row+3,block_column]]
		elif block_orientation==1:
			return [[block_row,block_column],[block_row,block_column-1],[block_row,block_column-2],[block_row,block_column-3]]
		elif block_orientation==2:
			return [[block_row,block_column],[block_row-1,block_column],[block_row-2,block_column],[block_row-3,block_column]]
		elif block_orientation==3:
			return [[block_row,block_column],[block_row,block_column+1],[block_row,block_column+2],[block_row,block_column+3]]		
	#this is r
	elif block_type==4:
		if block_orientation==0:
			return [[block_row,block_column],[block_row,block_column+1],[block_row+1,block_column],[block_row+2,block_column]]
		elif block_orientation==1:
			return [[block_row,block_column-2],[block_row,block_column-1],[block_row,block_column],[block_row+1,block_column]]
		elif block_orientation==2:
			return [[block_row,block_column-1],[block_row,block_column],[block_row-1,block_column],[block_row-2,block_column]]
		elif block_orientation==3:
			return [[block_row,block_column],[block_row,block_column-1],[block_row,block_column-2],[block_row-1,block_column-2]]
			
	#this is gamma looking one
	elif block_type==5:
		if block_orientation==0:
			return [[block_row,block_column],[block_row,block_column-1],[block_row+1,block_column],[block_row+2,block_column]]
		elif block_orientation==1:
			return [[block_row,block_column],[block_row-1,block_column],[block_row,block_column-1],[block_row,block_column-2]]
		elif block_orientation==2:
			return [[block_row,block_column],[block_row-1,block_column],[block_row-2,block_column],[block_row,block_column+1]]
		elif block_orientation==3:
			return [[block_row,block_column],[block_row-1,block_column],[block_row,block_column+1],[block_row-1,block_column+2]]

	#this is T
	elif block_type==6:
		if block_orientation==0:
			return [[block_row,block_column],[block_row+1,block_column],[block_row+1,block_column+1],[block_row+1,block_column-1]]
		elif block_orientation==1:
			return [[block_row,block_column],[block_row+1,block_column],[block_row+2,block_column],[block_row+1,block_column+1]]
		elif block_orientation==2:
			return [[block_row,block_column],[block_row,block_column-1],[block_row,block_column+1],[block_row+1,block_column]]
		elif block_orientation==3:
			return [[block_row,block_column],[block_row+1,block_column-1],[block_row+1,block_column],[block_row+2,block_column]]

def get_block_color(block_type):
	if block_type==0:
		color=square_color
	#this is the z
	elif block_type==1:
		color=z_color				
	#this is s
	elif block_type==2:
		color=s_color
	#this is line
	elif block_type==3:
		color=line_color	
	#this is r
	elif block_type==4:
		color=r_color
	#this is gamma looking one
	elif block_type==5:
		color=gamma_color
	#this is T
	elif block_type==6:
		color=T_color
	return color
def draw_square_at_given_cell(row,column,color):
	if (row >=0 and row <number_of_grid_rows) and (column >=0 and column< number_of_grid_columns):
		pygame.draw.rect(screen, color,pygame.Rect( column*row_spacing, row*column_spacing, row_spacing, column_spacing))
def draw_block(block):
	block_type=block[0]
	color=get_block_color(block_type)
	squares=get_sub_squares_from_block(block)
	draw_square_at_given_cell(squares[0][0],squares[0][1],color)
	draw_square_at_given_cell(squares[1][0],squares[1][1],color)
	draw_square_at_given_cell(squares[2][0],squares[2][1],color)
	draw_square_at_given_cell(squares[3][0],squares[3][1],color)
def draw_blocks():
	for block in the_set_of_blocks_on_ground:
		draw_block(block)
	draw_block(the_falling_block)

def the_two_blocks_are_intersecting(block_1,block_2):
	set_of_squares_1=get_sub_squares_from_block(block_1)
	set_of_squares_2=get_sub_squares_from_block(block_2)
	for square_1 in set_of_squares_1:
		for square_2 in set_of_squares_2:
			if square_1==square_2:
				return True;
	return False

def square_is_off_screen(square):
	if square[0]<0:
		return True
	else:
		return False
def the_falling_block_is_offscreen():
	sub_squares=get_sub_squares_from_block(the_falling_block)
	square1=sub_squares[0]
	square2=sub_squares[1]
	square3=sub_squares[2]
	square4=sub_squares[3]
	#debugging code
	#print("checking if the falling block is off screen:")
	#print(square1)
	#print(square2)
	#print(square3)
	#print(square4)
	#print(the_falling_block[0])
	if square_is_off_screen(square1) and square_is_off_screen(square2) and square_is_off_screen(square3) and square_is_off_screen(square4):
		return True
	else:
		return False
def the_block_is_intersecting_floor(test_block):
	sub_squares=get_sub_squares_from_block(test_block)
	square1=sub_squares[0]
	square2=sub_squares[1]
	square3=sub_squares[2]
	square4=sub_squares[3]
	if square1[0]>=number_of_grid_rows or square2[0]>=number_of_grid_rows  or square3[0]>=number_of_grid_rows  or square4[0]>=number_of_grid_rows :
		return True
	else:
		return False

def block_is_colliding(block):
	#check if it is colliding with other blocks
	for other_block in the_set_of_blocks_on_ground:
		if the_two_blocks_are_intersecting(other_block,block):
			return True
	#check if it is colliding with the floor
	if the_block_is_intersecting_floor(block):
		return True
	return False
def block_is_colliding_with_walls(block):
	sub_squares=get_sub_squares_from_block(block)
	square1=sub_squares[0]
	square2=sub_squares[1]
	square3=sub_squares[2]
	square4=sub_squares[3]
	if square1[1]>=number_of_grid_columns or square2[1]>=number_of_grid_columns  or square3[1]>=number_of_grid_columns  or square4[1]>=number_of_grid_columns or square1[1]<0 or square2[1]<0  or square3[1]<0  or square4[1]<0:
		print("you are collidng")
		return True
	else:
		return False
#uncomment for the debugger
#import pdb
#pdb.set_trace()
the_falling_block=generate_new_block()
score+=4
there_is_a_block_falling=True
while game_is_not_done:
	#each frame takes 1/framerate seconds to occur.  we have the drop rate, that is
	#the that the current block moves down the screen defined as a separate constant
	#that the frame rate.
	time_since_block_dropped+=1.0/frame_rate
	time_since_input_read+=1.0/frame_rate
	if time_since_input_read>= 1.0/input_rate:
		time_since_input_read=0
		game_is_not_done=read_input()
		
	if time_since_block_dropped >= 1.0/drop_rate and game_is_not_done:
		time_since_block_dropped=0
		test_block=copy.copy(the_falling_block)
		test_block[2]=test_block[2]+1
		if block_is_colliding(test_block):
			the_set_of_blocks_on_ground.append(the_falling_block);
			the_falling_block=generate_new_block()
			if block_is_colliding(the_falling_block):
				game_is_not_done=False
			score+=4
		if game_is_not_done:
			move_falling_block_down()

	
	screen.fill([0,0,0])
	draw_blocks()
	#right now this does not handle clearing completed lines.  
	pygame.display.flip()
	#make the game wait to get proper frame rate
	delay_game()


#display game over screen
print(score)
pygame.quit()
sys.exit()
