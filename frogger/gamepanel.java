
import java.awt.event.KeyListener;
import javax.swing.JPanel;
import java.awt.*;
import java.awt.event.KeyEvent;
import java.util.Iterator;
import java.util.LinkedList;
import javax.swing.ImageIcon;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author afmartinez4
 */
public class gamepanel extends JPanel implements KeyListener {

    //start a thread to move all the aliens, repaint panel
    LinkedList<movrect> movingthings;
    movrect player;
    ImageIcon background;
    int v;
    boolean gameinitialized;
    boolean gamefinished;

    gamepanel() {
        updaterthread ut = new updaterthread(this);
        gameinitialized=false;
        gamefinished=false;
        movingthings = new LinkedList<movrect>();
        player = new frog();
        background = new ImageIcon(this.getClass().getResource("background.jpg"));

        player.setSize(20, 20);
        //some mess happening here
        player.setLocation(getWidth()/2, getHeight()-player.height);
        movingthings.add(player);

        addKeyListener(this);
        setFocusable(true);
        ut.start();
    }
    public void initialize()
    {
         player.setLocation(getWidth()/2, getHeight()-player.height);
         car temp= new car();temp.xvel=10;movingthings.add(temp);
       gameinitialized=true; 
    }

    public void paintComponent(Graphics g) {

        //draw background
        g.drawImage(background.getImage(), 0, 0, getWidth(), getHeight(), this);

        //draw all the aliens in the game
        for (movrect a : movingthings) {
            a.draw(g, this);
        }
    }

    public void update() {
        for (movrect a : movingthings) {
            if (!a.iscolliding(movingthings)) {

                a.update();
                if(a.x-a.width>getWidth())
                    a.x=-a.width;
                if(a.x+a.width<0)
                    a.x=getWidth();
            }
            player.xvel = player.yvel = 0;
        }
    }
    //key Listener methods

    public void keyPressed(KeyEvent k) {
        char c = k.getKeyChar();
        v = 40;
        //figure out which key was pressed
        if (c == 'd') {
            player.pic = new ImageIcon(this.getClass().getResource("frogR.gif"));
            player.x += v;
        }
        if (c == 'a') {
            player.pic = new ImageIcon(this.getClass().getResource("frogL.gif"));
            player.x += -v;
        }
        if (c == 'w') {
            player.pic = new ImageIcon(this.getClass().getResource("frogU.gif"));
            player.y += -v;
        }
        if (c == 's') {
            player.pic = new ImageIcon(this.getClass().getResource("frogD.gif"));
            player.y += v;
        }

        //check for fire button
        if (c == ' ') {
        }
    }

    public void keyReleased(KeyEvent k) {
    }

    public void keyTyped(KeyEvent k) {
        //???
    }
}
