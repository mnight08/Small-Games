/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author afmartinez4
 */
public class updaterthread extends Thread {

    gamepanel panel;

    updaterthread(gamepanel p) {
        panel = p;
    }

    public void run() {
        while(!panel.gamefinished){
          if(!panel.gameinitialized)
          {
              panel.initialize();
          }
              panel.update();
        //repaint panel
        panel.repaint();

        //wait awhile
        try {
            Thread.sleep(50);
        } catch (Exception e) {
        }
        }
    }
}
