/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */



/**
 *
 * @author afmartinez4
 */
import java.awt.*;
import javax.swing.*;
public class Main {

    public static void main(String[] args) {
        // TODO code application logic here

    	//step 1, create a frame
		JFrame frame = new JFrame("Frogger!!");

		//step 2, create a panel, put it in frame
		gamepanel panel = new gamepanel();
		frame.getContentPane().add(panel);

		//step 3, set frame size, make it visible
		frame.setSize(600,600);
		frame.setVisible(true);

    }

}
