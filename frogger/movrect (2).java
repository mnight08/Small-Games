
import java.awt.Component;
import java.awt.Graphics;
import java.awt.Rectangle;
import java.util.LinkedList;
import javax.swing.ImageIcon;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author afmartinez4
 */
public class movrect extends Rectangle {
    int xvel;
    int yvel;
    ImageIcon pic;
    String type;
    movrect()
    {
        pic= null;//new ImageIcon(this.getClass().getResource("frogD.gif"));
        type="frog";
    }
    public void setpic(String p)
    {
        pic= new ImageIcon(this.getClass().getResource(p));
        width=pic.getIconWidth();
        height=pic.getIconHeight();
    }

    public void update()
    {
        x+=xvel;
        y+=yvel;
    }

    public void draw(Graphics g, Component c)
    {
        if(pic!=null)
        g.drawImage(pic.getImage(), x, y, c);
    }

}
