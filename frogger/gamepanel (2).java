
import java.awt.event.KeyListener;
import javax.swing.JPanel;
import java.awt.*;
import java.awt.event.KeyEvent;
import java.util.Iterator;
import java.util.LinkedList;
import javax.swing.ImageIcon;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author afmartinez4
 */
public class gamepanel extends JPanel implements KeyListener {

    //start a thread to move all the aliens, repaint panel
    LinkedList<movrect> movingthings;
    movrect player;
    ImageIcon background;
    int v;
    boolean gameinitialized;
    boolean gameover;
    boolean isonlog;
    boolean isinwater;
    gamepanel() {
        v=40;
        isonlog=false;
        isinwater=false;
        updaterthread ut;
        ut = new updaterthread(this);
        gameinitialized = false;
        gameover = false;
        movingthings = new LinkedList<movrect>();
        player = new movrect();
        background = new ImageIcon(this.getClass().getResource("background.jpg"));

        player.setSize(20, 20);
        //some mess happening here
        player.setLocation(getWidth() / 2, getHeight() - player.height);
        movingthings.add(player);

        addKeyListener(this);
        setFocusable(true);
        ut.start();
    }

    public void initialize() {
        player.setLocation(getWidth()/2, getHeight()-2*player.height);
        player.setpic("frogU.gif");


        car temp = new car();

        temp.y=getHeight()/2;
        temp.type = "car";temp.setpic("busR.gif");
        temp.xvel = 12;
        movingthings.add(temp);

        temp=new car();
        temp.y=getHeight()/2;
        temp.type = "car";temp.setpic("busR.gif");
        temp.xvel = 12;temp.x=2*temp.width;
        movingthings.add(temp);

        
        temp=new car();
        temp.y=getHeight()/2;
        temp.type = "car";temp.setpic("busR.gif");
        temp.xvel = 12;temp.x=5*temp.width;
        movingthings.add(temp);

        temp=new car();
       
        temp.type = "car";temp.setpic("busL.gif"); temp.y=getHeight()/2+getHeight()/5;
        temp.xvel = -9;temp.x=getWidth()/8;
        movingthings.add(temp);

        temp=new car();
    
        temp.type = "car";temp.setpic("busL.gif");    temp.y=getHeight()/2+getHeight()/5;
        temp.xvel = -9;temp.x=3*temp.width+getWidth()/8;
        movingthings.add(temp);


        temp= new car();temp.type="laser";temp.setpic("laser.jpg");temp.x=getWidth()/10;
        temp.yvel=15;
        movingthings.add(temp);


        temp= new car();temp.type="laser";temp.setpic("laser.jpg");temp.x=2*getWidth()/10;temp.y=getHeight()/8;
        temp.yvel=12;
        movingthings.add(temp);

        temp= new car();temp.type="laser";temp.setpic("laser.jpg");temp.x=6*getWidth()/10;
        temp.yvel=15;
        movingthings.add(temp);


        temp= new car();temp.type="laser";temp.setpic("laser.jpg");temp.x=2*getWidth()/10;
        temp.yvel=12;
        movingthings.add(temp);

        temp= new car();temp.type="laser";temp.setpic("laser.jpg");temp.x=7*getWidth()/10;
        temp.yvel=30;
        movingthings.add(temp);

        temp= new car();temp.type="laser";temp.setpic("laser.jpg");temp.x=4*getWidth()/10;temp.y=getHeight()/5;
        temp.yvel=7;
        movingthings.add(temp);

        temp= new car();temp.type="fly";temp.setpic("fly.png");temp.x=2*getWidth()/10;temp.y=getHeight()/2+getHeight()/5;
        movingthings.add(temp);

        
        temp= new car();temp.type="fly";temp.setpic("fly.png");temp.x=8*getWidth()/10;temp.y=getHeight()/2+getHeight()/5;
        movingthings.add(temp);

        temp= new car();
        temp.type="log";
        temp.xvel=-13;
        temp.setpic("medlog.png");
        temp.setLocation(0, getHeight()/12);
        movingthings.add(temp);

        temp= new car();
        temp.type="log";
        temp.xvel=-13;
        temp.setpic("medlog.png");
        temp.setLocation(getWidth()/2, getHeight()/12);
        movingthings.add(temp);

        temp= new car();
        temp.type="log";
        temp.xvel=19;
        temp.setpic("medlog.png");
        temp.setLocation(0, getHeight()/12+temp.height);
        movingthings.add(temp);

           temp= new car();
        temp.type="log";
        temp.xvel=-2;
        temp.setpic("longlog.png");
        temp.setLocation(0, getHeight()/12+2*temp.height);
        movingthings.add(temp);

        temp= new car(); temp.type="water";temp.setSize(getWidth(), getHeight()/3);
        temp.y=getWidth()/12;
        movingthings.add(temp);
        gameinitialized = true;

        temp= new car(); temp.type="car";
            temp.xvel=-2;
        temp.setpic("alileft.gif");
        temp.setLocation(getWidth()/2, 0);
        movingthings.add(temp);
    }

    public void paintComponent(Graphics g) {

        //draw background
        g.drawImage(background.getImage(), 0, 0, getWidth(), getHeight(), this);

        //draw all the aliens in the game
        for (movrect a : movingthings) {
            if(a!=player)
            a.draw(g, this);
        }
        player.draw(g,this);
    }

    public void update() {
        isonlog=false;isinwater=false;
        for (movrect a : movingthings) {


            a.update();
            if (a.x - a.width > getWidth()) {
                a.x = -a.width;
            }
            if (a.x + a.width < 0) {
                a.x = getWidth();
            }
            if(a!=player&&a.y-a.height>getHeight())a.y=-a.height;
            if (a!=player&&a.y + a.height < 0) a.y = getHeight();

            //also check for collisions
            if (a.intersects(player)) {
               if (a.type.equals("car")) {
                    //kill the frog
                    player.setpic("froggyDead.gif");gameover=true;
                } else if (a.type.equals("log")) {
                    //move the frog with the log!
                    isonlog=true;
                    player.x += a.xvel;
                    player.y += a.yvel;
                } else if (a.type.equals("laser")){
                    v=10;
                } else if (a.type.equals("fly")){
                    v=80;
                } else if (a.type.equals("water")){
                    isinwater=true;
                }

            }
            player.xvel = player.yvel = 0;
        }
        if(isinwater&&!isonlog){
            gameover=true;
            player.setpic("froggyDead.gif");
        }
        if(player.y<=0&&gameinitialized)gameover=true;
    }
    //key Listener methods

    public void keyPressed(KeyEvent k) {
        char c = k.getKeyChar();
   
        //figure out which key was pressed
        if (c == 'd') {
            player.pic = new ImageIcon(this.getClass().getResource("frogR.gif"));
            player.x += v;
        }
        if (c == 'a') {
            player.pic = new ImageIcon(this.getClass().getResource("frogL.gif"));
            player.x += -v;
        }
        if (c == 'w') {
            player.pic = new ImageIcon(this.getClass().getResource("frogU.gif"));
            player.y += -v;
        }
        if (c == 's') {
            player.pic = new ImageIcon(this.getClass().getResource("frogD.gif"));
            player.y += v;
        }

        //check for fire button
        if (c == ' ') {
        }
    }

    public void keyReleased(KeyEvent k) {
    }

    public void keyTyped(KeyEvent k) {
        //???
    }
}
