#include "gameengine.h"
#include <time.h>
gameengine::gameengine(int rooml, int characterl)
{	
	treasureroom=rand()%rooml;
	characterrelations= new graph(characterl);
	roomrelations=new graph(rooml);
	roomlimit=rooml;
	characterlimit=characterl;
    charactercount=0;
	totalturncount=0;
}

//right now this is set up for a single player.
//this is essentially the most dangerous game, but as rpg stye
bool gameengine::start()
{
	character *npclist;
	npclist= new character[characterlimit];
	npclist[0].updatecharacter(100,100,100,100,100,100,100, "HE",rand()%roomlimit,false,-50);
	npclist[0].type=character::god;
	charactercount++;

	cout<<"you awaken in a dark room.  unsure of how you got here, your heart begins to pound. you reach around for anything, but only grasp the dark.  you attempt to walk, but cant seem to remember how.  when your heart feels ready to explode, a voice begins to echo from within the air"<<endl;
	cout<<endl;
	cout<<"\"what is it?? a new warrior seeking fame? a new fearless hero, seeking to destroy evil??  what are you?? who are you?  tell me. i demamd it\""<<endl;
	cout<<"as you scatter to come up with anything, you realize you know nothing of youself.  you decide to lie"<<endl;
	cout<<"what would you like to claim to be."<<endl;


	int i=0;
	bool quit=false;

	while(!quit)
	{
		//if i allowed custom setup, it might take long, so i wont do it right now
		cout<<"1:warrior\n2:thief\n3:scholar\n=>";
			int option=-1;
		cin>>option;

		srand(time(0));
		switch(option)
		{
		case 1:

			npclist[1].updatecharacter(5,1,1,5,2,3,3,"warrior",rand()%roomlimit,true,0);
			quit=true;
			break;
		case 2:

			npclist[1].updatecharacter(2,2,3,2,5,5,1,"thief",rand()%roomlimit,true,0);quit=true;
			break;
		case 3:

			npclist[1].updatecharacter(1,5,5,2,2,1,4, "scholar",rand()%roomlimit,true,0);quit=true;
			break;

		default:
			switch(i)
			{
			case 0:
				cout<<"what are you a comedian? "<<endl;i++;
				break;
			case 1:
				cout<<"i am all that is, tremble before me and answer"<<endl;i++;
				break;
			case 2:
			//	cout<<"\"it seems you insolence only has one cure.  being the merciful being that i am, i have decided to provide you that cure\"";
			//	cout<<"the room begins to light up, and reveals mountains of treasure.  in a fit of joy somehow you manage to rush the mountain, and pocket as much as you can.  soon you begin to notice a strange smell, one that would not be expected from treasure.  you continue regardless, and then notice your skin seems to become leather like.  soon you notice the pain all over your body.  as you look to you look to the treasure, it begins to tarnish, and soon you realize that the treasure was that inards of screaming warriors.  as you try and move away, you become pulled in, and fused to the pile.  as you scream you notice an unconscious warrior being dragged in, as the light is somehow sucked out of the room.
					break;
			default:
				cout<<"Burrrrnnnnnnnnnnnnn"<<endl;
				break;
			}
			

		}
	}
	npclist[1].type=character::human;
	charactercount++;
	/*
	for(i=0;i<roomlimit;i++)
	{	//insert a 100 rooms
		roomrelations->insertnode(i);
	}
*/
	int temp;
	//assuming there are warps in spacetime and the such so this makes sense
	//set up random room relations, probability ranging from 0 to 100 out of 100, and weight being 0 to 50
	srand(time(0));
	for( i=0;i<roomlimit;i++)
	{
 		temp=(rand()%roomlimit)+1;
		//temp is the number of rooms you will be able to get to from where you are
		for(;temp>0;temp--)
		{
			roomrelations->insertnoderel(i,rand()%roomlimit,rand()%101,rand()%51);

		}
	}

	//make enemies random
	for(int i=2;i<characterlimit;i++)
	{
		npclist[i].type=character::weakguy;
		npclist[i].updatecharacter(rand()%3,rand()%3,rand()%3,rand()%3,rand()%3,rand()%3,rand()%3,"generic enemy name",rand()%roomlimit,true,(-1*rand()%1));
	
	}

//	int begincounttime=time(0);
//	int turncount=0;
	//the game loop
	while(true)
	{
		
		for(int i=0; i<characterlimit; i++)
		{
		
			if	(npclist[i].taketurn(roomrelations,npclist,i,*this))
				return true;
		}	
	}

	return true;
}


void gameengine::end()
{
	exit(0);

}

