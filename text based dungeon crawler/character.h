//#include "inventory.h"




#include <iostream>
using namespace std;

#ifndef CHARACTEr
#define CHARACTEr
#include "gameengine.h"
#include "armor.h"
#include "weapon.h"
#include <stdlib.h>


class character
{
public:
	int deathcount;
	string name;
	int turncount;	//determines which actions can be taken per turn, when this gets to <=zero, the enemies move.the delay should really be nonexistant, if there are no other players
	int whichroom;

	//stats
	//some will factor into more than what is mentioned here.
	int level;		//determines the amount of stats available to distribute
	int xp;			//this is a  bit dangerous to do, but this will be the amount of xp you currently have, and also the amount xp gained if you are killed(gained by the enemy)
	int health;		//determines how many attacks a character can withstand before death.
	int strength;	//determines amount of stuff character can carry, and amount of damage dealt via meelee.
	int inteligence;//determines ability to 
	int wisdom;		//determines what paths non-player characters will take.  set to max makes them omnipetent i.e. the boss
	int endurance;	//determines how many paths/actions can be taken per turn also probably damage taken
	int speed;		//determines ability to run from combat, and possible the amount of time till the characters next turn.  will very likely factor into combat
//	int armor;		//sum of armor bonus
	int wealth;		//the amount of money the character has.  it is planned to allow the player to interact with non-hostile characters, one of those interactions being trade
	int agility;	//determines ability to traverse complex, and dangerous paths successfully. also will factor into combat
	int charisma;	//determines ability to make friends/enemies

//	int maxweight;	//if current weight is above this, then movement is not an option

	bool isalive;	//if health is >0 return true, else return false.
//	bool handfree();//determines if you can equip a weapon
//	bool canmove();	//return current weight >maxweight? false:true
enum CHARACTER
	{
		god, human/*a player*/,weakguy, averageguy, strongguy, boss
	};

	CHARACTER type;

	bool move(int i, graph*);//move to room i
//	void findenemy();//this is really just for the boss
	//	void talknonsense(int i);//for the boss again, and possibly some cool enemies	

	//it would be nice to use these, but it is very unlikely that i will have time.
	armor chestc,headc,forearmc,shoulderc,upperlegc,lowerlegc,bootsc, glovesc,	shieldc;
	weapon leftarmc,rightarmc;

		//may not have time for this.
	//inventory storedstuff;
	
	void defend(int i);
	void revive();
	void attack(character& enemy);
	//void equipweapon();//move item from inventory to body
	//void equiparmor();
	void displayinfo();
	void displaypaths(graph*);
	bool checktreasure(class gameengine& game);
	bool taketurn(graph *,character*,int,class gameengine&);	//return true for quit
	void updatecharacter(int str, int intel, int wis, int endur, int spd, int agil, int charis,  string n, int room, bool alive, int deathct=0);
	bool battle(character& enemy);
	void checkbattle(character*, int,gameengine&);
	
	//int listposition;
	
	character();

};

#endif