#include "gameengine.h"
#include "character.h"
#include "graph.h"
#include "graphnode.h"
#include <time.h>
int main()
{	
	srand(time(0));
	int temp=rand()%10;
	gameengine game(temp,rand()%temp+1);
	if(!game.start())
		return -1;
	
	return 0;
}