#include <iostream>
#ifndef GRAPHNODE
#define GRAPHNODE

class graphnode		//trying to use this for two different graphs, so it might be a bit awkward
{
public:
	int id;
	int probability;//this is the likelyness of the path being accessible. for determining relation, it could be just set to max, or considered as the likelyhood of the other character recognizing another.
					//it is a bit lazy, but this can also be used to give random drops, rather than keep track of everything
	int edgeweight;	//cost for taking the path, for relatoin, says how well a character knows the person at the point.  or how easy/difficult to modify relation
	graphnode *next;
	graphnode(int p=0, int w=0 )
	{probability=p; edgeweight=w;next=NULL;}
};
#endif