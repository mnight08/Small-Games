
#include "graph.h"
#include<string>
#include<iostream>
using namespace std;
#ifndef MINHEAP
#define MINHEAP
class minHeap
{
public:
	minHeap(int max);

	void initializeheap(graphnode** itemlist, int newsize);

	void insert(graphnode *s);
	graphnode *extractMin();

	//test function
	void display();
	int getSize();
private:
	int parent(int i);
	int lchild(int i);
	int rchild(int i);

	int size;  //number of items in heap
	//i need an pointer to pointers.  this is gonna be annoying to fix
	graphnode ** heap;

};

#endif