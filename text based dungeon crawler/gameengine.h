



#ifndef GAMEENGINE
#define GAMEENGINE
#include "graph.h"
#include "character.h"

class gameengine
{
public:
	int treasureroom;
	int roomlimit;
	int totalturncount;
	int characterlimit;

	int charactercount;

	graph* characterrelations;
	graph* roomrelations;
	bool start();//run the game
	void end();	//end game
	
	//did not have enough time to implement this feature.
	//void spawnenemy(character* list);//spawn an enemy at random position
	//void updateroomrelations();
	//void characterrelations();

	gameengine(int roomlimit, int characterlimit);
};

#endif