#include "character.h"

character::character()
{
	isalive=false;
	deathcount=0;
	level=xp=health=strength=inteligence=wisdom=endurance=speed=wealth=agility=charisma/*=maxweight*/=0;
	type=weakguy;
	turncount=0;

}
/*
void character::talknonsense(character &c;int i)
{
switch(type)
{
case human:
break;//if human, then they dont have to talk nonsense
case weakguy:
//pick from random messages
switch(rand()%5)
{
case 0:
cout<<"\"PleasSE dONts Kilss me\""<<endl;
break;
case 1:
cout<<"can i be yours friendz"<<endl;
break;
case 2:
cout<<"can i havzee stuf???????"<<endl;
break;
case 3:
cout<<"are yos gOD"<<endl;
break;
case 4:

break;
default:
break;
}
break;
case averageguy:
//pick from random messages
switch(rand()%5)
{
case 0:
break;
case 1:
break;
case 2:
break;
case 3:
break;
case 4:
break;
default:
break;
}
break;

case strongguy:
//pick from random messages
switch(rand()%5)
{
case 0:
break;
case 1:
break;
case 2:
break;
case 3:
break;
case 4:
break;
default:
break;
}
break;
case boss:
//pick from random messages
switch(rand()%5)
{
case 0:
cout<<"i came here to kick ass and chew bubblegume... and im all out of bubblegum"<<endl;
break;
case 1:
break;
case 2:
break;
case 3:
break;
case 4:
break;
default:
break;
}
break;
case god:
//pick from random messages
switch(rand()%5)
{
case 0:
break;
case 1:
break;
case 2:
break;
case 3:
break;
case 4:
break;
default:
break;
}
break;
default:
break;
}

}
*/
//this will probably be wrong somewhere//i have been working for about 3 days, and have not compiled yet.
//there is a huge cheat here, essentially instead of making new characters, i just revive old ones that have died, after a given amount of turns.  i havent touched the second graph that was gonna be used, cause of lack of time.
bool character::taketurn(graph* roomrelations, character *npclist,int index,gameengine& game)
{

	int newpos=whichroom;
	//should factor in weight, but probably not enought time right now
	this->turncount+=(speed+level+inteligence+endurance+agility);

	switch(this->type)
	{

	case god:
		for(;turncount>0;)
		{
			if(this->isalive)
			{


				while(true)
				{
					this->checkbattle(npclist,index,game);

					newpos=roomrelations->shortestpath(this->whichroom,npclist[1].whichroom);
					graphnode* current=roomrelations->list[this->whichroom];
					if(current->id==newpos)
					{
						if(turncount<current->edgeweight)
							return false;
						else
						{
							if(!move(newpos,roomrelations))
							{
								whichroom=rand()%game.roomlimit;
								return false;
							}
							turncount-=current->edgeweight;

							this->checkbattle(npclist,index,game);
							break;
						}
					}
					else 
						current=current->next;
				}
			}
			else 
			{
				if(this->deathcount>10)
					revive();
				else 
				{
					deathcount++;
					turncount=0;
					return false;
				}
			}
		}

		break;

	case human:

		for(;turncount>0;)
		{		
//			system("cls");
		cout<<"the treasure is in room "<<game.treasureroom<<endl;
		if(this->isalive){
			while(true)
			{	
				checkbattle(npclist,index,game);


				displayinfo();
				cout<<"enter the room you would like to enter, or -1 to quit or -2 to skip turn"<<endl;

				displaypaths(roomrelations);

				cin>>newpos;
				if(newpos==-1)
					return true;
				if(newpos==-2)
					return false;
				graphnode* current=roomrelations->list[this->whichroom];
				for(;current!=NULL;current=current->next)
					if(current->id==newpos)
						break;
				if(current==NULL)
					return false;
				if(turncount<current->edgeweight)
					return false;
				else
				{
					turncount-=current->edgeweight;
					if(!move(newpos, roomrelations))
					{
						cout<<"you attempt to traverse the dangerous path, but as you near the end, you fall, and find yourself  in a new room"<<endl;
						whichroom=rand()%game.roomlimit;
						if(this->checktreasure(game))
						{
							cout<<"you found the treasure, and beat the game, hooray"<<endl;
							game.end();

						}
						return false;
					}
					if(this->checktreasure(game))
					{
						cout<<"you found the treasure, and beat the game, hooray"<<endl;
						game.end();

					}
					this->checkbattle(npclist,index,game);
					break;
				}


			}
		}
		else
		{
			if(this->deathcount>10)
				revive();
			else
			{
				deathcount++;
				turncount=0;
				return false;
			}
		}
		}

		break;
	case weakguy:

		for(;turncount>0;)
		{
			//essentially just move to a random room
			while(true)
			{

				if(this->isalive)
				{
					checkbattle(npclist,index,game);
					graphnode* current=roomrelations->list[whichroom];
					int count=0;
					for (;current!=NULL;current=current->next)
						count++;
					newpos=rand()%(count);
					current=roomrelations->list[whichroom];

					//roomrelations->list[whichroom]->
					for(int i=0;i<newpos;)
						current=current->next;

					newpos=current->id;
					if(turncount<current->edgeweight)
						return false;
					else
					{
						turncount-=current->edgeweight;
						if(!move(newpos,roomrelations))
						{
							whichroom=rand()%game.roomlimit;						
							return false;
						}
						this->checkbattle(npclist,index,game);
						break;

					}
				}
				else
				{
					if(this->deathcount>10)
						revive();
					else
					{
						deathcount++;
						turncount=0;
						return false;
					}

				}
			}	
		}

		break;
	case averageguy:
		for(;turncount>0;)
		{
			//essentially just move to a random room//if i have time, i will make this depend on the actual character type
			while(true)
			{

				if(this->isalive)
				{
					checkbattle(npclist,index,game);
					graphnode* current=roomrelations->list[whichroom];
					int count=0;
					for (;current!=NULL;current=current->next)
						count++;
					newpos=rand()%count;
					current=roomrelations->list[whichroom];
					for(int i=0;i<newpos;current=current->next);
					newpos=current->id;
					if(turncount<current->edgeweight)
						return false;
					else
					{
						turncount-=current->edgeweight;
						if(!move(newpos, roomrelations))
						{
							whichroom=rand()%game.roomlimit;
							return false;

						}
						this->checkbattle(npclist,index,game);
						break;
					}
				}
				else
				{
					if(this->deathcount>10)
						revive();
					else
					{
						deathcount++;
						turncount=0;
						return false;
					}

				}

			}	
		}
		break;
	case strongguy:
		for(;turncount>0;)
		{
			//essentially just move to a random room
			while(true)
			{

				if(this->isalive)
				{
					checkbattle(npclist,index,game);
					graphnode* current=roomrelations->list[whichroom];
					int count=0;
					for (;current!=NULL;current=current->next)
						count++;
					newpos=rand()%count;
					current=roomrelations->list[whichroom];
					for(int i=0;i<newpos;current=current->next);
					newpos=current->id;
					if(turncount<current->edgeweight)
						return false;
					else
					{
						if(!move(newpos,roomrelations))
						{
							whichroom=rand()%game.roomlimit;
							return false;
						}

						turncount-=current->edgeweight;

						this->checkbattle(npclist,index,game);
						break;
					}
				}
				else
				{
					if(this->deathcount>10)
						revive();
					else
					{
						deathcount++;
						turncount=0;
						return false;
					}

				}
			}	
		}
		break;
	case boss:
		for(;turncount>0;)
		{
			//essentially just move to a random room
			while(true)
			{

				if(this->isalive)
				{
					checkbattle(npclist,index,game);
					graphnode* current=roomrelations->list[whichroom];
					int count=0;
					for (;current!=NULL;current=current->next)
						count++;
					newpos=rand()%count;
					current=roomrelations->list[whichroom];
					for(int i=0;i<newpos;current=current->next);
					newpos=current->id;
					if(turncount<current->edgeweight)
						return false;
					else
					{
						if(move(newpos,roomrelations))
						{
							whichroom=rand()%game.roomlimit;
							return false;
						}
						turncount-=current->edgeweight;

						this->checkbattle(npclist,index,game);
						break;
					}
				}
				else
				{
					if(this->deathcount>10)
						revive();
					else
					{
						deathcount++;
						turncount=0;
						return false;
					}

				}
			}	
		}
	default:
		return true;
	}
	return false;
}

void character::displayinfo()
{
	cout<<"name\t\t"<<name.c_str()<<endl
		<<"turncount\t\t"<<turncount<<endl
		<<"whichroom\t\t"<<whichroom<<endl
		<<"level\t\t"<<level<<endl
		<<"xp\t\t"<<xp<<endl;

	cout<<"health\t\t"<<health<<endl
		<<"strength\t\t"<<strength<<endl
		<<"inteligence\t\t"<<inteligence<<endl
		<<"wisdom\t\t"<<wisdom<<endl
		<<"endurance\t\t"<<endurance
		<<endl;

	cout<<"speed\t\t"<<speed<<endl
		//	<<"armor\t"<<armor
		<<"wealth\t\t"<<wealth<<endl
		<<"agility\t\t"<<agility<<endl
		<<"charisma\t\t"<<charisma<<endl;

	cout//<<"maxweight\t"<<maxweight
		<<"currentweight\t\t"<<chestc.weight+headc.weight+forearmc.weight+shoulderc.weight+upperlegc.weight+lowerlegc.weight+bootsc.weight+glovesc.weight+shieldc.weight+leftarmc.weight+rightarmc.weight;
	cout<<endl;


}
void character::checkbattle(character* npclist,int i, gameengine& game)
{
	//this is terrible
	for(int j=0;j<game.charactercount;j++)
		if(npclist[j].whichroom==whichroom&&j!=i)
			//forces a battle, at least it does if i define battle function to do that
		{

			if(npclist[j].isalive)
			{		
				if(npclist[i].type==character::human)
					cout<<"you encounter an enemy, and engage in a battle poets will sing of"<<endl;
				if(npclist[j].type==character::human)
					cout<<"you have been approached by a being of undescibable evil.  you attempt to ready yourself as the creature approaches you."<<endl;
				bool win=npclist[i].battle(npclist[j]);

				if(!win)
				{

					if(npclist[i].type==character::human)
					{
						cout<<"ohh no, you have been slain."<<endl;
						game.end();
					}

					else if(npclist[j].type==character::human)
						cout<<"good job, you defeated your enemy"<<endl;
				}
				else if(win)
				{

					if(npclist[j].type==character::human)
					{
						cout<<"ohh no, you have been slain."<<endl;
						game.end();
					}

					else if(npclist[i].type==character::human)
					{
						cout<<"good job, you defeated your enemy"<<endl;
						return;
					}
				}
			}
		}
}

bool character::checktreasure(gameengine& game)
{

	if(this->whichroom==game.treasureroom)
		return true;
	else return false;


}

void character::revive()
{
	this->deathcount=0;
	this->isalive=true;
	whichroom=0;
}

void character::defend(int i)
{
	int armorsum=this->chestc.defencebonus+headc.defencebonus+forearmc.defencebonus+shoulderc.defencebonus+upperlegc.defencebonus+lowerlegc.defencebonus+bootsc.defencebonus+glovesc.defencebonus+shieldc.defencebonus;

	//if you are lucky, you can defend twice as much
	i-=((agility+charisma+endurance+speed+armorsum)/5);
	if((rand()%(1000-(wisdom+inteligence+agility)))>1000)
		i=i*2;
	if(i<0)
		i=0;
	if(!(i<0))
	{
		if(this->type==human)
			cout<<"player  is dealt "<<i<<" damage"<<endl;
		this->health-=i;
	}

	if(!(this->health>0))
	{
		cout<<"player  has been killed"<<endl;
		this->isalive=false;
	}
}


void character::attack(character & enemy)
{
	int attackvalue=0;
	int attackbonus=0;
	attackbonus=leftarmc.damagebonus+rightarmc.damagebonus;
	attackvalue=((attackbonus+strength+inteligence+wisdom+speed)/5+(attackbonus+strength+inteligence+wisdom+speed));

	/*	//critical hit
	if((rand()%(1000-(wisdom+inteligence+agility)))>10000)
	attackvalue=attackvalue*2;
	*/
	enemy.defend(attackvalue);

}

void character::displaypaths(graph* roomrelations)
{
	int i=0;

	for(graphnode* current=roomrelations->list[this->whichroom];current!=NULL;current=current->next)
	{
		cout<<"room :"<<current->id<<"\t"<<"weight :"<<current->edgeweight<<"\t"<<"prob for move:"<<current->probability<<endl;
	}
	if(roomrelations->list[whichroom]==NULL)
	{	
		cout<<" oh no! it seems the room has folded in on itself, leaving you trapped.  well, at least you kinda had fun while it lasted"<<endl;
		cout<<"gameover"<<endl;
		exit(0);
	}
}

bool character::move(int i,graph *roomrelations)
{
	int temp=roomrelations->list[whichroom]->probability;
	if(rand()%100>temp)
		return false;
	//since i didnt pass the graph, this is pretty much all that movement can do.
	this->whichroom=i;
	return true;

}

bool character::battle(character& enemy)
{
	while(this->isalive&&enemy.isalive)
	{
		attack(enemy);
		if(enemy.isalive)
		{
			enemy.attack(*this);
			if(!this->isalive)
			{	
				enemy.xp+=rand()%strength;
				enemy.wealth+=rand()%wisdom;	
				return false;
			}
		}
		else {
			xp+=rand()%enemy.strength;
			wealth+=rand()%enemy.wisdom;
			return true;
		}
	}


}

void character::updatecharacter(int str, int intel, int wis, int endur, int spd, int agil, int charis, std::string n, int room, bool alive, int deathct)
{
	strength=str;inteligence=intel;wisdom=wis;endurance=endur;speed=spd;agility=agil;charisma=charis;name=n;whichroom=room;isalive=alive;deathcount=deathct;
	health=strength+endurance+wisdom+inteligence;
}