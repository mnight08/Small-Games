import pygame
import random
import math
from pygame.locals import *
import sys
from vector import Vector2
import pygame.sprite
import pygame.rect
import pygame.mixer
from pygame.mixer import music
from constants import *
from gamesprite import GameSprite
from boxSprite import Box
from DeathBox import DeathBox
import MapHandler
##print SCREENWIDTH

#get the boxes taht the Player is colliding with and react accordingly
def checkcollision(Boxes,Player,changeintime):
    collidingwith=pygame.sprite.spritecollide(Player,Boxes,False)
    for a in collidingwith:
        a.applyEffect(Player)


def update(Player,changeinttime):
    Player.update(changeintime)

def findDistance(VecA, VecB):
    return math.sqrt(pow(VecA.x - VecB.x, 2) + pow(VecA.y - VecB.y, 2))

# D: Scroll method. Checks if the player sprite crosses a boundry
#    and corrects all the objects by the amount crossed
def scroll(Player, allGroup):
    correctionX = 0
    correctionY = 0
    scrollRight = 0
    scrollLeft = 0
    if Player.velocity.x > 0: #if ball is moving right
        if Player.pos.x < 100: # Player is too far to the left
            correctionX = 100 - Player.pos.x
            scrollRight = correctionX
        if Player.pos.x + Player.width > 150: # Player is too far to the right
            correctionX = Player.pos.x + Player.width - 150
            scrollLeft = -min(correctionX, 35) #so it slides when changing direction
    elif Player.velocity.x < 0: #if ball is moving left
        if Player.pos.x + Player.width < SCREENWIDTH - 150: # Player is too far to the left
            correctionX = SCREENWIDTH - 150 - Player.pos.x - Player.width
            scrollRight = min(correctionX, 35) #so it slides when changing direction
        if Player.pos.x + Player.rect.width > SCREENWIDTH - 100: # Player is too far to the right
            correctionX = Player.pos.x + Player.rect.width - SCREENWIDTH - 100
            scrollLeft = -correctionX

    if (scrollRight != 0):
        correctionX = scrollRight
    elif (scrollLeft != 0):
        correctionX = scrollLeft
    else:
        correctionX = 0
        
    if Player.pos.y < 400: # Player is too far up
        correctionY = 400 - Player.pos.y
    elif Player.pos.y + Player.rect.height > 600: # Player is too far down
        correctionY = 600 - Player.pos.y - Player.rect.height

    #go through each sprite if there is an offset and corrects all the sprites
    if correctionX != 0:
        for sprite in allGroup:
            sprite.pos.x += correctionX
        Player.spawn.x += correctionX
    if correctionY != 0:
        for sprite in allGroup:
            sprite.pos.y += correctionY
        Player.spawn.y += correctionY

    
            
    ##print(str(Player.pos.x) + " " + str(Player.pos.y))

#initialize audio settings
pygame.mixer.pre_init(44100,-16,4, 512)
pygame.init()
pygame.mixer.init()
pygame.mixer.music.load( "../res/Sounds/ProgrammableLoveSong.wav" )
pygame.mixer.music.play(-1)


#initialize screen settings.
#D: was too high for my screen, couldn't see the bottom(SCREENWIDTH,SCREENHEIGHT) )
#A: I think it is better to change constants in the file constants.py, or move them to the top of this file.
screen = pygame.display.set_mode((0,0), pygame.FULLSCREEN )
screenInfo = pygame.display.Info()
SCREENWIDTH = screenInfo.current_w
SCREENHEIGHT = screenInfo.current_h
#screen = pygame.display.set_mode((SCREENWIDTH,SCREENHEIGHT) )
pygame.display.set_caption( "Ballin'" )



#create groups
Boxes=pygame.sprite.Group()
All=pygame.sprite.Group()

#add player
#Player=GameSprite(Vector2(100, 200))
#Player.velocity=STARTINGVELOCITY
#All.add(Player)

def setBoxSides(boxMatrix, i, j, box):
    if (i > 0):
        if boxMatrix[i-1][j] == 1:
            box.disableTop = True
    if (i < len(boxMatrix) - 1):
        if boxMatrix[i+1][j] == 1:
            box.disableBottom = True
    if (j > 0):
        if boxMatrix[i][j-1] == 1:
            box.disableLeft = True
    if (j < len(boxMatrix[i]) - 1):
         if boxMatrix[i][j+1] == 1:
            box.disableRight = True

#add boxes
#boxMatrix = MapHandler.LoadMap("testLevel1.bmp")
boxMatrix = MapHandler.LoadMap("testLevel4.bmp")
#boxMatrix = MapHandler.LoadMap("newboxtest.bmp")
for i in range(len(boxMatrix)):
    for j in range(len(boxMatrix[i])):
        if boxMatrix[i][j] == 1:
            box = Box(Vector2((j * 40), i * 40))
            setBoxSides(boxMatrix, i, j, box)
            All.add(box)
            Boxes.add(box)
        elif boxMatrix[i][j] == 2:
            Player=GameSprite(Vector2(j * 40, i* 40))
            Player.velocity=STARTINGVELOCITY
            All.add(Player)
        elif boxMatrix[i][j] == 3:
            box = DeathBox(Vector2((j * 40), i * 40))
            All.add(box)
            Boxes.add(box)
        elif boxMatrix[i][j] == 4:
            box = Box(Vector2((j * 40), i * 40),"../res/Boxes/speedBox.png")
            box.leftEffect=box.speedupEffect
            box.rightEffect=box.speedupEffect
            box.topEffect=box.speedupEffect
            box.bottomEffect=box.speedupEffect            
            All.add(box)
            Boxes.add(box)
        elif boxMatrix[i][j] == 5:
            box = Box(Vector2((j * 40), i * 40),"../res/Boxes/springBox.png")
            box.leftEffect=box.springEffect
            box.rightEffect=box.springEffect
            box.topEffect=box.springEffect
            box.bottomEffect=box.springEffect
            All.add(box)
            Boxes.add(box)
        elif boxMatrix[i][j] == 6:
            b=Box(Vector2(0,0));
            box = Box(Vector2((j * 40), i * 40),"../res/Boxes/goalBox.png","../res/Sounds/victory.wav")
            box.leftEffect=box.goalEffect
            box.rightEffect=box.goalEffect
            box.topEffect=box.goalEffect
            box.bottomEffect=box.goalEffect

            All.add(box)
            Boxes.add(box)
        elif boxMatrix[i][j] == 7:
            b=Box(Vector2(0,0));
            box = Box(Vector2((j * 40), i * 40),"../res/Boxes/stickyBox.png")
            box.leftEffect=box.stickyEffect
            box.rightEffect=box.stickyEffect
            box.topEffect=box.stickyEffect
            box.bottomEffect=box.stickyEffect
            All.add(box)
            Boxes.add(box)
        elif boxMatrix[i][j] == 8:
            pass
        elif boxMatrix[i][j] == 9:
            pass
'''            
for i in range(0,6):
    box = Box(Vector2(160 + (i * 40), 400))
    All.add(box)
    Boxes.add(box)
box = Box(Vector2(360, 360))
All.add(box)
Boxes.add(box)
for i in range (0, 20):
    box = Box(Vector2(0 + (i * 40), 520))
    All.add(box)
    Boxes.add(box)
box = Box(Vector2(0, 480))
All.add(box)
Boxes.add(box)
'''
'''
carl=GameSprite(Vector2(100, 200))
carl.velocity=Vector2(.1,.2)
All.add(carl)
'''

'''
#this generates some random boxes.  Set RANDOMBOXCOUNT to zero in constants.py to skip, 
i=0
for i in range (0,RANDOMBOXCOUNT):
    box=Box(Vector2(random.randint(-4*SCREENWIDTH,4*SCREENWIDTH),random.randint(-4*SCREENHEIGHT,4*SCREENHEIGHT)))
    if(not pygame.sprite.spritecollide(box,All,False)):
        i+=1
        All.add(box)
        Boxes.add(box)
'''

        
starttime=pygame.time.get_ticks()
changeintime=0                                       
while True:
    for evt in pygame.event.get():
        if evt.type == QUIT or (evt.type == KEYDOWN and evt.key == K_ESCAPE):
            pygame.quit()
            sys.exit()
        elif evt.type == KEYDOWN and evt.key == K_SPACE:
            #if(Player.bouncingVector.magnitude()==0):
            #    Player.bouncingVector=BOUNCESTRENGTH
            Player.bounce=True
        elif evt.type == KEYUP and evt.key == K_SPACE:
            #if(Player.bouncingVector.magnitude()==0):
            #    Player.bouncingVector=BOUNCESTRENGTH
            Player.bounce=False
    changeintime=pygame.time.get_ticks()-starttime
    starttime=pygame.time.get_ticks()
    #handleevents(Player)
    #handlecollisions(Boxes,Player,changeintime)
    checkcollision(Boxes,Player,changeintime)
   ## update(Player,changeintime)
    scroll(Player, All)
    All.update(changeintime);
    
    
    screen.fill((255,255,0))
    All.draw(screen)
    pygame.display.flip()

class Ballin:
    self.res_path="..\res"

    def start_game(self,level):

        pass

    def __init__(self):
        pass

