import pygame
from boxSprite import Box
from constants import *
from vector import Vector2

class DeFocusBox(Box):
    def __init__(self, loc,topGain=DEFAULTTOPGAIN,bottomGain=DEFAULTBOTTOMGAIN,leftGain=DEFAULTLEFTGAIN,rightGain=DEFAULTRIGHTGAIN,topEffect=None,bottomEffect=None,leftEffect=None,rightEffect=None):
        super(DeFocusBox,self).__init__(loc)
        self.image=pygame.image.load( "boxes/emptyBox.png" ).convert_alpha()
        
    def applyEffect( self, player):
        player.focused = False

