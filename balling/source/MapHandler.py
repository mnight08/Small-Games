import pygame
from vector import Vector2
import pygame.sprite
import pygame.rect
import pygame.pixelarray
import pygame.surface
from constants import *

#returns an array of integers.
def LoadMap(mapName):
    
    image=pygame.image.load( "../res/maps/" + mapName ).convert()
    width = image.get_width()
    height = image.get_height()
    gameMatrix = [[0 for col in range(width)] for row in range(height)]
    #print len(gameMatrix)
    #print len(gameMatrix[0])
    pxarray = pygame.PixelArray (image)

    for i in range(0, height):
        for j in range(0, width):
            if pxarray[j][i] == image.map_rgb((0,0,0)):
                gameMatrix[i][j] = 1
            elif pxarray[j][i] == image.map_rgb((0,0,255)): #blue
                gameMatrix[i][j] = 2
            elif pxarray[j][i] == image.map_rgb((255,0,0)): #red
                gameMatrix[i][j] = 3
            elif pxarray[j][i] == image.map_rgb((0,255,0)): #green
                gameMatrix[i][j] = 4
            elif pxarray[j][i] == image.map_rgb((255,255,0)): #yellow
                gameMatrix[i][j] = 5
            elif pxarray[j][i] == image.map_rgb((255,0,255)): #purple
                gameMatrix[i][j] = 6
            elif pxarray[j][i] == image.map_rgb((0,255,255)): #teal
                gameMatrix[i][j] = 7
            elif pxarray[j][i] == image.map_rgb((200,200,200)): #silver
                gameMatrix[i][j] = 8
            elif pxarray[j][i] == image.map_rgb((100,100,100)): #gray
                gameMatrix[i][j] = 9
    return gameMatrix

#0 is nothing 
#1 is a wall  (0,0,0)
#2 is the spawn (0,0,255)
#3 is a deathbox (255,0,0)
#4 is speedbox  (0,255,0)
#5 is springbox  (255,255,0)
#6 is goalbox     (255,0,255)
#7 is stickybox    (0,255,255)
#8 is a defocus box (200,200,200)
#9 is a refocus box  (100,100,100)
