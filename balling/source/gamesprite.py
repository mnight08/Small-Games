import pygame
from vector import Vector2

import pygame.sprite
import pygame.rect
from constants import *

#class representing the player
class GameSprite(pygame.sprite.Sprite):
    def __init__(self, pos):
        super(GameSprite,self).__init__()
        self.spawn = Vector2(pos.x,pos.y)
        self.pos=Vector2(pos.x,pos.y)
        self.prevPos = Vector2(self.pos.x, self.pos.y)
        self.velocity=Vector2(0,0)
        self.acceleration=Vector2(0,0)
        self.image=pygame.image.load( "../res/Balls/ball.png" ).convert_alpha()
        self.rect=pygame.rect.Rect(pos.x,pos.y,self.image.get_width(),self.image.get_height())
        self.width = self.image.get_width()
        self.height = self.image.get_height()
        self.skipGrav = False;
        self.death=False;
        self.sticky=False;
        self.bouncingVector=Vector2(0,0)
        self.focused = True
        #used to make sure the player cant bounce to often
        self.bounceWait=0
        #A: if the object is colliding, and we correct its position, and we are checking for
        #future positions, then we already did the update with the correction.  we can
        #go about this different ways, but this seemed like the most straight forward.
        #if we calculate the time to collision, then make the correction, when the objects get
        #to update, they can decrement the change in time by that amount, and then update.
        #in this case, the correction really only does a partial update.  For now though
        #I believe that it shouldnt be a big deal to just not update if an object is corrected.

        #D: K, I agree
        self.dontupdateposition=False
        self.bounce=False

    def respawn(self):
        self.pos=spawn;
    def update(self,changeintime):
        #D: Lets put this back in when we have something to land on.
        #self.velocity=self.velocity+self.acceleration*changeintime
        if(self.bounce == True and self.skipGrav):#self.bounceWait <= changeintime):
            self.velocity=self.bouncingVector
            #self.velocity=self.velocity+self.bouncingVector
            #self.bounceWait=1000/BOUNCINGRATE
            self.skipGrav = False
        else:
            self.bounceWait=self.bounceWait-changeintime


        if(self.sticky):
            self.velocity=self.prevVelocity
            self.sticky=False;
        #I think its best we keep our constants in one file.  GRAVITY is defined in constants.py
        if(not self.dontupdateposition):
            if (not self.skipGrav):
                if (self.velocity.y < 0 and self.bounce == False):
                    self.velocity.y += GRAVITY.y * changeintime * 3
                else:
                    self.velocity.y += GRAVITY.y * changeintime
                
            else:
                self.velocity.y = 0
            self.skipGrav = False
            self.prevPos = Vector2(self.pos.x, self.pos.y)
            self.pos=self.pos + (self.velocity * changeintime)
            #this seems to work better
            self.rect.center = ((round(self.pos.x + (self.rect.width / 2)),round(self.pos.y + (self.rect.height / 2))))
            #self.rect.move_ip(round(self.pos.x),round(self.pos.y))
        if(self.death):
            self.respawn();
            
        #self.bounce=False
