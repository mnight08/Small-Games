import pygame
import random
from constants import *
from vector import Vector2



class Sprite:   
     def __init__(self,image):
          self.pos=Vector2(0,0)
          self.velocity=Vector2(0,0)
          self.oldvelocity=Vector2(0,0)
          self.image=pygame.image.load( image ).convert()
          self.width=self.image.get_width()
          self.height=self.image.get_height()
          self.radius=Vector2(self.width/2,self.height/2).norm()
          self.setcirclecollisiontest()
          self.error=0
          self.setrandommass()
          self.drawbounds=True
     #Check if self contains any of the corners of test, and vice versa.
     #if they do not, they are not colliding
     def rectangletest(self,test):
          return False
     def movetoboundarycircle(self,other):
          other.pos=(other.pos-self.pos).normalize()*self.radius-(self.pos-other.pos).normalize()*other.radius+self.pos
          #print "stub call"
     def movetoboundaryrectangle(self,other):
          #print "stub call"
     def drawcollisionrectangle(self,screen):
          #print "stub call"
         #pygame.draw.rect(screen,(100,100,100),(self.width,self.height)
     def drawcollisioncircle(self,screen):
          pygame.draw.circle(screen,(0,0,230),self.pos.tointegertuple(),int(self.radius))
     def circletest(self,test):
          if(self.radius+test.radius>=(self.pos-test.pos).norm()):
               return True
          else:
               return False
     def reacttocollision(self,other):
          other.oldvelocity=other.velocity
          other.velocity=(other.oldvelocity*(other.mass-self.mass)+2*other.mass*self.oldvelocity)/(self.mass+other.mass)
          self.movetoboundary(other)
     def setcirclecollisiontest(self):
          self.collisiontest=self.circletest
          self.drawcollisionbounds=self.drawcollisioncircle
          self.movetoboundary=self.movetoboundarycircle
          
     def setrectanglecollisiontest(self):
          self.collisiontest=self.rectangletest
          self.drawcollisionbounds=self.drawcollisionrectangle
          self.movetoboundary=movetoboundaryrectangle
     def setrandommass(self):
          self.mass=random.randrange(1,100,1)
     def setrandomtarget(self):
         self.target=Vector2(random.randrange(0,SCREENWIDTH,1),random.randrange(0,SCREENHEIGHT,1))
     def setrandomposition(self):
         self.pos=Vector2(random.randrange(0,SCREENWIDTH,1),random.randrange(0,SCREENHEIGHT,1))
     def setrandomvelocity(self):
         self.velocity=Vector2(random.uniform(0,1),random.uniform(0,1))
         self.oldvelocity=self.velocity
     def update(self,changeintime):
         self.pos=self.pos+changeintime*self.velocity
         self.velocity=self.velocity+GRAVITY
     def settargetvector(self,target):
         self.target=target
         self.velocity=self.gettargetvector().normalize()*self.velocity.norm()
     def settargettuple(self,target):
         self.settargetvector(Vector2(target[0],target[1]))
     def gettargetvector(self):
         return (self.target-self.pos).normalize()
     def draw(self,screen):
          if(self.drawbounds):
               self.drawcollisionbounds(screen)
          screen.blit( self.image, (self.pos.x,self.pos.y) )
