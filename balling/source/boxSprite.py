import pygame
from vector import Vector2
import pygame.sprite
import pygame.rect
import pygame.mixer
from gamesprite import GameSprite
from constants import *


class Box(pygame.sprite.Sprite):
    #The effect arguments are functions that determine the effects that the different sides of the box have on the player
    def __init__(self, loc,image="../res/boxes/wallBox.png",sound="../res/Sounds/sideHit.wav",topEffect=None,bottomEffect=None,leftEffect=None,rightEffect=None,topGain=DEFAULTTOPGAIN,bottomGain=DEFAULTBOTTOMGAIN,leftGain=DEFAULTLEFTGAIN,rightGain=DEFAULTRIGHTGAIN,):
        super(Box,self).__init__()
        self.pos= loc
        self.image=pygame.image.load( image ).convert()
        self.rect=pygame.rect.Rect(self.pos.x,self.pos.y,self.image.get_width(),self.image.get_height())
        self.width = self.image.get_width()
        self.height = self.image.get_height()
        
        self.disableTop = False
        self.disableLeft = False
        self.disableRight = False
        self.disableBottom = False
        self.hitSound = pygame.mixer.Sound( sound )
        
        #check if different wall effects were given, otherwise, default to bounce
        if topEffect == None:
            topEffect=self.bounceEffect
        if bottomEffect == None:
            bottomEffect=self.bounceEffect
        if leftEffect == None:
            leftEffect=self.bounceEffect
        if rightEffect == None:
            rightEffect=self.bounceEffect

        #set the effects that the different edges have
        self.topEffect=topEffect
        self.bottomEffect=bottomEffect
        self.leftEffect=leftEffect
        self.rightEffect=rightEffect

        #set the values the different sides scale velocity when an object hits them
        self.topGain=topGain
        self.bottomGain=bottomGain
        self.leftGain=leftGain
        self.rightGain=rightGain

        
    def bounceEffect(self,player,sideHit):
        if sideHit == "top":
            #player.velocity.y = -self.topGain*player.velocity.y
            player.pos.y = self.pos.y - player.height + 1
            player.skipGrav = True
        elif sideHit == "bottom":
            player.velocity.y = -self.topGain*player.velocity.y
            player.pos.y = self.pos.y + self.height
        elif sideHit == "left":
            self.hitSound.play()
            player.velocity.x = -self.rightGain * abs(player.velocity.x)
        elif sideHit == "right":
            self.hitSound.play()
            player.velocity.x = self.leftGain * abs(player.velocity.x)
        player.antiGrav=False
        #if(player.bouncingVector.magnitude()==0):
        player.bouncingVector=Vector2(player.velocity.x,-1)
        
    def update(self,changeintime):
        self.rect.center = ((round(self.pos.x + (self.rect.width / 2)),round(self.pos.y + (self.rect.height / 2))))
    #Using y = mx + b to determin if point v3 is above the line created by points v1 and v2
    def isLeft(self, v1, v2, v3):
        m = (v1.y - v2.y)/(v1.x - v2.x) #determine slope
        b = v1.y - (m * v1.x)           #solve for b
        return v3.y > (m * v3.x) + b

    #Determins the side of the box that was hit
    def sideHit( self, player):
        #Get the center of the other selfect's prev position
        center = Vector2(player.pos.x + (player.width/2), player.pos.y + (player.height/2))
        
        topLeft = Vector2(self.pos.x, self.pos.y)
        topRight = Vector2(self.pos.x + self.width, self.pos.y)
        bottomLeft = Vector2(self.pos.x, self.pos.y + self.height)
        bottomRight = Vector2(self.pos.x + self.width, self.pos.y + self.height)
        ##print(str(player.prevPos.x + player.width <= self.pos.x + 1))
        ##print ((player.prevPos.y, player.pos.y, player.height, self.pos.y))
        #if player.prevPos2.y + player.height > self.pos.y and player.prevPos2.y < self.pos.y + self.height:# \
        #and (player.prevPos.x + player.width < self.pos.x or player.prevPos.x > self.pos.x + self.width):
            #return "side"

        if (player.prevPos.y + player.height - 1 <= self.pos.y ):
            if not self.disableTop:
                return "top"
            else:
                return "disabled"
        if (player.prevPos.y >= self.pos.y + self.height - 1):
            if not self.disableBottom:
                return "bottom"
            else:
                return "disabled"
        if (player.prevPos.x < self.pos.x + (self.width / 2)):
            if not self.disableLeft:
                return "left"
            else:
                return "disabled"
        else:
            if not self.disableRight:
                return "right"
            else:
                return "disabled"
        return "side"
        #if object is on the bottom-left
        if self.isLeft(topLeft, bottomRight, center):
            return "bottom"
        else:
            return "top"
        '''
            if self.isLeft(bottomLeft, topRight, center): #collision on bottom
                return "bottom"
            else: # collision on left side
                return "left"
        else: #is on top right
            if self.isLeft(bottomLeft, topRight, center): #collision on right side
                return "right"
            else: # collision on top
                return "top"
        '''
    def springEffect(self,player,sideHit):
        #self.correct(player,sideHit)
        if(sideHit=="top"):
            player.velocity.y=-1.25;
        elif (sideHit == "left" ):
            player.velocity.x=-1.25;
        elif (sideHit == "right"):
            player.velocity.x=1.25;
        elif (sideHit=="bottom"):
            player.velocity.y=1.25;

    def speedupEffect(self,player,sideHit):
        player.velocity=1.25*player.velocity.unitvector()


    def stickyEffect(self,player,sideHit):
        player.velocity=.25*player.velocity.unitvector()


    
     #   player.bouncingVector=player.velocity
    #    player.velocity=Vector2(0,0);

    def goalEffect(self,player,sideHit):
        self.hitSound.play();
        #print "You Beat ME!!!!!!!";
        
    def correct(self,player,sideHit):
        if(sideHit=="top"):
            player.pos.y = self.pos.y - player.height + 1
        elif (sideHit == "left" ):
            player.pos.x=self.pos.x-player.width
        elif (sideHit == "right"):
            player.pos.x=self.pos.x+self.width;
        elif (sideHit=="bottom"):
            player.pos.y = self.pos.y + self.height

    #not functioning properly.
    def antigravityEffect(self,player,sideHit):
        self.correct(player,sideHit)
        player.antiGrav=True
        if(sideHit == "left" or sideHit == "right"):
            player.bouncingVector=Vector2(-player.velocity.x,0)
            player.velocity.x=0;
            
        else:
            player.bouncingVector=Vector2(0,-player.velocity.y)
            player.velocity.y=0;

    

    def applyEffect( self, player):
        sideHit = self.sideHit(player)
        if sideHit == "top":
            #player.velocity.y = -self.topGain*player.velocity.y
            self.topEffect(player,sideHit)
        elif sideHit == "bottom":
            self.bottomEffect(player,sideHit)
        elif sideHit == "left":
          #  self.hitSound.play()
            self.leftEffect(player,sideHit)
        elif sideHit == "right":
           # self.hitSound.play()
            self.rightEffect(player,sideHit)
            ##print player.velocity
        #if sideHit == "right":
            #player.velocity.x = -1 * abs(player.velocity.x)

        
