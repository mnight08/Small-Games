import pygame
from boxSprite import Box
from constants import *
from vector import Vector2


#Box that instantly kills the player if touched. 
class DeathBox(Box):
    def __init__(self, loc,topGain=DEFAULTTOPGAIN,bottomGain=DEFAULTBOTTOMGAIN,leftGain=DEFAULTLEFTGAIN,rightGain=DEFAULTRIGHTGAIN,topEffect=None,bottomEffect=None,leftEffect=None,rightEffect=None):
        super(DeathBox,self).__init__(loc)
        self.image=pygame.image.load( "../res/Boxes/deathBox.png" ).convert()
        self.hitSound = pygame.mixer.Sound( "../res/Sounds/death.wav" )
        
    def applyEffect( self, player):
        player.pos = Vector2(player.spawn.x, player.spawn.y)
        self.hitSound.play()
        pass
