package guessinggame;

import java.util.Scanner;
import java.util.Random;
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author mnight08
 */
public class GuessingGame {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        int userguess = 0;
        int secretnumba = 0;
        int score = 0;
        Random randgen = new Random();
        secretnumba = randgen.nextInt(100) + 1;
        Scanner scanner = new Scanner(System.in);
        while (true) {
            score++;
            System.out.println("pick a number between 1 and 100");
            userguess = scanner.nextInt();
            if (userguess == secretnumba) {
                break;
            }

            System.out.println("That is incorrect... if you keep this up you wont recieve cake!");

            if (userguess < secretnumba) {
                System.out.println("Too low!!");
            } else {
                System.out.println("Too high!!!");
            }
        }
        System.out.println("That was my number!");
        System.out.print("your score was:" + score +"\n");
        if (score == 1) {
            System.out.println("what is this trickery?");
        } else if (score < 10) {
            System.out.println("Good job!  Please be sure to grab some cake on the way out.");
        } else if (score < 20) {
            System.out.println("Not too bad, i remember when i used to score this bad.");
        } else if (score < 30) {
            System.out.println("getting a");
        } else if (score < 40) {
            System.out.println("thats an impressive score");
        } else if (score < 50) {
            System.out.println("If you were playing golf this would be a very impressive score... your not  playing golf.");
        } else if (score < 60) {
            System.out.println("Its like you're shooting fish in a barrel... a barrel the size of texas.");
        } else if (score < 70) {
            System.out.println("maybe you should stop thinking of fish analogies for a second and you would have an impressive score.");
        } else if (score < 80) {
            System.out.println("well at  least you finished.");
        } else if (score < 90) {
            System.out.println("you do realize that a high score is bad right?");
        } else if (score < 100) {
            System.out.println("well this is awkward, i kinda stopped paying attention 40 attempts ago.");
        } else {
            System.out.println("Wow... you're still here? That's just really bad.  Way to keep at it though, I mean persistence is worth something right? ... Right?!  Well nothing right now.  The cake ran out some time back.  I would have told you, but you seemed distracted.");
        }
    }
}
