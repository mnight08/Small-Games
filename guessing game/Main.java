package guessinggame;

import java.util.Scanner;
import java.util.Random;
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author alex martinez 10308748
 *	part 1 hwk 2
 */
public class Main {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        int userguess = 0;
        int secretnumba = 0;
        int score = 0;
        int low = 0;
        int high = 0;
        Random randgen = new Random();

        Scanner scanner = new Scanner(System.in);
        System.out.println("please enter a lower range of numbers to guess from:");
        low = scanner.nextInt();
        System.out.println("please enter a higher range of numbers to guess from:");
        high = scanner.nextInt();
        secretnumba = randgen.nextInt(high) + low;
        while (true) {
            score++;
            System.out.println("pick a number between " + low + " and " + high);
            userguess = scanner.nextInt();
            if (userguess == secretnumba) {
                break;
            }

            System.out.println("That is incorrect... if you keep this up you wont recieve cake!");

            if (userguess < secretnumba) {
                System.out.println("Too low!!");
            } else {
                System.out.println("Too high!!!");
            }
        }
        System.out.println("That was my number!");
        System.out.println("your score was "+ score);
        if (score == 1) {
            System.out.println("what is this trickery?");
        } else if (score < 10) {
            System.out.println("Good job!  Please be sure to grab some cake on the way out.");
        } else if (score < 20) {
            System.out.println("Not too bad, i remeber when i used to score this bad.");
        } else if (score < 30) {
            System.out.println("how old are you again?");
        } else if (score < 40) {
            System.out.println("\"thats an impressive score!\" ... is that what you want to here, because you wont if you cant do better than this.");
        } else if (score < 50) {
            System.out.println("If you were playing golf this would be a very impressive score... your not  playing golf.");
        } else if (score < 60) {
            System.out.println("Its like you shooting fish in a barrel... a barrel the size of texas.");
        } else if (score < 70) {
            System.out.println("maybe you should stop thinking of fish analogies for a second and you would have an impresive score.");
        } else if (score < 80) {
            System.out.println("well at  least you finished.");
        } else if (score < 90) {
            System.out.println("you do realize that a high score is bad right?");
        } else if (score < 100) {
            System.out.println("well this is awkward, i kinda stopped paying attention 40 attempts ago.");
        } else {
            System.out.println("Wow... your still here? thats just really bad.  way to keep at it though, i mean persistence is worth something right? ... right?!  well nothing right now.  the cake ran out some time back.  i would have told you, but you seemed distracted.");
        }
    }
}
