
import java.awt.*;
import java.util.*;
import javax.swing.*;

public class Map {
    //list of portals and players

    PriorityQueue<Player> list;
    LinkedList<Player> removelist;

    Map() {
        list = new PriorityQueue<Player>();
        removelist = new LinkedList<Player>();
    }

    public void update() {
        Player temp = null;

        for (Iterator<Player> it = list.iterator(); it.hasNext();) {
            temp = it.next();
            temp.isinteractingwith = temp.checkcollision(list);
            temp.update();
        }
        for (Iterator<Player> it = removelist.iterator(); it.hasNext();) {
            temp = it.next();
            list.remove(temp);
        }
        removelist.clear();
    }

    public boolean draw(Graphics g, Player view) {
        for (Iterator<Player> it = list.iterator(); it.hasNext();) {
            it.next().draw(g, view);
        }
        return true;
    }

    //this will probably not stay in the final version
    public void initializecharacters() {
        Random rand = new Random();
        Player temp;
        for (int i = 0; i < 10; i++) {
            temp = new Player();
            temp.initializepanda();
            if (rand.nextInt(2) % 2 == 0) {
                temp.xvel = rand.nextInt(10);
                temp.yvel = rand.nextInt(10);
            } else {
                temp.xvel = -rand.nextInt(10);
                temp.yvel = -rand.nextInt(10);

            }
            temp.type = "panda";
            temp.currentmap = this;

            temp.updatepos(rand.nextInt(Main.width - temp.width), rand.nextInt(Main.height - temp.height));
            list.add(temp);
        }
        // currentplayer=characters.element();


    }



   /* public void initializedoorsmap1(Map[] maps) {
        Player door;
        door = new Player();
        door.initializedoor(list, 20, -50);

    }
*/
    public void buildengineering() {

        Player temp;
        int startx = 0;
        int starty = 0;
        temp= new Player();
        temp.intitializebackground( startx, starty,"engineeringFloor.png", list);
        temp= new Player();
        temp.intializeinvisiblebox( 0,  0,  62,  1500,list);

        temp= new Player();
        temp.intializeinvisiblebox( 1440,  10,  1500,  1500,list);


        temp= new Player();
        temp.intializeinvisiblebox( 0,  0,  1500,  62,list);

                temp= new Player();
        temp.intializeinvisiblebox( 0,  1438,  1500,  1500,list);

                temp= new Player();
        temp.intializeinvisiblebox( 85,  62,  435,  135,list);

        temp= new Player();
        temp.intializeinvisiblebox( 85,  205,  435,  270,list);


        temp= new Player();
        temp.intializeinvisiblebox( 85,  335,  435,  380,list);

        temp= new Player();
        temp.intializeinvisiblebox( 85,  435,  435,  470,list);

        temp= new Player();
        temp.intializeinvisiblebox( 600,  150,  980,  210,list);

        temp= new Player();
        temp.intializeinvisiblebox( 155,  540,  1155,  600,list);

        temp= new Player();
        temp.intializeinvisiblebox( 500,  720,  1440,  775,list);

        temp= new Player();
        temp.intializeinvisiblebox( 175,  1155,  1330,  1215,list);

        temp= new Player();
        temp.intializeinvisiblebox( 600,  150,  650, 540,list);

        temp= new Player();
        temp.intializeinvisiblebox( 1095,  150,  1150,  536,list);

        temp= new Player();
        temp.intializeinvisiblebox( 500,  700,  560,  1165,list);
        temp= new Player();

        temp.intializeinvisiblebox( 910,  365,  1090,  470,list);

        temp= new Player();
        temp.intializeinvisiblebox( 80,  990,  255,  1080,list);

        temp= new Player();
        temp.intializeinvisiblebox( 790,  985,  975,  1075,list);

    }

    public void buildmazelevel() {
        Player wall1;
        int collumns = 39, rows = 25;
        int pwidth = 0;
        int pheight = 0;
        pwidth = (new ImageIcon(Main.class.getResource("pandamaze1.2.png"))).getIconWidth() / collumns;
        pheight = (new ImageIcon(Main.class.getResource("pandamaze1.2.png"))).getIconHeight() / rows;

        wall1 = new Player();
        wall1.initializewall(0, 0, "0westwall.png", list);

        wall1 = new Player();
        wall1.initializewall(1 * pwidth, 11 * pheight, "1infrom0set1.png", list);
        wall1 = new Player();
        wall1.initializewall(1 * pwidth, 24 * pheight, "1infrom0set2.png", list);

        wall1 = new Player();
        wall1.initializewall(2 * pwidth, 11 * pheight, "2infrom0set1.png", list);
        wall1 = new Player();
        wall1.initializewall(2 * pwidth, 24 * pheight, "2infrom0set2.png", list);

        wall1 = new Player();
        wall1.initializewall(3 * pwidth, 0 * pheight, "03rdfrom1set1.png", list);
        wall1 = new Player();
        wall1.initializewall(3 * pwidth, 11 * pheight, "03rdfrom1set2.png", list);
        wall1 = new Player();
        wall1.initializewall(3 * pwidth, 13 * pheight, "03rdfrom1set3.png", list);


        wall1 = new Player();
        wall1.initializewall(4 * pwidth, 0 * pheight, "04infrom0set1(2).png", list);
        wall1 = new Player();
        wall1.initializewall(4 * pwidth, 9 * pheight, "04infrom0set1(3).png", list);
        wall1 = new Player();
        wall1.initializewall(4 * pwidth, 11 * pheight, "04infrom0set1(4).png", list);
        wall1 = new Player();
        wall1.initializewall(4 * pwidth, 13 * pheight, "04infrom0set1(5).png", list);
        wall1 = new Player();
        wall1.initializewall(4 * pwidth, 24 * pheight, "04infrom0set1(6).png", list);



        wall1 = new Player();
        wall1.initializewall(5 * pwidth, 0 * pheight, "5infrom0set2a.png", list);
        wall1 = new Player();
        wall1.initializewall(5 * pwidth, 9 * pheight, "5infrom0set2a(2).png", list);
        wall1 = new Player();
        wall1.initializewall(5 * pwidth, 11 * pheight, "5infrom0set2b.png", list);
        wall1 = new Player();
        wall1.initializewall(5 * pwidth, 13 * pheight, "5infrom0set2c.png", list);
        wall1 = new Player();
        wall1.initializewall(5 * pwidth, 24 * pheight, "5infrom0set3.png", list);

        wall1 = new Player();
        wall1.initializewall(6 * pwidth, 0 * pheight, "6infrom0set1.png", list);
        wall1 = new Player();
        wall1.initializewall(6 * pwidth, 3 * pheight, "6infrom0set2 .png", list);
        wall1 = new Player();
        wall1.initializewall(6 * pwidth, 9 * pheight, "6infrom0set3a.png", list);
        wall1 = new Player();
        wall1.initializewall(6 * pwidth, 11 * pheight, "6infrom0set3b.png", list);
        wall1 = new Player();
        wall1.initializewall(6 * pwidth, 13 * pheight, "6infrom0set3c.png", list);
        wall1 = new Player();
        wall1.initializewall(6 * pwidth, 15 * pheight, "6infrom0set4 .png", list);
        wall1 = new Player();
        wall1.initializewall(6 * pwidth, 24 * pheight, "6infrom0set5.png", list);

        wall1 = new Player();
        wall1.initializewall(7 * pwidth, 0 * pheight, "7 in from 0 set1.png", list);
        wall1 = new Player();
        wall1.initializewall(7 * pwidth, 3 * pheight, "7 in from 0 set2.png", list);
        wall1 = new Player();
        wall1.initializewall(7 * pwidth, 9 * pheight, "7 in from 0 set3a.png", list);
        wall1 = new Player();
        wall1.initializewall(7 * pwidth, 11 * pheight, "7 in from 0 set3b.png", list);
        wall1 = new Player();
        wall1.initializewall(7 * pwidth, 13 * pheight, "7 in from 0 set3c.png", list);
        wall1 = new Player();
        wall1.initializewall(7 * pwidth, 21 * pheight, "7 in from 0 set4.png", list);
        wall1 = new Player();
        wall1.initializewall(7 * pwidth, 24 * pheight, "7 in from 0 set5.png", list);


        wall1 = new Player();
        wall1.initializewall(8 * pwidth, 0 * pheight, "8 in from 0 set1.png", list);
        wall1 = new Player();
        wall1.initializewall(8 * pwidth, 3 * pheight, "8 in from 0 set2.png", list);
        wall1 = new Player();
        wall1.initializewall(8 * pwidth, 9 * pheight, "8 in from 0 set3a.png", list);
        wall1 = new Player();
        wall1.initializewall(8 * pwidth, 11 * pheight, "8 in from 0 set3b.png", list);
        wall1 = new Player();
        wall1.initializewall(8 * pwidth, 13 * pheight, "8 in from 0 set3c.png", list);
        wall1 = new Player();
        wall1.initializewall(8 * pwidth, 21 * pheight, "8 in from 0 set3d.png", list);
        wall1 = new Player();
        wall1.initializewall(8 * pwidth, 24 * pheight, "8 in from 0 set4.png", list);

        wall1 = new Player();
        wall1.initializewall(9 * pwidth, 0 * pheight, "9 in from 0 set1.png", list);
        wall1 = new Player();
        wall1.initializewall(9 * pwidth, 3 * pheight, "9 in from 0 set2.png", list);
        wall1 = new Player();
        wall1.initializewall(9 * pwidth, 9 * pheight, "9 in from 0 set3a.png", list);
        wall1 = new Player();
        wall1.initializewall(9 * pwidth, 11 * pheight, "9 in from 0 set3b.png", list);
        wall1 = new Player();
        wall1.initializewall(9 * pwidth, 19 * pheight, "9 in from 0 set4a.png", list);
        wall1 = new Player();
        wall1.initializewall(9 * pwidth, 21 * pheight, "9 in from 0 set4b.png", list);
        wall1 = new Player();
        wall1.initializewall(9 * pwidth, 24 * pheight, "9 in from 0 set5.png", list);

        wall1 = new Player();
        wall1.initializewall(10 * pwidth, 0 * pheight, "10 in from 0 set1.png", list);
        wall1 = new Player();
        wall1.initializewall(10 * pwidth, 3 * pheight, "10 in from 0 set2.png", list);
        wall1 = new Player();
        wall1.initializewall(10 * pwidth, 9 * pheight, "10 in from 0 set3a.png", list);
        wall1 = new Player();
        wall1.initializewall(10 * pwidth, 11 * pheight, "10 in from 0 set3b.png", list);
        wall1 = new Player();
        wall1.initializewall(10 * pwidth, 19 * pheight, "10 in from 0 set4a.png", list);
        wall1 = new Player();
        wall1.initializewall(10 * pwidth, 21 * pheight, "10 in from 0 set4b.png", list);
        wall1 = new Player();
        wall1.initializewall(10 * pwidth, 24 * pheight, "10 in from 0 set5.png", list);

        wall1 = new Player();
        wall1.initializewall(11 * pwidth, 0 * pheight, "11 in from 0 set1.png", list);
        wall1 = new Player();
        wall1.initializewall(11 * pwidth, 3 * pheight, "11 in from 0 set2.png", list);
        wall1 = new Player();
        wall1.initializewall(11 * pwidth, 9 * pheight, "11 in from 0 set3a.png", list);
        wall1 = new Player();
        wall1.initializewall(11 * pwidth, 11 * pheight, "11 in from 0 set3b.png", list);
        wall1 = new Player();
        wall1.initializewall(11 * pwidth, 13 * pheight, "11 in from 0 set3c.png", list);
        wall1 = new Player();
        wall1.initializewall(11 * pwidth, 21 * pheight, "11 in from 0 set3d.png", list);
        wall1 = new Player();
        wall1.initializewall(11 * pwidth, 24 * pheight, "11 in from 0 set4.png", list);

        wall1 = new Player();
        wall1.initializewall(12 * pwidth, 0 * pheight, "12 in from 0 set1.png", list);
        wall1 = new Player();
        wall1.initializewall(12 * pwidth, 3 * pheight, "12 in from 0 set2a.png", list);
        wall1 = new Player();
        wall1.initializewall(12 * pwidth, 5 * pheight, "12 in from 0 set2b 145pix.png", list);
    //    wall1 = new Player();
      //  wall1.initializewall(12 * pwidth, 11 * pheight, "12 in from 0 set2c.png", list);
        wall1 = new Player();
        wall1.initializewall(12 * pwidth, 21 * pheight, "12 in from 0 set3.png", list);
        wall1 = new Player();
        wall1.initializewall(12 * pwidth, 24 * pheight, "12 in from 0 set4.png", list);

        wall1 = new Player();
        wall1.initializewall(13 * pwidth, 0 * pheight, "13 in from 0 set1.png", list);
        wall1 = new Player();
        wall1.initializewall(13 * pwidth, 3 * pheight, "13 in from 0 set2.png", list);
     //   wall1 = new Player();
    //    wall1.initializewall(13 * pwidth, 11 * pheight, "13 in from 0 set3.png", list);
        wall1 = new Player();
        wall1.initializewall(13 * pwidth, 21 * pheight, "13 in from 0 set4.png", list);
        wall1 = new Player();
        wall1.initializewall(13 * pwidth, 24 * pheight, "13 in from 0 set5.png", list);

        wall1 = new Player();
        wall1.initializewall(14 * pwidth, 0 * pheight, "14 in from 0 set1.png", list);
        wall1 = new Player();
        wall1.initializewall(14 * pwidth, 3 * pheight, "14 in from 0 set2a.png", list);
        wall1 = new Player();
        wall1.initializewall(14 * pwidth, 5 * pheight, "14 in from 0 set2b 570pix.png", list);

        wall1 = new Player();
        wall1.initializewall(15 * pwidth, 0 * pheight, "15 in from 0 set1.png", list);
        wall1 = new Player();
        wall1.initializewall(15 * pwidth, 3 * pheight, "15 in from 0 set2.png", list);
        wall1 = new Player();
        wall1.initializewall(15 * pwidth, 22 * pheight, "15 in from 0 set3.png", list);
        wall1 = new Player();
        wall1.initializewall(15 * pwidth, 24 * pheight, "15 in from 0 set4.png", list);

        wall1 = new Player();
        wall1.initializewall(16 * pwidth, 0 * pheight, "16 in from 0 set1.png", list);
        wall1 = new Player();
        wall1.initializewall(16 * pwidth, 3 * pheight, "16 in from 0 set2.png", list);
        wall1 = new Player();
        wall1.initializewall(16 * pwidth, 22 * pheight, "16 in from 0 set3.png", list);
        wall1 = new Player();
        wall1.initializewall(16 * pwidth, 24 * pheight, "16 in from 0 set4.png", list);

        //names are numbered plus one from this point on

        wall1 = new Player();
        wall1.initializewall(17 * pwidth, 0 * pheight, "18 in from 0 set1a.png", list);
        wall1 = new Player();
        wall1.initializewall(17 * pwidth, 2 * pheight, "18 in from 0 set1b 425pix.png", list);
        wall1 = new Player();
        wall1.initializewall(17 * pwidth, 18 * pheight, "18 in from 0 set1c 85pix.png", list);
        wall1 = new Player();
        wall1.initializewall(17 * pwidth, 22 * pheight, "18 in from 0 set1d.png", list);
        wall1 = new Player();
        wall1.initializewall(17 * pwidth, 24 * pheight, "18 in from 0 set1e.png", list);

        wall1 = new Player();
        wall1.initializewall(18 * pwidth, 0 * pheight, "19 in from 0 set1a.png", list);
        wall1 = new Player();
        wall1.initializewall(18 * pwidth, 2 * pheight, "19 in from 0 set1b.png", list);
        wall1 = new Player();
        wall1.initializewall(18 * pwidth, 16 * pheight, "19 in from 0 set2a.png", list);
        wall1 = new Player();
        wall1.initializewall(18 * pwidth, 18 * pheight, "19 in from 0 set2b.png", list);
        wall1 = new Player();
        wall1.initializewall(18 * pwidth, 20 * pheight, "19 in from 0 set2c.png", list);
        wall1 = new Player();
        wall1.initializewall(18 * pwidth, 22 * pheight, "19 in from 0 set2d.png", list);
        wall1 = new Player();
        wall1.initializewall(18 * pwidth, 24 * pheight, "19 in from 0 set2e.png", list);

        wall1 = new Player();
        wall1.initializewall(19 * pwidth, 0 * pheight, "20 in from 0 - 1a.png", list);
        wall1 = new Player();
        wall1.initializewall(19 * pwidth, 2 * pheight, "20 in from 0 - 1b.png", list);
        wall1 = new Player();
        wall1.initializewall(19 * pwidth, 16 * pheight, "20 in from 0 - 2a.png", list);
        wall1 = new Player();
        wall1.initializewall(19 * pwidth, 18 * pheight, "20 in from 0 - 2b.png", list);
        wall1 = new Player();
        wall1.initializewall(19 * pwidth, 20 * pheight, "20 in from 0 - 2c.png", list);
        wall1 = new Player();
        wall1.initializewall(19 * pwidth, 22 * pheight, "20 in from 0 - 2d.png", list);
        wall1 = new Player();
        wall1.initializewall(19 * pwidth, 24 * pheight, "20 in from 0 - 2e.png", list);



    }
}
