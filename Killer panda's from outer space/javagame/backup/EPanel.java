/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author mnight08
 */
import java.awt.*;
import javax.swing.*;
import java.util.*;
import java.awt.event.*;
//so the general structure of the program will be to update the player list, and have input affect the player in the list that player reffers to.  this could be
//done with an index to the array, or something similar, but i thought this would be the least confusing.
//cycling through the array seems easier using an integer, so ill do that
public class EPanel extends JPanel implements KeyListener  {

    Random rand;
    Player view;
    Player currentplayer;//reffers to a particular map;
   
   Map[] maps;//a set of all the maps availible.

    EPanel() {
        UpdaterThread ut = new UpdaterThread(this);
        rand = new Random();

        initializemaps();
        initializequest();
        //maps[0].currentplayer = maps[0].characters.element();

        //what is displayed on screen is what ever the player view boxes in.

        //every object that is gonna be displayed will be zeroed, so that the
        //upper left corner of view is the upper left corner of what is
        //displayed
        view = new Player();
        //view.updatedim(Main.width, Main.height);
        //view.updatepos(Main.width / 4, Main.height / 4);
        //view.updatevel(1, 1);

        ut.start();
        addKeyListener(this);
       
        setFocusable(true);
    }
    public void initializequest()
    {
        currentplayer.buildquestlog(this);

        add(new JLabel());

        Quest q= new Quest("welcome to the world");
        q.Q_desc="this is your first quest, have fun";
        currentplayer.pquestlog.Quests.add(q);

        q= new Quest("anotherone");
        q.Q_desc="this is your second quest, good luck";
        currentplayer.pquestlog.Quests.add(q);
    }

    public void initializemaps()
    {
        maps= new Map[2];

        maps[0]= new Map();
        maps[1]= new Map();

        maps[0].initializecharacters();
        maps[0].initializedoorsmap0(maps);
        maps[0].initializecollision();
        maps[0].buildengineering();
        currentplayer=maps[0].characters.element();

        maps[1].initializecharacters();
        maps[1].initializedoorsmap1(maps);
        maps[1].initializecollision();
        maps[1].buildmazelevel();

    }


    public void paintComponent(Graphics g) {
        super.paintComponent(g);
        //view.width = getWidth();
        //view.height = getHeight();

        currentplayer.currentmap.draw(g,view);

    }

    public void update() {
        System.out.println(currentplayer.x+" "+currentplayer.y);
        currentplayer.currentmap.update();
        view.updatedim(getWidth(), getHeight());
        //view keep player in center.
        view.x = currentplayer.x + currentplayer.width / 2 - view.width / 2;
        view.y = currentplayer.y + currentplayer.height / 2 - view.height / 2;
    }

    public void keyPressed(KeyEvent k) {
        if (k.getKeyChar() == 'a') {
            currentplayer.xvel = -10;
        }
        if (k.getKeyChar() == 'w') {
           currentplayer.yvel = -10;
        }
        if (k.getKeyChar() == 'd') {
            currentplayer.xvel = 10;
        }
        if (k.getKeyChar() == 's') {
            currentplayer.yvel = 10;
        }
        //this would be the key to switch characters left
        if (k.getKeyChar() == 'q') {

        }
        if (k.getKeyChar() == ' ')
        {
            currentplayer.interact();
        }
        //this would be the key to switch characters right
        if (k.getKeyChar() == 'e') {

        }

    }

    public void keyReleased(KeyEvent k) {
       currentplayer.xvel = 0;
       currentplayer.yvel = 0;

    }

    public void keyTyped(KeyEvent k) {
    }
   
}
