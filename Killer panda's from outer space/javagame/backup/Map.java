
import java.util.LinkedList;
import java.awt.*;
import java.util.*;
import javax.swing.*;

public class Map {
    //list of portals and players

    LinkedList<Player> characters;
    LinkedList<Player> doors;
    LinkedList<Player> collidableobjects;
    Inventory items;
   // Player currentplayer;//should be a refference the player that is being controlled.
    Map()
    {
        characters= new LinkedList<Player>();
        items = new Inventory();
        doors= new LinkedList<Player>();
        collidableobjects= new LinkedList<Player>();
        initializecharacters();
    }
    public void update() {
        Player temp = null;
        //reset everyones is colliding variables
         for (Iterator<Player> it = characters.iterator(); it.hasNext();) {
            temp = it.next();
            temp.unsetcolliding();
        }
        for (Iterator<Player> it = characters.iterator(); it.hasNext();) {
            temp = it.next();
            temp.isinteractingwith=temp.checkcollision(collidableobjects);
            temp.update();
        }

    }

    public boolean draw(Graphics g, Player view) {


        for (Iterator<Player> it = items.Items.iterator(); it.hasNext();) {
            it.next().draw(g, view);
        }
        for (Iterator<Player> it = collidableobjects.iterator(); it.hasNext();) {
            it.next().draw(g, view);
        }

                for (Iterator<Player> it = characters.iterator(); it.hasNext();) {
            it.next().draw(g, view);
        }
      // currentplayer.pquestlog.draw(g);
        return true;
    }

    //this will probably not stay in the final version
    public void initializecharacters() {
        Random rand = new Random();
        Player temp;

            temp = new Player();
            temp.type="PLAYER";temp.name="PANDY";
            temp.currentmap=this;
            temp.spritesheet = new ImageIcon(Main.class.getResource("images.png"));
            //   players[i].updatedim(30, 30);
            temp.updatepos(rand.nextInt(Main.width - temp.width), rand.nextInt(Main.height - temp.height));
            characters.add(temp);

        for (int i = 0; i < 4; i++) {
            temp = new Player();
            temp.type="questitem";
            temp.currentmap=this;
            temp.spritesheet = new ImageIcon(Main.class.getResource("images.png"));
            //   players[i].updatedim(30, 30);
            temp.updatepos(rand.nextInt(Main.width - temp.width), rand.nextInt(Main.height - temp.height));
            items.Items.add(temp);
        }
       // currentplayer=characters.element();
 

    }
    public void initializedoorsmap0(Map[] maps)
    {
        Player door= new Player();
        door.type="door";
        door.imagesperstate=1;
        door.numberofstates=1;
        door.previmage = 0;
        door.imagematrix = new ImageIcon[door.numberofstates][door.imagesperstate];
        door.collisionscaleh=1;
        door.collisionscalew=1;
        door.state=0;
        door.image=0;
        door.spritesheet = new ImageIcon(Main.class.getResource("door.png"));
        door.buildmatrix();
        door.currentmap=maps[1];


        door.updatepos(400, 400);
        doors.add(door);
    }
    public void initializedoorsmap1(Map[] maps)
    {
        Player door= new Player();
        door.type="door";
        door.imagesperstate=1;
        door.numberofstates=1;
        door.previmage = 0;
        door.imagematrix = new ImageIcon[door.numberofstates][door.imagesperstate];
        door.collisionscaleh=1;
        door.collisionscalew=1;
        door.state=0;
        door.image=0;
        door.spritesheet = new ImageIcon(Main.class.getResource("door.png"));
        door.buildmatrix();
        door.currentmap=maps[0];

        door.updatepos(400, 400);
        doors.add(door);
    }
    public void initializecollision()
    {
        collidableobjects.clear();
        for( Iterator<Player> it= characters.iterator();it.hasNext();)
        {
            collidableobjects.add(it.next());

        }
        for( Iterator<Player> it= items.Items.iterator();it.hasNext();)
        {
            collidableobjects.add(it.next());
        }
        for( Iterator<Player> it= doors.iterator();it.hasNext();)
            collidableobjects.add(it.next());
    }

    public void buildengineering()
    {

        Player wall1;
        int startx=0;
        int starty=0;
        //vertical line
        wall1 = new Player();

        wall1.type="WALL";
        wall1.img=new ImageIcon(Main.class.getResource("wall.png"));
        wall1.collisionscaleh=1;
        wall1.collisionscalew=1;
        wall1.imagesperstate=1;
        wall1.numberofstates=1;
        wall1.imagematrix[0][0]=wall1.img;
        wall1.updatepos(startx, starty);
        wall1.updatedim(30, 1000);
        collidableobjects.add(wall1);

        wall1 = new Player();
        wall1.type="WALL";
        wall1.img=new ImageIcon(Main.class.getResource("wall.png"));
        wall1.collisionscaleh=1;
        wall1.collisionscalew=1;
        wall1.imagesperstate=1;
        wall1.numberofstates=1;
        wall1.imagematrix[0][0]=wall1.img;
        wall1.updatepos(startx+30, starty);
        wall1.updatedim(1000, 30);
        collidableobjects.add(wall1);

        wall1 = new Player();
        wall1.type="WALL";
        wall1.img=new ImageIcon(Main.class.getResource("wall.png"));
        wall1.collisionscaleh=1;
        wall1.collisionscalew=1;
        wall1.imagesperstate=1;
        wall1.numberofstates=1;
        wall1.imagematrix[0][0]=wall1.img;
        wall1.updatepos(startx+30, starty+970);
        wall1.updatedim(1000, 30);
        collidableobjects.add(wall1);

        wall1 = new Player();
        wall1.type="WALL";
        wall1.img=new ImageIcon(Main.class.getResource("wall.png"));
        wall1.collisionscaleh=1;
        wall1.collisionscalew=1;
        wall1.imagesperstate=1;
        wall1.numberofstates=1;
        wall1.imagematrix[0][0]=wall1.img;
        wall1.updatepos(startx+1030, starty);
        wall1.updatedim(30, 1000);
        collidableobjects.add(wall1);

    }

    public void buildmazelevel() {
        Player wall1;

        wall1 = new Player();
        wall1.img=new ImageIcon(Main.class.getResource("wall.png"));
        wall1.type="WALL";
        wall1.collisionscaleh=1;
        wall1.collisionscalew=1;
        wall1.imagesperstate=1;
        wall1.numberofstates=1;
        wall1.imagematrix[0][0]=wall1.img;
        wall1.updatepos(0, 0);
        wall1.updatedim(300, 100);
        collidableobjects.add(wall1);

        wall1 = new Player();
        wall1.img=new ImageIcon(Main.class.getResource("wall.png"));
        wall1.type="WALL";
        wall1.collisionscaleh=1;
        wall1.collisionscalew=1;
        wall1.imagesperstate=1;
        wall1.numberofstates=1;
        wall1.imagematrix[0][0]=wall1.img;
        wall1.updatepos(0, 0);
        wall1.updatedim(100, 30);
        collidableobjects.add(wall1);


        //gonna be something like a thousand entries
        /*int[][] spritebreakup={   {1,0,0,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1},
                                    {1,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1},
                                    {1,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,0,0,1},
                                    {1,0,0,1,0,0,1,1,1,1,1,1,1,1,1,1,1,1,0,0,0,0,0,0,0,0,0,1,0,0,0,0,0,0,0,0,0,1},
                                    {1,0,0,1,0,0,1,0,0,0,0,0,0,0,0,0,0,1,0,0,1,1,1,0,0,1,1,1,1,1,1,1,1,1,1,1,0,1},
                                    {1,0,0,1,0,0,1,0,0,0,0,0,1,0,1,0,0,1,0,0,1,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1},
                                    {1,0},
                                    {1,0},
                                    {1,0},
                                    {1,0},
                                    {1,0},
                                    {1,0},
                                    {1,1},
                                    {1,0},
                                    {1,0},
                                    {1,0},
                                    {1,0},
                                    {1,0},
                                    {1,0},
                                    {1,0},
                                    {1,0},
                                    {1,0},
                                    {1,0},
                                    {1,0},
                                    {1,0},
                                    {1,0},
        };*/
    }
}
