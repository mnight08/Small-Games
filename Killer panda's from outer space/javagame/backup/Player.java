/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author mnight08
 */
import java.awt.*;
import java.awt.image.*;
import javax.swing.ImageIcon;
import java.lang.*;
import java.util.LinkedList;
import java.util.*;
//this should be renamed,to something like animated actor
public class Player extends Rectangle {
    //the number of different animations it will cycle through

    int xvel;
    int yvel;
    int collisionscalew;
    int collisionscaleh;
    boolean isscaled;
    int numberofstates;     //the number of different rows in imagematrix
    int imagesperstate;     //the number of collumns of image matrix
    int state;              //the current row of imagematrix
    int image;              //the current collumn of image matrix
    int prevstate;
    int previmage;
    ImageIcon img;      //the image that will be drawn
    ImageIcon[][] imagematrix;//a matrix with each sprite in the sprite sheet
    ImageIcon spritesheet;      //sheet with all the sprites
    boolean iscollidingleft;
    boolean iscollidingright;
    boolean iscollidingup;
    boolean iscollidingdown;
    QuestLog pquestlog;
    Player isinteractingwith;
    Map currentmap;
    Inventory questItems;
    Inventory clothes;
    String type;
    String name;

    Player() {
        numberofstates = 4;
        collisionscalew = 1;
        collisionscaleh = 2;
        imagesperstate = 4;
        prevstate = 0;
        previmage = 0;
        imagematrix = new ImageIcon[numberofstates][imagesperstate];
        spritesheet = new ImageIcon(Main.class.getResource("panda.png"));

        questItems = new Inventory();
        clothes = new Inventory();

        state = 0;
        image = 0;        //this should probably be the maximum number of images per state.  this assumes that every sprite sheet is a square matrix filled with equally sized images
        isscaled = false;
        buildmatrix();

    }

    public void moveplayer(Player p) {
        if (p.currentmap != null) {
            p.currentmap.characters.remove(p);
            p.currentmap.collidableobjects.remove(p);
        }
        p.currentmap = currentmap;
        currentmap.characters.add(p);
        currentmap.collidableobjects.add(p);
    }

    public void pickup(Player item, int mode) {
        System.out.println(type + " " + name + " pick up " + item.type + " " + item.name);
        currentmap.items.Items.remove(item);
        currentmap.characters.remove(item);
        currentmap.collidableobjects.remove(item);
        currentmap.doors.remove(item);
        if (mode == 0) //right now just let everything be a quest item.
        {
            questItems.Items.add(isinteractingwith);
        } else if (mode == 1) {
            clothes.Items.add(isinteractingwith);
        }
    }

    void interact() {

        if (isinteractingwith == null) {
            return;
        } else {
            System.out.println(isinteractingwith.type + " " + isinteractingwith.name);
            if (isinteractingwith.type == "door") {
                isinteractingwith.moveplayer(this);
            } else if (isinteractingwith.type == "questitem") {
                pickup(isinteractingwith, 0);

            } else if (isinteractingwith.type == "clothes") {
                pickup(isinteractingwith, 1);

            }
        }

    }
    /*   public void setinteracting(LinkedList<Player> l)
    {
    isinteractingwith=checkcollision(l);

    }
     */ public void buildquestlog(EPanel p) {
        pquestlog = new QuestLog(p);
    }

    public void buildmatrix() {
        width = spritesheet.getIconWidth() / imagesperstate;
        height = spritesheet.getIconHeight() / numberofstates;

        for (int i = 0; i < numberofstates; i++) {
            for (int j = 0; j < imagesperstate; j++) {
                imagematrix[i][j] = new ImageIcon(Toolkit.getDefaultToolkit().createImage(new FilteredImageSource(spritesheet.getImage().getSource(), new CropImageFilter(j * width, i * height, width, height))));
            }
        }
    }

    public void scaleforcollision() {
        y = y + (height - height / collisionscaleh);//to get back origional position just y=y-(height*collisionscaleh-height)
        //collisions should check a box that is in the lower left corner of the picture
        width = width / collisionscalew;
        height = height / collisionscaleh;//to get back origional position just y=y-(height*collisionscaleh-height)
        isscaled = true;
    }

    public void unscale() {
        width = width * collisionscalew;
        height = height * collisionscaleh;
        y = y - (height - height / collisionscaleh);
        isscaled = false;
    }

    public void setimg() {
        setimage();
        setstate();
        if (state >= numberofstates) {
            img = imagematrix[prevstate][0];
        } else {
            img = imagematrix[state][image];
            //           System.out.println("img set to imagematrix["+state+"]["+image+"]");
        }
    }
    //we should probably change from Image to BufferedImage

    public void setstate() {
        prevstate = state;//this might not be neccesary
        if (xvel < 0) {
            state = 1;
        } else if (xvel > 0) {
            state = 2;
        } else if (yvel > 0) {
            state = 0;
        } else if (yvel < 0) {
            state = 3;
        } else {
            state = prevstate;
        }
    }

    public void setimage() {
        if (xvel != 0 || yvel != 0) {
            image = (image + 1) % imagesperstate;
        }
    }

    public boolean draw(Graphics g, Player view) {
        if (view.intersects(this)) {
            setimg();//set img to the appropriate image from the image matrix

            if (isscaled == false) {
                scaleforcollision();//set the position and dimensions so that the picture is drawn right
            }            //g.setColor(Color.red);
            //g.fillRect(x-view.x, y-view.y, width, height);//this draws where each object is colliding
            unscale();
            g.drawImage(img.getImage(), x - view.x, y - view.y, width, height, null);
            //g.drawImage(img.getImage(), x - view.x, y - view.y, null);//to get back origional position just y=y-(height*collisionscaleh-height)
            scaleforcollision();//set the position and dimensions so that collisions happen right
            return true;
        } else {
            return false;
        }
    }

    public void updatedim(int w, int h) {
        width = w;
        height = h;
    }

    public void update() {
        if ((((iscollidingright) && (iscollidingleft)) || (iscollidingleft && xvel < 0) || (iscollidingright && xvel > 0)) && !(iscollidingup && iscollidingdown && iscollidingleft && iscollidingright)) {
            xvel = 0;
        }
        if ((((iscollidingup) && (iscollidingdown)) || (iscollidingup && yvel < 0) || (iscollidingdown && yvel > 0)) && !(iscollidingup && iscollidingdown && iscollidingleft && iscollidingright)) {
            yvel = 0;
        }

        if (iscollidingup && iscollidingdown && iscollidingleft && iscollidingright && xvel < 0) {
            xvel = 2;
        } else if (iscollidingup && iscollidingdown && iscollidingleft && iscollidingright && xvel > 0) {
            xvel = -2;
        }
        if (iscollidingup && iscollidingdown && iscollidingleft && iscollidingright && yvel < 0) {
            yvel = 2;
        } else if (iscollidingup && iscollidingdown && iscollidingleft && iscollidingright && yvel > 0) {
            yvel = -2;
        }

        x = x + xvel;
        y = y + yvel;
    }

    //to give animated motion to the objects, we can have a counter, and a number
    //of images, so that it cycles through a different image each tick
    //ImageIcon image[5][5];int imagen;int direction;
    public void updatevel(int xt, int yt) {
        xvel = xt;
        yvel = yt;
    }

    public void updatepos(int xt, int yt) {
        x = xt;
        y = yt;
    }
    //im not sure if this is fine if i can make bounce do the right thing

    public Player checkcollision(LinkedList<Player> p) {
        //int k=0;
        Player temp = null;
        Player iscolliding = null;
        for (Iterator<Player> it = p.iterator(); it.hasNext();) {
            temp = it.next();
            if (temp.intersects(this) && this != temp) {
                setcolliding(temp);
                iscolliding = temp;
            }

        }
        if (iscolliding == null) {
            unsetcolliding();
        }
        return iscolliding;
    }

    public void setcolliding(Player p) {

        if (x + width >= p.x && x + width <= p.x + p.width) {
            iscollidingright = true;
        }

        if (x >= p.x && p.x + p.width <= x + width) {
            iscollidingleft = true;
        }

        if (y + height >= p.y && y + height <= p.y + p.height) {
            iscollidingdown = true;
        }

        if (y >= p.y && p.y + p.height <= y + height) {
            iscollidingup = true;
        }
    }

    public void unsetcolliding() {
        iscollidingleft = iscollidingright = iscollidingup = iscollidingdown = false;
    }
}
