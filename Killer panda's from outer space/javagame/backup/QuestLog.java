/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author mnight08
 */

import java.awt.*;
import java.awt.event.*;
import java.util.LinkedList;
import java.util.*;
import javax.swing.BoxLayout;

public class QuestLog{

    LinkedList<Quest>   Quests;
    EPanel              panel;
    GridBagLayout layout;
    GridBagConstraints c;
   Graphics draw;
        QuestLog( EPanel p )
        {
            panel = p;
            Quests= new LinkedList<Quest>();
            layout = new GridBagLayout();
            panel.setLayout(new BoxLayout(panel, BoxLayout.Y_AXIS));

            //not sure on how to get the buttons to layout correctly on the screen
            c = new GridBagConstraints();

        }
        public void refresh()
        {
           minusQuest();
            addQuest();

        }
        //iterarte through a list and add each quest button to the panel
        public void addQuest()
        {
            Quest q_holder;
            for( Iterator<Quest> it = Quests.iterator(); it.hasNext(); )
            {
                q_holder = it.next();
                panel.remove( q_holder.Q_button );///panel.setLayout(new GridLayout(1,2));
                panel.add( q_holder.Q_button,BorderLayout.WEST);
            }
        }

        //iterate through the list and remove selected button from the panel
        public void minusQuest()
        {
            Quest q_holder;
            for( Iterator<Quest> it = Quests.iterator(); it.hasNext(); )
            {
                q_holder = it.next();
                panel.remove( q_holder.Q_button );
            }
        }

    public void update(Player interactingwith)
    {



    }

    public void draw(Graphics g)
    {
        refresh();
        g.fillRect(0, 0, panel.getWidth(), 60);
        int step =15;int i=1;
        for(Iterator<Quest> it= Quests.iterator();it.hasNext();)
        {
            if( it.next().draw(g,0,i*step));
            i++;
        }
    }

}
