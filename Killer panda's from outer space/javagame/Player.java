/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author mnight08
 */
import java.awt.*;
import java.awt.image.*;
import javax.swing.ImageIcon;
import java.util.*;
//this should be renamed,to something like animated actor
public class Player extends Rectangle implements Comparable<Player> {
    //the number of different animations it will cycle through

    int xvel;
    int yvel;
    int collisionscalew;
    int collisionscaleh;
    
    boolean isscaled;

    int numberofstates;     //the number of different rows in imagematrix
    int imagesperstate;     //the number of collumns of image matrix
    int state;              //the current row of imagematrix
    int image;              //the current collumn of image matrix
    int prevstate;
    int previmage;
    int draworder;

    ImageIcon img;      //the image that will be drawn
    ImageIcon[][] imagematrix;//a matrix with each sprite in the sprite sheet
    ImageIcon spritesheet;      //sheet with all the sprites

    boolean iscollidingleft;
    boolean iscollidingright;
    boolean iscollidingup;
    boolean iscollidingdown;

    QuestLog pquestlog;
    Player isinteractingwith;
    Player otherside;
    Map currentmap;

    Inventory questItems;
    Inventory clothes;

    String type;
    String name;
    String dialog;
    Player() {


    }
    public int compareTo(Player p)
    {
        if(draworder==p.draworder)
            return 0;
       else if(draworder<p.draworder)
            return -1;
        else return 1;
    }

    public void intializeinvisiblebox(int xt, int yt, int endx, int endy,PriorityQueue list)
    {
        numberofstates = 1;
        collisionscalew = 1;
        collisionscaleh = 1;
        imagesperstate = 1;
        prevstate = 0;
        previmage = 0;
        draworder=3;
        updatepos(xt,yt);
        updatedim(endx-xt,endy-yt);

     //   imagematrix = new ImageIcon[numberofstates][imagesperstate];
       // spritesheet = new ImageIcon(Main.class.getResource("panda.png"));
      //  x=y=50;
      //  questItems = new Inventory();
        //clothes = new Inventory();
        dialog="There is nothing of use here";
        state = 0;
        image = 0;        //this should probably be the maximum number of images per state.  this assumes that every sprite sheet is a square matrix filled with equally sized images
        isscaled = false;
        list.add(this);
        //buildmatrix();
    }

    public void initializepanda()
    {
        numberofstates = 4;
        collisionscalew = 3;
        collisionscaleh = 3;
        imagesperstate = 4;
        prevstate = 0;
        previmage = 0;
        draworder=3;
        imagematrix = new ImageIcon[numberofstates][imagesperstate];
        spritesheet = new ImageIcon(Main.class.getResource("panda.png"));
        x=y=50;
        questItems = new Inventory();
        clothes = new Inventory();
        dialog="I wonder when they are gonna finish remodeling?";
        state = 0;
        image = 0;        //this should probably be the maximum number of images per state.  this assumes that every sprite sheet is a square matrix filled with equally sized images
        isscaled = false;
        buildmatrix();

    }
    public void initializeplayer(Map m)
    {
          initializepanda();
          type="PLAYER";name="panda";
          currentmap=m;
          draworder=3;
          spritesheet = new ImageIcon(Main.class.getResource("images.png"));
          m.list.add(this);
    }
    public void intitializebackground(int xt, int yt,String name, PriorityQueue list)
    {
        draworder=1;
        type="background";
        collisionscaleh=1;
        collisionscalew=1;
        imagesperstate=1;
        numberofstates=1;

        imagematrix = new ImageIcon[numberofstates][imagesperstate];
        spritesheet = new ImageIcon(Main.class.getResource(name).getFile().replace("%20", " "));
        state = 0;
        image = 0;        //this should probably be the maximum number of images per state.  this assumes that every sprite sheet is a square matrix filled with equally sized images
        isscaled = false;
        buildmatrix();

        updatepos(xt, yt);
        //updatedim(0,0);
        list.add(this);
    }
    public void initializedoor(PriorityQueue list, int xt, int yt)
    {
        type="door";
        draworder=1;
        imagesperstate=1;
        numberofstates=4;
        previmage = 0;
        imagematrix = new ImageIcon[numberofstates][imagesperstate];
        collisionscaleh=2;
        collisionscalew=1;
        state=0;
        image=0;
        spritesheet = new ImageIcon(Main.class.getResource("door.png"));
        buildmatrix();

        updatepos(xt, yt);
        list.add(this);
    }
    public void initializewall(int xt, int yt, int wt,int ht, PriorityQueue list)
    {
        draworder=0;
        type="WALL";
        collisionscaleh=1;
        collisionscalew=1;
        imagesperstate=1;
        numberofstates=1;
        imagematrix = new ImageIcon[numberofstates][imagesperstate];
        spritesheet = new ImageIcon(Main.class.getResource("wall.png"));
        state = 0;
        image = 0;        //this should probably be the maximum number of images per state.  this assumes that every sprite sheet is a square matrix filled with equally sized images
        isscaled = false;
        buildmatrix();

        updatepos(xt, yt);
        updatedim(wt, ht);
        list.add(this);
    }
    public void initializewall(int xt, int yt, String name, PriorityQueue list)
    {
        draworder=1;
        type="WALL";
        collisionscaleh=1;
        collisionscalew=1;
        imagesperstate=1;
        numberofstates=1;
        name.replace("%20", " ");
        imagematrix = new ImageIcon[numberofstates][imagesperstate];
        spritesheet = new ImageIcon(Main.class.getResource(name).getFile().replace("%20", " "));
        state = 0;
        image = 0;        //this should probably be the maximum number of images per state.  this assumes that every sprite sheet is a square matrix filled with equally sized images
        isscaled = false;
        buildmatrix();

        updatepos(xt, yt);
        list.add(this);
    }
    public void moveplayer(Player p) {
        if(p!=null)
        {

           otherside.currentmap.list.add(p);
           if (p.currentmap != null) {
               p.currentmap.removelist.add(p);
            }
           p.updatepos(otherside.x,otherside.y+otherside.height);
           p.currentmap = otherside.currentmap;

        }
    }

    public void pickup(Player item, int mode) {
        System.out.println(type + " " + name + " pick up " + item.type + " " + item.name);
        currentmap.list.remove(item);
        if (mode == 0) 
        {
            questItems.Items.add(isinteractingwith);
        } else if (mode == 1) {
            clothes.Items.add(isinteractingwith);
        }
    }

    void interact() {
        if (isinteractingwith == null) {
            return;
        } else {
            
            if(type=="PLAYER"&&isinteractingwith.type=="panda")System.out.println(isinteractingwith.type + " " + isinteractingwith.name + "says:" +dialog);
            if (isinteractingwith.type == "door") {
                isinteractingwith.moveplayer(this);
            } else if (isinteractingwith.type == "questitem") {
                pickup(isinteractingwith, 0);
            } else if (isinteractingwith.type == "clothes") {
                pickup(isinteractingwith, 1);
            }
        }
    }
    public void buildquestlog(EPanel p) {
        pquestlog = new QuestLog(p);
    }

    public void buildmatrix() {
        width = spritesheet.getIconWidth() / imagesperstate;
        height = spritesheet.getIconHeight() / numberofstates;

        for (int i = 0; i < numberofstates; i++) {
            for (int j = 0; j < imagesperstate; j++) {
                imagematrix[i][j] = new ImageIcon(Toolkit.getDefaultToolkit().createImage(new FilteredImageSource(spritesheet.getImage().getSource(), new CropImageFilter(j * width, i * height, width, height))));
            }
        }
    }

    public void scaleforcollision() {
       // if(!isscaled)
       // {
        y = y + (height - height / collisionscaleh);//to get back origional position just y=y-(height*collisionscaleh-height)
        x = x + (width/2 - width/(2*collisionscalew));
        //collisions should check a box that is in the lower left corner of the picture
        width = width / collisionscalew;
        height = height / collisionscaleh;//to get back origional position just y=y-(height*collisionscaleh-height)
      //  }

        isscaled = true;
    }

    public void unscale() {
        if(isscaled)
        {
            width = width * collisionscalew;
            height = height * collisionscaleh;
            y = y - (height - height / collisionscaleh);
            x = x - (width/2 - width/(2*collisionscalew));
        }
        isscaled = false;
    }

    public void setimg() {
        setimage();
        setstate();
        if(spritesheet==null)return;
        if (state >= numberofstates) {
            img = imagematrix[prevstate][0];
        } else {
            img = imagematrix[state][image];
            //           System.out.println("img set to imagematrix["+state+"]["+image+"]");
        }
    }
    //we should probably change from Image to BufferedImage

    public void setstate() {
        if(type == "door")
        {
            if(isinteractingwith!=null)
            {
                if(state!=3)
                    state=(state+1)%numberofstates;
            }
            else {
            state=0;
            }
        }
        else
        {
            prevstate = state;//this might not be neccesary
            if (xvel < 0) {
             state = 1;
            } else if (xvel > 0) {
                state = 2;
            } else if (yvel > 0) {
                state = 0;
            } else if (yvel < 0) {
                state = 3;
            } else {
                state = prevstate;
            }
        }
    }

    public void setimage() {

        if (xvel != 0 || yvel != 0) {
            image = (image + 1) % imagesperstate;
        }
    }

    public void drawcollisionbox(Graphics g,Player view)
    {
        if(!isscaled)scaleforcollision();
        g.setColor(Color.red);
        g.fillRect(x-view.x, y-view.y, width, height);

    }
    public boolean draw(Graphics g, Player view) {

        if (view.intersects(this)) {
            setimg();//set img to the appropriate image from the image matrix

            //for testing collision
            //if(type!="background")
           // drawcollisionbox(g,view);
            if(type=="PLAYER")
            System.out.println(x+","+y);
            if (isscaled) {
                unscale();//set the position and dimensions so that the picture is drawn right
            }            //g.setColor(Color.red);

            if(img!=null)
                g.drawImage(img.getImage(), x - view.x, y - view.y, width, height, null);

            return true;
        } else {
            return false;
        }
    }

    public void updatedim(int w, int h) {
        width = w;
        height = h;
    }

    public void update() {
        if(type=="PLAYER")
        {
            if ((((iscollidingright) && (iscollidingleft)) || (iscollidingleft && xvel < 0) || (iscollidingright && xvel > 0)) && !(iscollidingup && iscollidingdown && iscollidingleft && iscollidingright)) {
                xvel = 0;
            }
            if ((((iscollidingup) && (iscollidingdown)) || (iscollidingup && yvel < 0) || (iscollidingdown && yvel > 0)) && !(iscollidingup && iscollidingdown && iscollidingleft && iscollidingright)) {
                yvel = 0;
            }
            if (iscollidingup && iscollidingdown && iscollidingleft && iscollidingright && xvel < 0) {
                xvel = 2;
            } else if (iscollidingup && iscollidingdown && iscollidingleft && iscollidingright && xvel > 0) {
                xvel = -2;
            }
            if (iscollidingup && iscollidingdown && iscollidingleft && iscollidingright && yvel < 0) {
                yvel = 2;
            } else if (iscollidingup && iscollidingdown && iscollidingleft && iscollidingright && yvel > 0) {
                yvel = -2;
            }
        }
        else if(isinteractingwith!=null&&type=="panda")
        {
                interact();

                if ((((iscollidingright) && (iscollidingleft)) || (iscollidingleft && xvel < 0) || (iscollidingright && xvel > 0)) && !(iscollidingup && iscollidingdown && iscollidingleft && iscollidingright)) {
                    xvel = -xvel;
                }
                if ((((iscollidingup) && (iscollidingdown)) || (iscollidingup && yvel < 0) || (iscollidingdown && yvel > 0)) && !(iscollidingup && iscollidingdown && iscollidingleft && iscollidingright)) {
                    yvel = -yvel;
                }
                if (iscollidingup && iscollidingdown && iscollidingleft && iscollidingright && xvel < 0) {
                    xvel = 2;
                } else if (iscollidingup && iscollidingdown && iscollidingleft && iscollidingright && xvel > 0) {
                    xvel = -2;
                }
                if (iscollidingup && iscollidingdown && iscollidingleft && iscollidingright && yvel < 0) {
                    yvel = 2;
                } else if (iscollidingup && iscollidingdown && iscollidingleft && iscollidingright && yvel > 0) {
                    yvel = -2;
                }
            
        }
        else
        {
                if ((((iscollidingright) && (iscollidingleft)) || (iscollidingleft && xvel < 0) || (iscollidingright && xvel > 0)) && !(iscollidingup && iscollidingdown && iscollidingleft && iscollidingright)) {
                    xvel = -xvel;
                }
                if ((((iscollidingup) && (iscollidingdown)) || (iscollidingup && yvel < 0) || (iscollidingdown && yvel > 0)) && !(iscollidingup && iscollidingdown && iscollidingleft && iscollidingright)) {
                    yvel = -yvel;
                }
                if (iscollidingup && iscollidingdown && iscollidingleft && iscollidingright && xvel < 0) {
                    xvel = 2;
                } else if (iscollidingup && iscollidingdown && iscollidingleft && iscollidingright && xvel > 0) {
                    xvel = -2;
                }
                if (iscollidingup && iscollidingdown && iscollidingleft && iscollidingright && yvel < 0) {
                    yvel = 2;
                } else if (iscollidingup && iscollidingdown && iscollidingleft && iscollidingright && yvel > 0) {
                    yvel = -2;
                }

        }
        x = x + xvel;
        y = y + yvel;
    }

    //to give animated motion to the objects, we can have a counter, and a number
    //of images, so that it cycles through a different image each tick
    //ImageIcon image[5][5];int imagen;int direction;
    public void updatevel(int xt, int yt) {
        xvel = xt;
        yvel = yt;
    }

    public void updatepos(int xt, int yt) {
        x = xt;
        y = yt;
    }
    //im not sure if this is fine if i can make bounce do the right thing

    public Player checkcollision(PriorityQueue<Player> p) {
        Player temp = null;
        Player iscolliding = null;
        unsetcolliding();
        for (Iterator<Player> it = p.iterator(); it.hasNext();) {
            temp = it.next();
            if(!temp.isscaled)temp.scaleforcollision();
            if (temp.intersects(this) && this != temp&&temp.type!="background"&&type!="background") {
                setcolliding(temp);
                iscolliding = temp;
            }


        }
        return iscolliding;
    }

    public void setcolliding(Player p) {
        if (x + width >= p.x && x + width <= p.x + p.width) {
            iscollidingright = true;
     //       if(xvel>0)xvel=-xvel;
        }

        if (x >= p.x && p.x + p.width <= x + width) {
            iscollidingleft = true;
       //     if(xvel<0)xvel=-xvel;
        }

        if (y + height >= p.y && y + height <= p.y + p.height) {
            iscollidingdown = true;
         //   if(yvel>0)yvel=-yvel;
        }

        if (y >= p.y && p.y + p.height <= y + height) {
            iscollidingup = true;
           // if(yvel<0)yvel=-yvel;
        }
    }

    public void unsetcolliding() {
        iscollidingleft = iscollidingright = iscollidingup = iscollidingdown = false;
    }
}
